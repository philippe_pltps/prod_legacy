
    <?php    

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}

?>
<!DOCTYPE html>
<html>

<head>

	<meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    
   
	<title>Platypus privacy notice</title>

	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/site.webmanifest">
  <meta name="viewport" content="width=device-width, user-scalable=yes">

</head>

<body >
<p>
Privacy notice for job candidates providing questionnaire responses in the Platypus tool <br>
Version: June 2019
</p>
<p>
When providing your responses to the questionnaire on organisational culture in connection with a specific job posting, we will, as a data processor for the employer as the data controller, collect, process and disclose personal data about you to the employer who has initiated the use of and provided you with access to the questionnaire. 
</p>
<p>
If you choose to create a profile in the tool and provide your consent, we will also, as the data controller, store your personal data and your responses in the tool beyond the initial recruitment process for the purpose of matching your profile with other job postings made by other employers which could be relevant to you based on your preferences and wishes. 
</p>
<p>
This privacy notice only concerns the processing of personal data that we are responsible for as a data controller. For more information on how a specific employer making use of our tool processes your personal data in connection with a recruitment process, please contact that employer.  
</p>
<p>
Categories of personal of data and purposes<br>
We collect and process the following types of personal data about you when you create a profile in the tool
</p>
<p>
Name and email address provided prior to gaining access to the survey<br>
Responses to questionnaire on organisational culture <br>
Years of work experience <br>
Gender <br>
Information on the job posting which you are applying for <br>
Job preferences <br>
Contact details<br>
Consents provided to processing of personal data<br>
Permissions to direct marketing, e.g. newsletters with marketing<br>
</p>
<p>
We process the personal data for the following purposes:<br>
To collect and present responses to the questionnaire on organisational culture and match such responses to the responses of a specific organisation’s employee on behalf of that organisation in connection with its recruitment efforts 
To store such responses beyond the initial recruitment process and match them to the responses regarding other organisations in connection with other job postings corresponding to your job preferences (if you consent to such further processing)
To comply with applicable personal data protection regulation and other legitimate interests, e.g. <br>
Marketing purposes, including direct marketing purposes<br>
Documentation requirements<br>
Compliance with principles and legal grounds for processing personal data 
Putting in place, maintaining and testing technical and organisational security measures<br>
Investigating and reporting suspected personal data breaches, if any<br>
Handling requests and complaints from data subjects and others, if any<br>
Establishment, exercise or defence of legal claims, if any<br>
Handling inspections and queries by supervisory authorities, if any<br>
Handling disputes with data subjects and third parties, if any<br>
</p>
<p>
The legal basis for the collection and processing of the personal data
The legal basis for our processing and storage of your personal data and the responses to the questionnaire beyond the initial recruitment process is your consent, cf. art. 6(1)(a) of the General Data Protection Regulation (GDPR).
</p>
<p>
The processing of personal data relating to the legitimate interests mentioned above is article 6(1)(f) of the GDPR.
</p>
<p>
When we collect the additional personal data directly from you for the purpose of creating a profile to enable the continued storage of your responses to the questionnaire, you provide the personal data voluntarily. You are not obliged to provide the information to us. The consequences of not providing the personal data are that your responses to the questionnaire cannot be re-used for other job postings relevant to you. 
</p>
<p>
Disclosure of the personal data to other data controllers<br>
Your responses to the questionnaire and contact details will be disclosed to Employers with job postings that match your job preferences and wishes
</p>
<p>
The legal basis for the disclosure of these personal data is your consent, cf. art. 6(1)(a) of the GDPR. The consent is not obtained for each disclosure but as a general consent to future disclosures to multiple potential employers. You will not be notified each time your personal data is disclosed.
</p>
<p>
Withdrawal of consent <br>
You have the right to withdraw your consent. If you withdraw the consent, this will not affect the lawfulness of the processing and disclosure prior to the withdrawal. Please contact us using the contact details below if you wish to exercise this right.
</p>
<p>
Transfer of personal data to data processors <br>
We transfer the personal data to our web server hosting and database provider which stores the personal data on our behalf.
</p>
<p>
Retention period<br>
We store the personal data for as long as necessary to fulfil the purposes above, however, for no longer than 12 months. 
</p>
<p>
These general periods apply: <br>
We store personal data relating to marketing purposes for as long as necessary to proof compliance with marketing and data protection legislation and to be able do have documentation necessary for establishment, exercise or defence of legal claims. 
Under the data protection legislation, claims are barred after 5 years. 
Under the marketing permissions, claims are barred after 2 years following from the latest use of the permission. <br>
Civil claims are generally barred after 3 – 10 years.
</p>
<p>
Your rights<br>
Subject to the conditions set out in the applicable data protection legislation, you enjoy the following certain rights: <br>
The right to request access to the personal data<br>
The right to rectification of personal data which inaccurate and the right to have incomplete personal data completed<br>
The right to erasure of the personal data<br>
The right to restriction of processing<br>
The right to data portability<br>
The right to object to automated, individual decisions, including profiling<br>
 </p>
<p>
The right to object<br>
You have the right to object, on grounds relating to your particular situation, at any time to processing of personal which is based on point (e) and (f) of Article 6(1) of the GDPR, including profiling based on those provisions. We are no longer permitted to process the personal data unless we demonstrate compelling legitimate grounds for the processing, which override your interests, rights and freedoms or if processing is necessary for the establishment, exercise or defence of legal claims.
 </p>
<p>
Where personal data are processed for direct marketing purposes, you have the right to object at any time to processing of personal data concerning you for such marketing, which includes profiling to the extent that it is related to such direct marketing. Where you object to the processing for direct marketing purposes, the personal data can no longer be processed for such purposes.
</p>
<p>
You also have the right to lodge a complaint with the competent supervisory authority, such as the Danish Data Protection Agency. Please consult their website for how to submit a complaint at www.datatilsynet.dk. 
</p>
<p>
Contact<br>
Please contact us on the details below if you have any questions in regard to the protection of your personal data or if you wish to exercise your legal rights.
</p>
<p>
Contact details of the data controller: <br>
ThePlatypus ApS<br>
Engvej 104. <br>
2300 København S<br> 
Denmark<br>
Company registration no. in Denmark: 40292829<br>
Email address: support@theplatypus.io<br>
</p>



</body>


</html>