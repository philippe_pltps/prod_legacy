<?php

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}


// Include config file
include "db_helper/db_util.php";
include "db_helper/session.php"; //Include PHP MySQL sessions
?>
<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script> 
<?php

 
// Define variables and initialize with empty values
$email = $password = $confirm_password = "";
$email_err = $password_err = $confirm_password_err = "";
 

 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validate username
    if(empty(trim($_POST["email"]))){
        $email_err = "Please enter an email.";
    } 
    else
    {
        if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) 
        {
    // invalid emailaddress
            $email_err = "Please enter a valid email address";
        }
        else
        {
            $param_email = trim($_POST["email"]);
            $user_id = get_user_id($param_email);
            if(!empty($user_id))
            {
                $email_err = "This email is already taken.";
            } else{
                $email = trim($_POST["email"]);

            }
        }
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
    
    // Check input errors before inserting in database
    if(!empty($email) && empty($email_err) && empty($password_err) && empty($confirm_password_err))
    {
    
    // Set parameters
    $param_email = $email;
    if(empty($_POST["consent"]))
        {
            echo '<script>';
            echo '$(window).load(function () {';
            echo '$("#box_doc").show();';
            echo '});';
            echo '</script>';
        } 
    else 
        {

            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            $user_id = insert_user_pwd($param_email,$param_password);

            if(!empty($user_id))
                {    
                    insert_user_consent($user_id);
                    // Store data in session variables
                    $_SESSION["loggedin"] = true;
                    $_SESSION["id"] = $user_id;
                    $_SESSION["email"] = $email;                            
                    header("location: welcome.php");
                }
            else{
                echo "Something went wrong.";
                }
        }
    
    }

}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign up to Theplatypus</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/styles_cand.css">
    <link rel="stylesheet" href="css/popup_box.css">
    <link rel="stylesheet" href="css/cookie_bar.css">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/site.webmanifest">
</head>
<body>
    <?php
    include "php_util/cookie_bar.php";
    
    ?>
    <div class="background">
        <div class="bg-div">
            <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
            <nav>
            </nav>
        </div>
        <h1>Sign Up</h1>
        <h2>Please fill this form to create an account.</h2>
        <div class="card">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                    <div class="banner-content">
                        <div class="signup-form-box">
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                                <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                                    <h3 class="bold">Email</h3>
                                    <input type="text" name="email" class="form-control" value="<?php echo $email; ?>">
                                    <span class="help-block"><?php echo $email_err; ?></span>
                                </div>    
                                <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                                    <h3 class="bold">Password</h3>
                                    <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                                    <span class="help-block"><?php echo $password_err; ?></span>
                                </div>
                                <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                                    <h3 class="bold">Confirm Password</h3>
                                    <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                                    <span class="help-block"><?php echo $confirm_password_err; ?></span>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="button-2">SUBMIT</button>
                                </div>
                                <p>Already have an account? <a href="login.php">Login here</a>.</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id='box_doc' class="hover_bkgr_fricc">
        <div class="popup_box popup_box_doc">
            <div w3-include-html="assets/eula.html" class='scroll_box_doc'></div>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <input type="text" name="email" class="form-control" value="<?php echo $email; ?>" style="display: none;">
                <input  type="password" name="password" class="form-control" value="<?php echo $password; ?>" style="display: none;">
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>" style="display: none;">
                <input  type="password" name="consent" class="form-control" value="<?php echo getRandString(15) ?>" style="display: none;">
            <button class="button-2"> ACCEPT </button>
            <a href="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"  class="button-2"> REFUSE </a>
            </form>
        </div>
    </div>    
</body>

<script src="js/functions/cookie_bar.js"></script>
<script src="js/functions/tools.js"></script>
<script>
includeHTML();
</script>

</html>