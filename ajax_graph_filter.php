<?php


include 'db_helper/db_util.php';
$input = $_POST["requestType"]; // Gets the intention of the ajax post requesting this code
/*$filter_gender = $_POST["filterGender"];
$filter_tenure = $_POST["filterTenure"];
$filter_department = $_POST["filterDepartment"];
$filter_location = $_POST["filterLocation"];*/
$graph_filters = $_POST["graph_filters"];
$org_id = $_POST["orgId"];

$today = new DateTime();

if ($input[0] == 'vector_data_org'){
$output = vector_data_org($org_id,$graph_filters,$today->format('Y-m-d'));
echo json_encode($output);
} 


if ($input[0] == 'graph_filter_call'){
$output = graph_filter_call($org_id,$graph_filters["gender"], $graph_filters["tenure"],$graph_filters["department"], $graph_filters["location"],$today->format('Y-m-d'));
echo json_encode($output);
} 

if ($input[0] == 'graph_filter_timeline'){

$result =[];

$creation_ts = new DateTime(get_orgid_from_id($org_id)['creation_ts']);
$month_diff= $creation_ts->diff($today);

for($i=0; $i<$month_diff->m;$i++)
{
	$creation_ts->modify('first day of next month');
	$result[] = graph_filter_call_date($org_id,$graph_filters["gender"], 
	$graph_filters["tenure"],$graph_filters["department"], $graph_filters["location"],$creation_ts->format('Y-m-d'));

}

$result[] = graph_filter_call_date($org_id,$graph_filters["gender"], 
	$graph_filters["tenure"],$graph_filters["department"], $graph_filters["location"],$today->format('Y-m-d'));
echo json_encode($result);
} 

//--------------------------------- Factories ---------------------------------
function graph_filter_call($org_id,$filter_gender,$filter_tenure, $filter_department, $filter_location, $today){
   //return get_avg_all_profiles_filtered($org_id,$filter_gender,$filter_tenure, $filter_department, $filter_location);
	return get_all_profiles_filtered_date($org_id,$filter_gender,$filter_tenure, $filter_department, $filter_location, $today);
}

function graph_filter_call_date($org_id,$filter_gender,$filter_tenure, $filter_department, $filter_location, $date){
   //return get_avg_all_profiles_filtered($org_id,$filter_gender,$filter_tenure, $filter_department, $filter_location);
	return get_all_profiles_filtered_date($org_id,$filter_gender,$filter_tenure, $filter_department, $filter_location,$date);
}

function vector_data_org($org_id,$graph_filters,$date){
	return get_vector_data_org($org_id,$graph_filters,$date);
}

?>