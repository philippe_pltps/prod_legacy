$(function() {
var input_plus = document.getElementById("input_plus");
var input_minus = document.getElementById("input_minus");
  //$(".button_spot").append('<div class="inc button_points"  data-pltps="+"></div><div class="dec button_points"  data-pltps="-"></div>');

  var $points = 30;
  var click_plus = click_minus = 0;

  $(".button_points").on("click", function() {

    var $button = $(this);
    var oldValue = $button.parent().find("input").val();
   // console.log($button.data("pltps"));

    if ($button.data("pltps") == "+" ) {
    	if($points>0)
    	{
    		var newVal = parseFloat(oldValue) + 1;
  	  		$points = $points - 1;		
  	  		click_plus ++;
    	}  	  
  	} else {
	   // Don't allow decrementing below zero
	      if (oldValue > 0) {
	        var newVal = parseFloat(oldValue) - 1;
	        $points = $points + 1;
	        click_minus++;
		    } else {
	        newVal = 0;
	      }
	  }
	  if(newVal != null)
	  {
	  	$button.parent().find("input").val(newVal);
	  }
	  document.getElementById("span_points").textContent=$points;
	  //document.getElementById("span_points_left").textContent=$points;

	  if($points<=0){
		$("#button_submit").addClass('button-3');
		$("#button_submit").removeClass('button-disabled');   
		$('#button_submit').prop('disabled', false);
	  } else {
	  	$("#button_submit").addClass('button-disabled');   	
	  	$("#button_submit").removeClass('button-3');
	  	$('#button_submit').prop('disabled', true);
	  }
    input_plus.value = click_plus;
    input_minus.value = click_minus;

  });
});
