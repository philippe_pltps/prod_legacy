
Chart.defaults.global.defaultFontFamily = 'Poppins';

        var defaultBarChartData={
            datasets: [{
              data:[ 1,2,3,4,5,6,7,8,9,10,11,12],
        backgroundColor: ["#def4e5",
            "#81dda0",
            "#13c44f",
            "#fbf3e1",
            "#fbd68f",
            "#fbb222",
            "#e8dbf9",
            "#c7a6fe",
            "#9c5fff",
            "#c8eef5",
            "#86dcef",
            "#14bfe4"],
        
        borderWidth: 1
        //,label: 'Differences' // for legend
        }],
        labels: ["Communication","Teamwork","Structure","Flexibility","Perks","Diversity","Mission","Career","Compensation","Leadership","Development","Social"]
        };

var vectorColor = Array();
vectorColor['communication'] = "#def4e5";
vectorColor['teamwork'] = "#81dda0";
vectorColor['structure'] = "#13c44f";
vectorColor['flexibility'] = "#fbf3e1";
vectorColor['perks'] = "#fbd68f";
vectorColor['diversity'] = "#fbb222";
vectorColor['mission'] = "#e8dbf9";
vectorColor['career'] = "#c7a6fe";
vectorColor['compensation'] = "#9c5fff";
vectorColor['leadership'] = "#c8eef5";
vectorColor['development'] = "#86dcef";
vectorColor['social'] = "#14bfe4";




var defaultBarChartOption={
          type: 'horizontalBar',
          data: defaultBarChartData,
          options : 
          {
            legend: 
              {
              display: false
              },
            scales: 
            {
              xAxes: [{
                  ticks: {
                      beginAtZero: false,
                    max: 8,
                    min: 0,
                    stepSize: 1,
                    display:true
                  },
                  display:false
              }]
            },
            tooltips: {
              backgroundColor:'rgba(255, 255, 255, 0.85)',
              titleFontColor:'rgba(0, 0, 0, 1)',
              bodyFontColor:'rgba(0, 0, 0, 1)',
            }
           /* responsive: true,
            legend: 
              {
              display: false
              },
            scale: 
            {
              display: false,

              ticks: 
               {
                  display: false
               }
            }*/
          }
        };
        //var barCanvas = document.getElementById("barCompChart");
        



var list_bar_chart=Array();
var list_bar_chart_option=Array();
var list_bar_data =Array();

function create_bar_chart(canvas_name){
//var marksCanvas = document.getElementById("marksChart");
var barCanvas = document.getElementById(canvas_name);
//ad a default data to the list as data not reference
list_bar_chart_option.push(JSON.parse(JSON.stringify(defaultBarChartOption)));
list_bar_data.push(JSON.parse(JSON.stringify(defaultBarChartData)));

list_bar_chart_option[list_bar_chart_option.length-1].data = list_bar_data[list_bar_data.length-1];

var barChart = new Chart(barCanvas, list_bar_chart_option[list_bar_chart_option.length-1]);

list_bar_chart.push(barChart);

}


function resize_all_bar_charts(){
          for (var i=0; i< list_bar_chart.length; i++)
          {
            list_bar_chart[i].resize();    
          }
        }

function replace_bar_dataset(chart_id,vector_data, sorted_array)
        {
            //marksData.labels.push("arf");
            //

            /*list_bar_data[chart_id].datasets[0].data.pop();
            list_bar_data[chart_id].labels.pop();
            list_bar_data[chart_id].datasets[0].backgroundColor.pop();*/
            var data2 = Array();
            var label2 = Array();
            var bckColor2=Array();
            for(var vector in sorted_array){
             label2.push(sorted_array[vector]) // this is each vector
             data2.push(vector_data[sorted_array[vector]]) // each points
             bckColor2.push(vectorColor[sorted_array[vector]]);
             
            }
            list_bar_data[chart_id].datasets[0].data = data2;
            list_bar_data[chart_id].labels = label2;
            list_bar_data[chart_id].datasets[0].backgroundColor=bckColor2;
            list_bar_chart[chart_id].update();
        }

        function add_bar_dataset (chart_id, vector_data)

        {
          sorted_array = getSortedVectors(vector_data);
            //marksData.labels.push("arf");
            //

            /*list_bar_data[chart_id].datasets[0].data.pop();
            list_bar_data[chart_id].labels.pop();
            list_bar_data[chart_id].datasets[0].backgroundColor.pop();*/
            var data2 = Array();
            var bckColor2=Array();
            for(var vector in sorted_array){
             data2.push(vector_data[sorted_array[vector]]) // each points
             bckColor2.push(vectorColor[sorted_array[vector]]);
            }
            console.log(data2);
            
            list_bar_data[chart_id].datasets.push(defaultBarChartData.datasets);
            list_bar_data[chart_id].datasets[1].data = data2;
            list_bar_data[chart_id].datasets[1].backgroundColor = list_bar_data[chart_id].datasets[0].backgroundColor;
            //list_bar_data[chart_id].datasets.push(bckColor2);
            list_bar_chart[chart_id].update();
        }
