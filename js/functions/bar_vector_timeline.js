
Chart.defaults.global.defaultFontFamily = 'Poppins';

        var defaultVectorBarChartData={
            datasets: [{
              data:[ 1,2,3,4],
        backgroundColor: "#def4e5",
        borderWidth: 1
        //,label: 'Differences' // for legend
        }],
        labels: ["01-01-2019", "01-02-2019", "01-03-2019", "01-04-2019"]
        };


var defaultVectorBarChartOption={
  type:'bar',
          data: defaultVectorBarChartData,
          options : 
          {
            legend: 
              {
              display: false
              },
            tooltips: {
              backgroundColor:'rgba(255, 255, 255, 0.85)',
              titleFontColor:'rgba(0, 0, 0, 1)',
              bodyFontColor:'rgba(0, 0, 0, 1)',
            },
            scales: 
            {
              yAxes: [{
                  ticks: {
                      beginAtZero: false,
                    max: 8,
                    min: 0,
                    stepSize: 1,
                    display:true
                  },
                  display:true
              }]
            },

            
          }
        };
        var vectorBarCanvas = document.getElementById("timelineVectorBarChart");
        var vectorBarChart = new Chart(vectorBarCanvas, defaultVectorBarChartOption);
        
function replace_timeline_bar_dataset(selected_vector,timeline_array)
        {
            let new_data = Array();
            let new_labels = Array();
            
            for(let vector in timeline_array){
             new_data.push(timeline_array[vector][selected_vector]);
             new_labels.push(timeline_array[vector].timeline_date) ;
            }

            defaultVectorBarChartData.datasets[0].backgroundColor=vectorColor[selected_vector];
            defaultVectorBarChartData.labels=new_labels
            defaultVectorBarChartData.datasets[0].data = new_data;

            vectorBarChart.update();
        }
