
        var defaultData={
            datasets: [{
              data:[
              1,2,3,4,5,6,7,8,9,10,11,12],
              borderWidth:[0,0,0,0,0,0,0,0,0,0,0,0],
        backgroundColor: [
            "#def4e5",
            "#81dda0",
            "#13c44f",
            "#fbf3e1",
            "#fbd68f",
            "#fbb222",
            "#e8dbf9",
            "#c7a6fe",
            "#9c5fff",
            "#c8eef5",
            "#86dcef",
            "#14bfe4"
        ],
        label: 'My dataset' // for legend
    }],
    labels: [
        "Communication",
        "Teamwork",
        "Structure",
        "Flexibility",
        "Perks",
        "Diversity",
        "Mission",
        "Career",
        "Compensation",
        "Leadership",
        "Development",
        "Social"
    ]
        };


var defaultOption={
          type: 'polarArea',
          options : 
          {
            responsive: true,
            legend: {
            display: false
            },
            scale: 
              {
                display: false,

                 ticks: {
                    display: false
                 }
              },

          
            hover: {
              onHover: function(evt, item) {
                //var item = myLineChart.getElementAtEvent(evt);
                //console.log(item.length);
                console.log(item[0]._index );
                if(div_definition != null)
                {
                  if(item.length)
                  {
                    div_definition.innerHTML = vectorDefinition[item[0]._index];
                  } else {
                    div_definition.innerHTML = "<br>";
                  }
                } else {

                  /*if(page != null)
                  {
                    $('.reflection_vector').removeClass('vector_hover');
                    $('#vector_'+item[0]._index).addClass('arf');
                  }*/
                }
              }
            }
          }
        };
var list_chart=Array();
var list_chart_option=Array();
var list_data =Array();
var list_factor_sum =Array();
var current_page='';

function create_chart(canvas_name, option){
//var marksCanvas = document.getElementById("marksChart");
var marksCanvas = document.getElementById(canvas_name);
//ad a default data to the list as data not reference
list_data.push(JSON.parse(JSON.stringify(defaultData)));
list_factor_sum.push(0);
//set options with preferences eventualy
reset_options(list_chart_option.length,option);

var radarChart = new Chart(marksCanvas, list_chart_option[list_chart_option.length-1]);

list_chart.push(radarChart);

}


function reset_options(current_id,option)
        {
          var new_options={
          type: 'polarArea',
          data:list_data[current_id],
          options : 
          {
            responsive: true,
            legend: {
            display: false
            },
            scale: 
              {
                display: false,
                 ticks: {
                    display: false,
                    beginAtZero: true,
                    //max: 6,
                    min: 0,
                    stepSize: 1
                 }
              },
              animation:{
                animateRotate:false,
                animateScale:true
              },
           /* scaleOverride:true,
            scaleSteps:10,
            scaleStartValue:0,
            scaleStepWidth:1,*/
          
            hover: {
              onHover: function(evt, item) {
                //var item = myLineChart.getElementAtEvent(evt);
                //console.log(item.length);
                if(div_definition != null)
                {
                  if(item.length)
                  {
                    div_definition.innerHTML = vectorDefinition[item[0]._index];
                  } else {
                    div_definition.innerHTML = "<br>";
                  }
                } else {
                  if(current_page!=undefined && current_page=='step4')
                  {
                    $('.reflection_vector').removeClass('vector_hover');
                    if(item.length)
                    {
                      $('#vector_'+item[0]._index).addClass('vector_hover');
                    }
                  }
                }
              }
            },
            tooltips: {
              backgroundColor:'rgba(255, 255, 255, 0.85)',
              titleFontColor:'rgba(0, 0, 0, 1)',
              bodyFontColor:'rgba(0, 0, 0, 1)',
            },
            onClick: function(c,i) {
                e = i[0];
                if(current_page=='culture_timeline' && i.length>0) 
                {
                  //see org_demo_culture
                  polar_culture_click(e._index);  
                }
                
            }
          }
        };
        let split = option.split('_');
          if(split[0]=="nonumbers")
          {
            new_options.options.tooltips.callbacks = {
              
                title: function(tooltipItem, data) {
                  return data['labels'][tooltipItem[0]['index']];
                },
                label: function(tooltipItem, data) {
                  //return data['datasets'][0]['data'][tooltipItem['index']];
                  return "";
                },
                afterLabel: function(tooltipItem, data) {
                  /*var dataset = data['datasets'][0];
                  var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
                  return '(' + percent + '%)';*/
                  return "";
                }
              
            };
          }
          if(split[1]!=null)
          {
            new_options.options.scale.ticks['max']=Number(split[1]);

            //new_options.options.scale.ticks.push({max:6});
            //new_options.options.scale.ticks.max = split[1];
          } 
          list_chart_option.push( new_options); 
        }
        
        function resize_all_charts(){
          for (var i=0; i< list_chart.length; i++)
          {
            list_chart[i].resize();    
          }
        }

        function replace_dataset(chart_id,array_data)
        {
            //marksData.labels.push("arf");
            //data['count_profiles']
            //console.log(array_data['count_profiles']);
            if(array_data['count_profiles']>=0)
            {
              //warning this is without the factor_sum aka only for demo
              list_factor_sum[chart_id]= Number(array_data['count_profiles']);
            } 
            
          //list_data[chart_id].datasets[0].data.pop();
          list_data[chart_id].datasets[0].data = transformVectorToIndex(array_data);  
          //try to reset the dafault options
          //reset_options(chart_id,"option");

          list_chart[chart_id].update();        
        }

        function transformVectorToIndex(array_data)
        {
          let toReturn = [array_data.communication, array_data.teamwork ,array_data.structure, 
                    array_data.flexibility ,array_data.perks, array_data.diversity ,array_data.mission
                            , array_data.career ,array_data.compensation, array_data.leadership
                            , array_data.development, array_data.social];
          for(let index in toReturn)
          {
            toReturn[index] = Number(toReturn[index]);
          }
          return toReturn;
        }

        //in use when add one profile to the full one
        function replace_dataset_merge(chart_id,data_id, to_add_data_id, factor_to_add)
        {

            //marksData.labels.push("arf");
            //list_data[to_add_data_id].datasets[0].data
            for(let index in list_data[to_add_data_id].datasets[0].data)
            {
              list_data[to_add_data_id].datasets[0].data[index] = Number(list_data[to_add_data_id].datasets[0].data[index]);
              list_data[data_id].datasets[0].data[index] = Number(list_data[data_id].datasets[0].data[index]);
            }
            //console.log(list_data[to_add_data_id].datasets[0].data);
            //console.log(list_data[data_id].datasets[0].data);

          let data2=[];
          for(let index in list_data[data_id].datasets[0].data)
          {
            data2[index]=Number( ( (list_data[data_id].datasets[0].data[index]*list_factor_sum[data_id]+(list_data[to_add_data_id].datasets[0].data[index]*factor_to_add))/(list_factor_sum[data_id]+factor_to_add)).toFixed(2) );
          }
          list_factor_sum[chart_id] = list_factor_sum[data_id]+1;
          list_data[chart_id].datasets[0].data = data2;

          list_chart[chart_id].update();        
        }

        function replace_dataset_plus_array(chart_id,data_id, array_to_add, factor_to_add)
        {

            //marksData.labels.push("arf");
            //list_data[to_add_data_id].datasets[0].data
            let data_to_add = transformVectorToIndex(array_to_add);
                        //console.log(list_data[data_id].datasets[0].data);
                        //console.log(factor_to_add);
            //list_data[chart_id].datasets[0].data.pop();
            let data2=[];
            for(let index in list_data[data_id].datasets[0].data)
            {
              data2[index]=Number( ( (list_data[data_id].datasets[0].data[index]*list_factor_sum[data_id]+(data_to_add[index]*factor_to_add))/(list_factor_sum[data_id]+factor_to_add)).toFixed(2) );
            }
            list_factor_sum[chart_id] = list_factor_sum[data_id]+factor_to_add;
            list_data[chart_id].datasets[0].data = data2;

            list_chart[chart_id].update();        
        }


        function replace_data(chart_id,data)
        {
            //marksData.labels.push("arf");
            //

          list_data[chart_id].datasets[0].data.pop();
          list_data[chart_id].datasets[0].data = data;               
          list_chart[chart_id].update();        
        }

        function add_dataset(chart_id, array_data)
        {
          var data2= {
              data:[array_data.communication, array_data.teamwork , array_data.structure, 
          array_data.flexibility ,array_data.perks, array_data.diversity ,array_data.mission
                  , array_data.career ,array_data.compensation, array_data.leadership
                  , array_data.development, array_data.social],
          backgroundColor: [
              "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)",
            "rgba(180, 180, 180, 0.8)"
          ],
         // borderWidth:[0,0,0,1,1,1,0,0,0,0,0,0],
          label: 'My dataset' // for legend
          };     
          list_data[chart_id].datasets.push(data2);               
          list_chart[chart_id].update();             

                
        let backgroundColor2= [
            "rgba(222, 244, 229, 0.7)",
            "rgba(129, 221, 160, 0.7)",
            "rgba(19, 196, 79, 0.7)",
            "rgba(251, 243, 225, 0.7)",
            "rgba(251, 214, 143, 0.7)",
            "rgba(251, 178, 34, 0.7)",
            "#e8dbf9",
            "#c7a6fe",
            "#9c5fff",
            "#c8eef5",
            "#86dcef",
            "#14bfe4"
        ]
        list_data[0].datasets[0].backgroundColor = backgroundColor2;
        //console.log(list_chart[0].options);

        let scale=
              {
                display: true,
                 ticks: {
                    display: true,
                    beginAtZero: true,
                    //max: 6,
                    min: 0,
                    stepSize: 1
                 }
              };
          list_chart[0].options.scale = scale;
          list_chart[0].update();
        }
        
        

