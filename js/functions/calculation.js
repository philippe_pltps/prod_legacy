
//support functions

//get max value in a key array
function getMaxValue(array_data)
{
	toReturn=0;
	for(var field in array_data)
	{
		if(array_data[field] > toReturn)
		{
			toReturn = array_data[field];
		}
	}
	return toReturn;
}

function getMaxNumber(array_data, number)
{
	let toReturn=[];
	let handle =0;
	let counter=0;
	//loop through the fields
	for(var field in array_data)
	{
		if(field =='communication' || 
					field =='teamwork' || 
					field == 'structure' ||
					field == 'flexibility' ||
					field =='perks' || 
					field == 'diversity' ||
					field == 'mission' ||
					field =='career' || 
					field == 'compensation' ||
					field == 'leadership' ||
					field =='development' || 
					field == 'social')
		{
			handle = Number(array_data[field]);
			//first fill up the returnh array
			if(counter<number)
			{
				toReturn[field] = handle;
				counter++;
			} else {

				//then check if the value is higher than the lowest one and if true, replace it !
				let min_val = getMinValue(toReturn);

				if(handle > min_val)
				{
					delete toReturn[getIdFromValue(toReturn,min_val)];	

					toReturn[field] = handle;
				}
			}
		}	
	}
	return toReturn;
}

//get min value in a key array
function getMinValue(array_data)
{
	toReturn=10000;
	for(var field in array_data)
	{
		if(array_data[field] < toReturn)
		{
			toReturn = array_data[field];
		}
	}
	return toReturn;
}

//search the first vector name from a value
function getIdFromValue(array_data, value)
{
	for(var field in array_data)
	{
		if(array_data[field]===value)
		{
			return field;
		}
	}
}

//last trqnsformation with decimal and eventual checks
function lastCheck(result)
{
	toReturn = 0;
		if(result>100)
			{
				result = 100;
			}
		if(result<0)
		{
			result = 0;
		}
	toReturn = result.toFixed(2);
	return toReturn;
}

/***************************************Match possibilities***********************************/
/*So far array_data2 is the aggregaded company data while array_data1 is the individual*/



/*****
This match function is the standard with top 3 with a factor 3 and low 3 with a factor 1/3
*****/
function getMatchTop3_Emphasis(array_data1, array_data2)
		{
			var toReturn = 0;
			var percent =0;
			var counter=0;
			var handle_1=0;
			var handle_2=0;
			var factor=1;

			var max_values=[];
			var min_values=[];

			var buffer=0;
			//get values not references
			var array_aggregated = JSON.parse(JSON.stringify(array_data2));
			//remove the items that could interfere with the max and min
			delete array_aggregated["count_profiles"];	
			delete array_aggregated["factor_sum"];	
			//first determine the top 3 value and put them in a separate array
			for(var i =0; i<3;i++)
			{
				
				//alert(array_data1.values());
				buffer = getMaxValue(array_aggregated); 
				max_values[getIdFromValue(array_aggregated,buffer)]= buffer;
				delete array_aggregated[getIdFromValue(array_aggregated,buffer)];	
			}
			for(var i =0; i<3;i++)
			{
				
				buffer = getMinValue(array_aggregated); 
				min_values[getIdFromValue(array_aggregated,buffer)]= buffer;
				delete array_aggregated[getIdFromValue(array_aggregated,buffer)];	
			}
			//put them back in the array2
			for(var field in max_values)
			{
				array_aggregated[field] = max_values[field];
			}
			for(var field in min_values)
			{
				array_aggregated[field] = min_values[field];
			}


			for (var field in array_data1){
				if(field =='communication' || 
					field =='teamwork' || 
					field == 'structure' ||
					field == 'flexibility' ||
					field =='perks' || 
					field == 'diversity' ||
					field == 'mission' ||
					field =='career' || 
					field == 'compensation' ||
					field == 'leadership' ||
					field =='development' || 
					field == 'social')
				{
					factor = 1;//important factors have move coefficiet
					if(max_values[field]>0){
						factor = 3;

					} //les important factors have less coefficient
					if(min_values[field]>0){
						factor =1/4;
					} 		

					
					handle_1 = Number(array_data1[field]);
					handle_2 = Number(array_aggregated[field]);	

					//determine factor
					//if candidate care a lot compare to the company this has a strong negative impact
					//it is proportionate to how much the candidate value that vactor
					if(handle_1 > handle_2 *2)
					{
						factor =factor *  -1 * ((handle_1/handle_2)*0.6);
					} else 
					{//the other way around if a company is strong and candidate does not care, this has a smaller impact
						if(handle_2 > handle_1 * 2)
						{
							factor =factor * 0.8;
						}
					}
					
					percent =0;
					


						if(handle_1==0 && handle_2==0)		
						{
							percent = 100;
						} 			
					if(handle_1 >= handle_2 && handle_1  != 0)
			          {
			            percent = 100 - ((handle_1- handle_2) * 100 / handle_1);			            
			          }
			        if(handle_1 < handle_2 && handle_2 != 0)
			          {
			           percent = 100 - ((handle_2 - handle_1) * 100 / handle_2); 
			          }
					toReturn = toReturn + (percent*factor);	
					counter+=Math.abs(factor);
					
				}				
			} 
			toReturn = toReturn/counter;
			
			return lastCheck(toReturn);
		}


function getMatchTriple(array_data1, array_data2)
		{
			//define how many vectors should we concider for the first step
			let top_max_number=5;
			let pct_top=0.4;
			let pct_fam=0.35;
			let pct_pts=0.25;
			let top_present_pool=84;
			let top_order_pool=16;

			let toReturn = 0;
			let percent =0;
			let counter=0;
			let handle_1=0;
			let handle_2=0;
			let points_cal=family_cal=top_cal=0;
			let factor=1;
			let family_candidate=family_aggregated=[];
			let top_candidate=top_aggregated=[];

			let buffer=0;
			//get values not references
			let array_aggregated = JSON.parse(JSON.stringify(array_data2));
			let array_candidate = JSON.parse(JSON.stringify(array_data1));
			//remove the items that could interfere with the max and min
			delete array_aggregated["count_profiles"];	
			delete array_aggregated["factor_sum"];	
			

			top_candidate = getSortedVectors(getMaxNumber(array_candidate, top_max_number));
			top_aggregated = getSortedVectors(getMaxNumber(array_aggregated, top_max_number));
			family_candidate = getSortedFamilies(getFamiliyPoints(array_candidate));
			family_aggregated = getSortedFamilies(getFamiliyPoints(array_aggregated));

			for( let index in top_candidate)
			{
				if(top_aggregated.includes(top_candidate[index]))
				{
					top_cal+=top_present_pool/top_max_number;
					if(top_aggregated[index]===top_candidate[index])
					{
						top_cal+=top_order_pool/top_max_number;
					}
				}
			}
			//console.log(top_cal,top_aggregated,top_candidate)

			for( let i=0;i<4;i++)
			{
				if(family_candidate[i]===family_aggregated[i])
				{
					family_cal+=25;
				}
			}


			for (let field in array_candidate)
			{
				if(field =='communication' || 
					field =='teamwork' || 
					field == 'structure' ||
					field == 'flexibility' ||
					field =='perks' || 
					field == 'diversity' ||
					field == 'mission' ||
					field =='career' || 
					field == 'compensation' ||
					field == 'leadership' ||
					field =='development' || 
					field == 'social')
				{
					factor = 1;				
					handle_1 = Number(array_candidate[field]);
					handle_2 = Number(array_aggregated[field]);	
					//determine factor
					//if candidate care a lot compare to the company this has a strong negative impact
					//it is proportionate to how much the candidate value that vactor
					if(handle_1 > handle_2 *1.8)
					{
						factor =factor *  -1 * ((handle_1/handle_2)*0.6);
					} else 
					{//the other way around if a company is strong and candidate does not care, this has a smaller impact
						if(handle_2 > handle_1 * 2)
						{
							factor =factor * 0.8;
						}
					}					
					percent =0;
					if(handle_1==0 && handle_2==0)		
					{
						percent = 100;
					} 			
					if(handle_1 >= handle_2 && handle_1  != 0)
			          {
			            percent = 100 - ((handle_1- handle_2) * 100 / handle_1);			            
			          }
			        if(handle_1 < handle_2 && handle_2 != 0)
			          {
			           percent = 100 - ((handle_2 - handle_1) * 100 / handle_2); 
			          }
					points_cal = points_cal + (percent*factor);	
					counter+=Math.abs(factor);
				}				
			} 
			points_cal = points_cal/counter;
			//console.log(top_cal,family_cal,points_cal);
			toReturn = pct_top*top_cal + pct_fam*family_cal + pct_pts*points_cal;
			return lastCheck(toReturn);
		}

function getFamiliyPoints(array_data){
	let data_family={'people':0,'workplace':0,'organisation':0,'behaviour':0};
	let handle=0;
	for (let field in array_data)
			{
				handle = Number(array_data[field]);
				switch(field){
					case 'communication': data_family.people =data_family.people + handle;
					break;
					case 'teamwork':  data_family.people =data_family.people + handle;
					break;
					case 'structure':  data_family.people =data_family.people + handle;
					break;
					case 'flexibility':  data_family.workplace =data_family.workplace + handle;
					break;
					case 'perks':  data_family.workplace =data_family.workplace + handle;
					break;
					case 'diversity':  data_family.workplace =data_family.workplace + handle;
					break;
					case 'mission':  data_family.organisation =data_family.organisation + handle;
					break;
					case 'career':  data_family.organisation =data_family.organisation + handle;
					break;
					case 'compensation':  data_family.organisation =data_family.organisation + handle;
					break;
					case 'leadership':  data_family.behaviour =data_family.behaviour + handle;
					break;
					case 'development':  data_family.behaviour =data_family.behaviour + handle;
					break;
					case 'social':  data_family.behaviour =data_family.behaviour + handle;
					break;
				}
			}
			return data_family;			
}


function getMatchTriple_distance(array_data1, array_data2)
		{

			//define how many vectors should we concider for the first step
			let top_max_number=5;
			let pct_top=0.7;
			let pct_fam=0.2;
			let pct_pts=0.1;
			
			// 1/ 6 * 6 * 12
			//let distance_factor = 100/(6*6*12);
			
			//distance in ordering
			let distance_limit = 6;
			let distance_factor = 100/(distance_limit*12);
			//let distance_factor = 100/(Math.sqrt(distance_limit)*12);

			
			//distance in points
			//let distance_family_factor = 100/ 4 * 2;
			let distance_family_factor = 100/ (4 * Math.sqrt(2));

			let toReturn = 0;
			let percent =0;
			let counter=0;
			let handle_1=0;
			let handle_2=0;
			let points_cal=family_cal=top_cal=0;
			let factor=1;
			let family_candidate=family_aggregated=[];
			let order_candidate=order_aggregated=[];

			let buffer=0;
			//get values not references
			let array_aggregated = JSON.parse(JSON.stringify(array_data2));
			let array_candidate = JSON.parse(JSON.stringify(array_data1));
			//remove the items that could interfere with the max and min
			delete array_aggregated["count_profiles"];	
			delete array_aggregated["factor_sum"];	
			

			
			order_candidate = getSortedVectors(array_candidate);
			order_aggregated= getSortedVectors(array_aggregated);
			
			family_candidate = getFamiliyPoints(array_candidate);
			family_aggregated = getFamiliyPoints(array_aggregated);
			let is_filter = false;
			//console.log(order_candidate, order_aggregated);
			for( let index in order_candidate)
			{
				is_filter=false;
				for(let filter_index in candidate_filters)
					{
						if(candidate_filters[filter_index]['vector']==order_candidate[index])
						{

							is_filter=true;
						}
					}
								
				if(!is_filter)
				{
					let vector_rank_cand = getIdFromValue(order_candidate,order_candidate[index]);
					let vector_rank_agg  = getIdFromValue(order_aggregated,order_candidate[index]);
					top_cal+= (Math.max(0, distance_limit-Math.abs(vector_rank_cand-vector_rank_agg))*distance_factor);
				} else { //give half point for the ignored one, could be debated as it will positivbely impact everybody
					top_cal+=distance_factor/2;
				}
				//top_cal+= (Math.pow(Math.max(0, 6-Math.abs(vector_rank_cand-vector_rank_agg)),2)*distance_factor);
				//top_cal+= (Math.sqrt(Math.max(0, distance_limit-Math.abs(vector_rank_cand-vector_rank_agg)))*distance_factor);
			}

			for(let field in family_candidate)
			{
				//console.log(field,family_candidate[field],family_aggregated[field],Math.max(0,2-Math.abs(family_candidate[field]-family_aggregated[field])));
				//family_cal+=Math.max(0,2-Math.abs(family_candidate[field]-family_aggregated[field])) * distance_family_factor;
				family_cal+=Math.sqrt(Math.max(0,2-Math.abs(family_candidate[field]-family_aggregated[field]))) * distance_family_factor;


			}
	
			
			for (let field in array_candidate)
			{
				is_filter=false;
				if((field =='communication' || 
					field =='teamwork' || 
					field == 'structure' ||
					field == 'flexibility' ||
					field =='perks' || 
					field == 'diversity' ||
					field == 'mission' ||
					field =='career' || 
					field == 'compensation' ||
					field == 'leadership' ||
					field =='development' || 
					field == 'social') )
				{
					for(let index in candidate_filters)
					{
						if(candidate_filters[index]['vector']==field)
							is_filter=true;
					}
					if(!is_filter)
					{
						//check if(jQuery.inArray(value,array) != -1)
					factor = 1;				
					handle_1 = Number(array_candidate[field]);
					handle_2 = Number(array_aggregated[field]);	
					//avoid divide by 0 
					if(handle_2==0)
					{
						handle_2=0.0001;
					}

					//determine factor
					//if candidate care a lot compare to the company this has a strong negative impact
					//it is proportionate to how much the candidate value that vactor

					if(handle_1 > handle_2 *1.8 )
					{
						factor =factor *  -1 * ((handle_1/handle_2)*0.6);
					} else 
					{//the other way around if a company is strong and candidate does not care, this has a smaller impact
						if(handle_2 > handle_1 * 2)
						{
							factor =factor * 0.8;
						}
					}					
					percent =0;
					if(handle_1==0 && handle_2==0)		
					{
						percent = 100;
					} 			
					if(handle_1 >= handle_2 && handle_1  != 0)
			          {
			            percent = 100 - ((handle_1- handle_2) * 100 / handle_1);			            
			          }
			        if(handle_1 < handle_2 && handle_2 != 0)
			          {
			           percent = 100 - ((handle_2 - handle_1) * 100 / handle_2); 
			          }
					points_cal = points_cal + (percent*factor);	
					counter+=Math.abs(factor);
					}
					
				}				
			} 
			points_cal = points_cal/counter;
				//if(array_candidate.compensation==4.67 && array_candidate.social==4.33)
				//console.log(top_cal,family_cal,points_cal);
			toReturn = pct_top*top_cal + pct_fam*family_cal + pct_pts*points_cal;
			return lastCheck(toReturn);
		}