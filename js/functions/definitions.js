

var vectorDefinition = [
'Clear & effective, courteous and proactive - via multiple channels',
'Whether independent or collaborative in nature - between individuals, teams and departments',
'A Clear and defined Organisational Structure and Hierarchy - regardless of type: Entrepreneurial, Professional, Divisional or Innovative',
'The option of flexible working hours and ability to work remotely or from home',
'Access to office comforts, healthy food and snacks as well as mental and physical programs (Yoga, crossfit etc)',
'Accept each individual\'s differences, embraces their strengths and provides opportunities for all staff to achieve their full potential',
'Visible and clear company values and Mission',
'Moving forward, being promoted, finding new challenges, opportunities to "climb the ladder" within the Organisation',
'Access to financial incentives whether commission or bonuses, salary reviews and Pensions',
'Visible and Accessible Leaders within an Organisation',
'Opportunities for internal and external personal development - new skills and learning programs',
'Group activities, social events based on shared interests'];
var family_definitions=
{families:
	[
		{
		name:'people',
		definition:''		
		},
		{
		name:'workplace',
		definition:''		
		},
		{
		name:'organisation',
		definition:''		
		},
		{
		name:'behaviour',
		definition:''		
		}
	]
};
var vector_definitions= 
{vectors:[ 
		{
			family:0,
			name:'communication',
			definition:'Whether independent or collaborative in nature - between individuals, teams and departments'
		},
		{
			family:0,
			name:'teamwork',
			definition:'Clear & effective, courteous and proactive - via multiple channels'
		},
		{
			family:0,
			name:'structure',
			definition:'A Clear and defined Organisational Structure and Hierarchy - regardless of type: Entrepreneurial, Professional, Divisional or Innovative'
		},
		{
			family:1,
			name:'flexibility',
			definition:'The option of flexible working hours and ability to work remotely or from home'
		},
		{
			family:1,
			name:'perks',
			definition:'Access to office comforts, healthy food and snacks as well as mental and physical programs (Yoga, crossfit etc)'
		},
		{
			family:1,
			name:'diversity',
			definition:'Accept each individual\'s differences, embraces their strengths and provides opportunities for all staff to achieve their full potential'
		},
		{
			family:2,
			name:'mission',
			definition:'Visible and clear company values and Mission'
		},
		{
			family:2,
			name:'career',
			definition:'Moving forward, being promoted, finding new challenges, opportunities to "climb the ladder" within the Organisation'
		},
		{
			family:2,
			name:'compensation',
			definition:'Access to financial incentives whether commission or bonuses, salary reviews and Pensions'
		},
		{
			family:3,
			name:'leadership',
			definition:'Visible and Accessible Leaders within an Organisation'
		},
		{
			family:3,
			name:'development',
			definition:'Opportunities for internal and external personal development - new skills and learning programs'
		},
		{
			family:3,
			name:'social',
			definition:'Group activities, social events based on shared interests'
		}
	]
};

function get_definition(vector_name)
{
	if(vector_name!=undefined)
	{for(let index in vector_definitions['vectors'])
		{
			if(vector_definitions['vectors'][index]['name']==vector_name.toLowerCase())
			{
				return vector_definitions['vectors'][index]['definition'];
			}
		}
	} else {
		return '';
	}
	
}

var div_definition = document.getElementById("div_definition");



