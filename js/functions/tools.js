//function to add elements
function addElement(parentId, elementTag, elementId, html) {
    // Adds an element to the document
    var p = document.getElementById(parentId);
    var newElement = document.createElement(elementTag);
    newElement.setAttribute('id', elementId);
    newElement.innerHTML = html;
    p.appendChild(newElement);
}

function addElementWithClass(parentId, elementTag, elementId, html, class_name) {
    // Adds an element to the document
    var p = document.getElementById(parentId);
    var newElement = document.createElement(elementTag);
    newElement.setAttribute('id', elementId);
    newElement.setAttribute('class', class_name);
    newElement.innerHTML = html;
    p.appendChild(newElement);
}

//return an arrray with the different vectors by the biggest one to the lowest one
//also remove anything else
function getSortedVectors(obj) {
	//remove any items which could interefere
    var keys = []; for(var key in obj) {
    	if(key=="communication" ||key=="mission" ||key=="leadership" ||key=="diversity" ||key=="development" ||key=="flexibility" ||key=="teamwork" ||key=="compensation" ||key=="social" ||key=="career" ||key=="perks" ||key=="structure")
    	keys.push(key);
    }
    return keys.sort(function(a,b){return obj[b]-obj[a]});
}

//return an arrray with the different vectors by the biggest one to the lowest one
//also remove anything else
function getSortedFamilies(obj) {
    //remove any items which could interefere
    var keys = []; for(var key in obj) {
        if(key=="people" ||key=="workplace" ||key=="organisation" ||key=="behaviour")
        keys.push(key);
    }
    return keys.sort(function(a,b){return obj[b]-obj[a]});
}

//used in org page
function getMin(obj1, obj2) {
    if(obj1>obj2){
        return obj2;
    } else {
        return obj1;
    }
}




//used in org page
//used to remove a filter by seraching the id in the list rather than just filterId which might be different if other filters has been removed before
function getIndexId(arrayToSearch,Id)
{
    //alert(arrayToSearch.length);
    for(var i=0;i<arrayToSearch.length;i++)
            {
                //alert(arrayToSearch[i]['id'] + " " +Id);
                if(arrayToSearch[i]['id']==Id)
                {
                    return i;
                }           
            }
}

//used in org page
//function to remove element
function removeElement(elementId) {
    // Removes an element from the document
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);

}


function get_id_from_value(array_data,value)
{
    for(let index in array_data)
    {
        if(array_data[index] == value)
        {
            return index;
        }
    }
    return -1;
}

function get_loc_dep_from_reg(array_data,reg)
{
    for(let index in array_data)
    {
        if(array_data[index]['role_name']==reg)
        {
            return {'location':array_data[index]['location'],'department':array_data[index]['department'] }
        }
    }
}


function funnel_list(main_select_id,main_attribute_name, slave_select_id,slave_attribute_name)
{
    let slave_str_id=slave_select_id;
    let selector = document.getElementById(main_select_id);
    buffer = $('#'+slave_str_id).val();
    //console.log(get_id_from_value($('#select_graph_dep option').map(function() {return $(this).val()}).get(),buffer));
    $('#'+slave_str_id).find('option').remove();
    if(selector.options[selector.selectedIndex].value=='All')
    {
        populate_select(removeDuplicatesInChild(parent_loc_dep),slave_str_id
        ,'All', slave_attribute_name);
    } else {
        populate_select(parent_loc_dep[selector.options[selector.selectedIndex].value]
        ,slave_str_id
        ,'All', slave_attribute_name);
    }
    //-1 is the value returned if not foudn in the array
    if(jQuery.inArray(buffer,$('#'+slave_str_id+' option').map(function() {return $(this).val()}).get()) != -1)
    {
        $('#'+slave_str_id).val(buffer);    
    } else {
        $('#'+slave_str_id).val('All'); 
    }
}

//used in demo page
function populate_select(list,select_id,default_value, default_name)
{
    //top item not selectable
    add_first_option_item(select_id,default_value,default_name);
    //item all selectable
    add_option_array_item("All", select_id);
    //the others
    for(let index in list)
    {
        if(default_name=='Location'||default_name=='location')
        {
            add_option_array_item(index, select_id)    
        } else {
            add_option_array_item(list[index], select_id)
        }    
    } 
}


//popualte the select for tenure from 0 to max tenure
function populateSelectTenure(select_tenure,max_tenure){
    var select = document.getElementById(select_tenure);
    let innerText='';
    for(var i=0;i<=max_tenure;i++)
    {
        var opt = document.createElement('option');
        opt.value = i;
        switch(i){
            case 0: innerText='< 1 yr';
            break;
            case 1: innerText='1 yr';
            break;
            default: innerText=i+' yrs';
            break;
        }
        if(i==(max_tenure))
        {
            innerText += ' +';
        }
        
        opt.innerHTML =   innerText;
        select.appendChild(opt);
    }
}

//used in demo page only for roles
function populate_select_roles(list,select_id,default_value, default_name)
{
    //top item not selectable
    add_first_option_item(select_id,default_value,default_name);
    //item all selectable
    //add_option_array_item("All", select_id);
    //the others
    for(let index in list)
    {
        add_option_array_item(list[index]['role_name'],select_id);
    } //$('#'+select_id).val(list[0]['role_name']);
}

//used in org page
function add_option_array_item(array_item,select_id){
    var select = document.getElementById(select_id);
    var opt = document.createElement('option');
        opt.value = array_item;
        opt.innerHTML = array_item; 
        select.appendChild(opt);
}

//used in org page
function add_option_item_distinct(item,select_id){
    var select = document.getElementById(select_id);
    var opt = document.createElement('option');
        opt.value = item.distinct;
        opt.innerHTML = item.distinct; 
        select.appendChild(opt);
}

//used in org page
function add_first_option_item(select_id,default_value,default_name){
    var select = document.getElementById(select_id);
    var opt = document.createElement('option');
        opt.value = default_value;
        opt.innerHTML = default_name;
        opt.disabled=true;    
        opt.selected=true;
        select.appendChild(opt);
}

//array_data is the list received
//reference is the value to check agains
//main is the list that will trigger the change
//slave is the list that will receive
//isbreak is to avoid more results if need be
function get_child_list(array_data, reference, main, slave)
{
    let list_return = [];
    //let buffer = [];
    for(let index in array_data)
    {
        if(array_data[index][main]==reference ||reference ==='All')
        {   
            list_return.push({'distinct':array_data[index][slave]});
        }
    }
    return list_return;
}


//array should be the main array to filter
//filter should be an array contaning all the filters under the form array[][vector,operator,value]
    function applyFilters(arrayToFilter,filters){
        console.log("applyFilters(super_cadidate_array,candidate_filters);-> V");
        if( filters.length>0)
        {
            let toReturn =[];
            let toAdd='yes';
            //loop through all elements of the array
            for(let i=0;i<arrayToFilter.length;i++)
            {
                toAdd='yes';
                //then lopp through all filters, if at least one fails then do not put the element in the return array
                for(let j=0; j<filters.length;j++)
                {
                    //search is a bit special because it is accross different attributes of the array
                    if(filters[j].vector!='search')
                    {
                        if(!singleFilter(arrayToFilter[i][filters[j].vector],filters[j].operator,filters[j].value))
                        {
                            toAdd='no';
                        }    
                    } else {
                        if(!searchFilter(arrayToFilter[i],filters[j].value))
                        {
                            toAdd='no';
                        }
                    }
                    
                }
                if(toAdd=='yes')
                {
                    toReturn.push(arrayToFilter[i]);
                }
            }
            return toReturn;     
        }
        else {
            return arrayToFilter;
        }
    }
//used to apply the basic operations
    function singleFilter( post, operator, value) {
        //currently operqtors != and == are only used by the loc dep role filters not the vectors

        if(operator != '!=' && operator !='==')
        {
            post= Number(post);
            value= Number(value);
        }
      switch (operator) {
        case '>':   return post > value;
        case '<':   return post < value;
        case '>=':  return post >= value;
        case '<=':  return post <= value;
        case '==':  return post == value;
        case '!=':  return post != value;
        case '===': return post === value;
        case '!==': return post !== value;
      }
    }

    function searchFilter(object, value)
    {
        if(value=='')
        {
            return true;
        } else {
            
            let match_name = object['name'].toLowerCase().indexOf(value.toLowerCase()) >= 0 ? true : false;
            let match_email = object['email'].toLowerCase().indexOf(value.toLowerCase()) >= 0 ? true : false;
            if(match_name || match_email)
            {
                return true;
            } else {
                return false;    
            }
            
        }
    }

//insert HTML if the right attribute w3-include-html set
function includeHTML() {
  var z, i, elmnt, file, xhttp;
  /* Loop through a collection of all HTML elements: */
  z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
    file = elmnt.getAttribute("w3-include-html");
    if (file) {
      /* Make an HTTP request using the attribute value as the file name: */
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {elmnt.innerHTML = this.responseText;}
          if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
          /* Remove the attribute, and call this function once more: */
          elmnt.removeAttribute("w3-include-html");
          includeHTML();
        }
      } 
      xhttp.open("GET", file, true);
      xhttp.send();
      /* Exit the function: */
      return;
    }
  }
}

//capitalise a string
function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function removeDuplicatesInChild(array_data) {
    let unique = [];
    for(let location in array_data)
    {
        for(let index in array_data[location])
        {
            if(!unique.includes(array_data[location][index]))
            {
                unique.push(array_data[location][index]);
            }
        }
    }
    return unique.sort(function(a,b){return unique[b]-unique[a]});
}
