//sorting super candidate by matching if defined
function sortArrayDesc(arrayToMath) {
  arrayToMath.sort(function(a, b){return b.match - a.match});
}
//repopulate the amtch to correspond to the new graph vector

function populateArrayMatch(arrayToMath, matching){
	for(let i=0;i<arrayToMath.length;i++)
	{		
		//arrayToMath[i]['match'] = getMatchTriple_distance(arrayToMath[i],graph_vectors);
		//TEST
		arrayToMath[i]['match'] = matching[i];
	}
	return arrayToMath;
}

function sort_with_select(array_data)
{
	let to_return =[];
	for(let i=1;i>=0;i--)
	{
		let array_buffer = get_all_is_selected(array_data,i);
		sortArrayDesc(array_buffer);
		to_return.push(...array_buffer);
	}
	return to_return;
}

function get_all_is_selected(array,value)
{
	let to_return =[];
	for(let i in array)
	{
		if(array[i].is_selected==value)
		{
			to_return.push(array[i]);
		}
	}
	return to_return;
}

//to change the mathing method
function changeMatching(org_id)
	{
		//actualize the graph
		graph_filter_call(0,org_id,graph_filters);		
	}


//onchange functions of each candidate filter. those are always: 0 location, 1 department, 2 role in the candidate filters list
//onchange functions of each candidate filter. those are always: 0 location, 1 department, 2 role in the candidate filters list
function changeCandidateFilter(candidate_filter,element) {
	var selector = document.getElementById(element.id);
	var filter_to_change =0;
	switch(candidate_filter)
	{
		case 'location': filter_to_change=0; break;
		case 'department': filter_to_change=1; break;
		case 'role': filter_to_change=2; break;
	}
	if(selector.options[selector.selectedIndex].value=="All")
	{
		candidate_filters[filter_to_change] = {'id':filter_to_change,'vector':candidate_filter,'operator':'!=','value':'nofudingloc'};
	} else {
		candidate_filters[filter_to_change] = {'id':filter_to_change,'vector':candidate_filter,'operator':'==','value':selector.options[selector.selectedIndex].value};
	}
	//if change in role
	if(filter_to_change==2)
	{
        if(async_load<2)
		{
			async_load+=2;	
		}
		//get corresponding loc and dep
		let loc_dep_res=get_loc_dep_from_reg(roles,$(element).val());
		//set the graph to the same
		$('#select_graph_loc').val(loc_dep_res['location']);
		$('#select_graph_dep').val(loc_dep_res['department']);
		
		//actualize the graph accordingly
		graph_filter_call(0,orgid,graph_filters,0);	
	}
}

function changeNbr(add_or_set){
	
	let selector = document.getElementById('select_nbr_candidates');
	if(add_or_set=='set')
	{
		max_candidate_display = Number(selector.options[selector.selectedIndex].value);

		//let span = document.getElementById('span_add_candidates');
		//span.innerHTML = "Add "+selector.options[selector.selectedIndex].value+" more";		
	} else {
		max_candidate_display = max_candidate_display+ Number(selector.options[selector.selectedIndex].value);		
	}
	//populate the table with the new value
	populateCandidateTable(filtered_super);
	let add_button = document.getElementById('button_add_candidates');	

	if(max_candidate_display ==9999 ||max_candidate_display > filtered_super.length)
	{
		add_button.style.display='none';
	} else {
		add_button.style.display ='block';
	}
}


//function to remove element which is a filter
function removeFilterElement(elementId,filterId) {
    // Removes an element from the document
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);

    candidate_filters.splice(getIndexId(candidate_filters,filterId),1);

    filtered_super = applyFilters(super_cadidate_array,candidate_filters)	
    populateCandidateTable(filtered_super);
}



function initialize_buttons(option){

let button_apply= document.getElementById("button_apply");
	button_apply.onclick = function() {
		let selector_vector = document.getElementById("select_vector");
		let vector = selector_vector.options[selector_vector.selectedIndex].text;
		
		let selector_operator = document.getElementById("select_operator");
		let operator = selector_operator.options[selector_operator.selectedIndex].value;
		let operator_text = selector_operator.options[selector_operator.selectedIndex].text;

		let selector_value = document.getElementById("select_value");
		let value = selector_value.options[selector_value.selectedIndex].text;
		
		let currentFilterId = getAttributeWithKey(candidate_filters,'vector',vector);
		//if not existing filter
		if(currentFilterId ==-1)
		{
			let html = 
			'  <div class="btn btn-warning '+vector+'" onclick="removeElementFilter('+filterId+')">'
			+vector + ' is ' +operator_text+ ' than '+ value +'</div>'
					addElement('div_filters_vectors', 'p', 'filter'+filterId,html);
			candidate_filters.push({'id':filterId,'vector':vector,'operator':operator,'value':value});
			filterId ++;
		} else {
			//else remove the filter in the background

			candidate_filters.splice(currentFilterId,1);
			//then change it to the new one
			candidate_filters.push({'id':currentFilterId,'vector':vector,'operator':operator,'value':value});
			//adjust the corresponding button with the right text now
			let button_x = document.getElementsByClassName(vector);
			button_x[0].innerHTML = vector + ' is ' +operator_text+ ' than '+ value;

		}
		
		filtered_super = applyFilters(super_cadidate_array,candidate_filters);	
		populateCandidateTable(filtered_super);			
		
	}
	if(option!='noback')
	{
		let button_back= document.getElementById("button_back");
		button_back.onclick = function() {
			var table_ref = document.getElementById("div_all_candidates");
			table_ref.style.display = "block";   	
			var candidate_graph = document.getElementById("div_candidate_chart");
			candidate_graph.style.display="none";
		}
	}
}

function getAttributeWithKey(arrayToSearch,attribute, key)
{
	//alert(arrayToSearch.length);
	let toReturn = -1;
    for(let  i=0;i<arrayToSearch.length;i++)
            {
                //alert(arrayToSearch[i]['id'] + " " +Id);
                if(arrayToSearch[i][attribute]==key)
                {
                    toReturn = i
                }           
            }
    return toReturn;
}
//remove the filter including the button
function removeElementFilter(filterId){
    // Removes an element from the document
    let elementId = "filter"+filterId;
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
    candidate_filters.splice(getIndexId(candidate_filters,filterId),1);
    filtered_super = applyFilters(super_cadidate_array,candidate_filters)	
    populateCandidateTable(filtered_super);
}

//populate the caddidate table with all cand and their match
function populateCandidateTable(candidate_array){
	//get all the matches with the new set, and sort then by match

	candidate_array= sort_with_select(candidate_array);
	let table_ref = document.getElementById("tbody_candidates");
	$("#tbody_candidates").empty();

	let to_display = getMin(candidate_array.length , max_candidate_display);
		
	document.getElementById("span_count").textContent=to_display;
	for(let i=0;i<to_display;i++)
		{
			let row = table_ref.insertRow(table_ref.length);
			//let cell3= row.insertCell(0);
			//let cell2= row.insertCell(0);		
			let cell1= row.insertCell(0);	

			let date = new Date(candidate_array[i].creation_ts);
			let formated_date = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();

			let html_cell1="<div id='div_star' data-id='"+candidate_array[i].id+"' class='button_star ";
			if(candidate_array[i].is_selected=='1')
			{
				html_cell1=html_cell1+"star_full'";
			} else {
				html_cell1=html_cell1+"star_empty'";
			}
			 html_cell1=html_cell1+"></div><div class='btn btn-primary' id='button-"
				+candidate_array[i].id+"' onclick='single_candidate("
				+candidate_array[i].id+")'><i class='ion-android-person'></i> ";

			if(org_anonymous ==1)
			{	
				html_cell1=html_cell1+"Candidate"+candidate_array[i].id;
			} else {
				html_cell1=html_cell1+candidate_array[i].name;
			}
			html_cell1=html_cell1+ "</div>";
			cell1.innerHTML=html_cell1;
			
			/*let html = '<label class="container" id="candlist">'+
			'<input id="'+candidate_array[i].id+'" data-id="'+candidate_array[i].id+'" class="checkbox_cand" type="checkbox" name="checkbox_'+candidate_array[i].id+'" value="show"' ;
			//console.log(candidate_array[i].is_selected);
			if(candidate_array[i].is_selected=='1')
			{
				html = html + 'checked';
			}			
			html = html + '><span class="checkmark"></span></label>';
			cell2.innerHTML = html; */
			
			
			/*cell3.innerHTML  =candidate_array[i].match;

			switch(Math.floor(candidate_array[i].match/10)){
				case 9 : cell3.style.color='#0F913A' ; break;
				case 8 : cell3.style.color='#11AB44' ; break;
				case 7 : cell3.style.color='#11AB44' ; break;
				case 6 : cell3.style.color='#11AB44' ; break;
				case 5 : cell3.style.color='#FFCD69' ; break;
				case 4 : cell3.style.color='#FFCD69' ; break;
				case 3 : cell3.style.color='#FF614F' ; break;
				case 2 : cell3.style.color='#FF614F' ; break;
				case 1 : cell3.style.color='#FF614F' ; break;
				case 0 : cell3.style.color='#FF614F' ; break;
			}*/

		}

		$(".button_star").click(function (){
				//console.log(this);
			if($(this).hasClass('star_full'))
			{
				$(this).addClass('star_empty');	
				$(this).removeClass('star_full');
				super_cadidate_array[getIndexId(super_cadidate_array,$(this).data('id'))].is_selected =0;
				update_candidate_check($(this).data('id'),0)	;
				populateCandidateTable(applyFilters(super_cadidate_array,candidate_filters));	
			}else {
				$(this).addClass('star_full');	
				$(this).removeClass('star_empty');	
				super_cadidate_array[getIndexId(super_cadidate_array,$(this).data('id'))].is_selected =1;
				update_candidate_check($(this).data('id'),1)	;		
				populateCandidateTable(applyFilters(super_cadidate_array,candidate_filters));
			}
		});
}


function update_candidate_check(user_id,is_checked)
{
           $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['update_candidate_check'],
                        arguments: [user_id,is_checked]
                            },
                success: function(data) {                    
                   // alert("done"+data);
                }
            });
} 

function insert_employee_hire(user_id)
{
           $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['insert_employee_hire'],
                        arguments: [user_id]
                            },
                success: function(data) {                    
                   // alert("done"+data);
                }
            });
} 

function update_org_anonymous(org_id,is_checked)
{
           $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['update_org_anonymous'],
                        arguments: [org_id,is_checked]
                            },
                success: function(data) {                    
                   // alert("done"+data);
                }
            });
} 