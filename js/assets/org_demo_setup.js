//populate the initial list with existing ones
function populate_links(list_name)
{
    
    for(var i=0;i<all_links.length;i++)
    {
        if(all_links[i].type=="employee")
        {
            var html = 
            "<br>"
            +"<p>The "+all_links[i].type+" link is the following <br> </p>"
            +"<input type='text' value='"+all_links[i].full_url+"' id='input_"+all_links[i].type+"' readonly>"
            +'<button id="button_'+all_links[i].type+'" onclick="copy_link(\''+all_links[i].type+'\')">'
            +"Copy link</button>"
            +"<p>Your email by default is <i>XYZ</i>"+all_links[i].default_email+"</p>"
            +"<br>";
            addElement(list_name, 'div', 'link'+i,html);                
        }
    }   
}

function copy_link(caller){
    let input = document.getElementById('input_'+caller);
    input.select();
    document.execCommand('copy');
}

function removeElementRole(elementId,item) {
    // Removes an element from the document
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
    /*if(item.type=="loc_dep")
    {
        delete_org_loc_dep(item.location,item.department);
        remove_loc_dep({location:item.location, department:item.department});   
    }*/
    if(item.type=="role")
    {
        deactivate_org_role(item.role);
    }
}

function populate_structure(array_data, list_name, element)
{
    //console.log(array_data);
    for(let key in array_data)
    {
        let html = "<b ";
        if(element=='location')
        {
            html+="onclick=click_loc(this,'"+key+"')";
            html+=">"+ key + "</b>";
        }
        
        else {
            html+='onclick="click_dep(this,\'';
            html+=array_data[key];
            html+='\'    )"';
            html+=">"+ array_data[key] + "</b>";
        }
        
        /*+'  <div class="btn btn-warning" onclick="removeElement(\'dept'+ dep_counter + '\','
            +'{type:\'loc_dep\',location:\''+loc_dep[i].location + '\',department:\''
            + loc_dep[i].department + '\'})">Remove</div>'*/
        addElement(list_name, 'p', element+counters[element],html);                
        counters[element]++;
    }   
}

function click_loc(button,location)
{
    current_location = location;
    $('#div_org_dep').show();
    $('#div_block_org_roles').hide();
    $('#div_department_list').html('');
    //button.css('font-weight', 'bold');
    $( "#div_location_list" ).find( "b" ).css( "color", "#073E4A" );
    button.style.color = "#000";
    populate_structure(parent_loc_dep[location],'div_department_list','department');
}

function click_dep(button,department)
{
    current_department = department;
    $('#div_block_org_roles').show();
    $('#role_list').html('');
    $( "#div_department_list" ).find( "b" ).css( "color", "#073E4A" );
    button.style.color = "#000";
    populate_roles_loc_dep('role_list',current_location,current_department)
    
}


//ajax calls to get the list updated
function insert_org_location(location)
{
           $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['insertLoc'],
                        location: location
                            },
                success: function(data) {                    
                   // alert("done"+data);
                }
            });
}
//ajax calls to get the list updated
function insert_org_department(department,location)
{
           $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['insertDep'],
                        department: department,
                        location: location
                            },
                success: function(data) {                    
                   // alert("done"+data);
                }
            });
}



//populate the initial list with existing ones
function populate_roles_loc_dep(list_name,location,department)
{
    for(var i=0;i<roles.length;i++)
    {
        if(location ==roles[i].location && department==roles[i].department)
        {
            addElement(list_name, 'p', 'role'+role_counter,
            get_role_html(roles[i].role_name,roles[i].location,roles[i].department, role_counter, roles[i].full_url));                
                role_counter++;    
        }
    }   
}

//populate the initial list with existing ones
function populate_roles(list_name)
{
    for(var i=0;i<roles.length;i++)
    {
        addElement(list_name, 'p', 'role'+role_counter,
            get_role_html(roles[i].role_name,roles[i].location,roles[i].department, role_counter, roles[i].full_url));                
                role_counter++;
    }   
}

function get_role_html(name, location, department, counter, full_url){
    let html = "<div class='role_added'>Role "+ name + " at loc/dep " + location + "->" + department
                //+'<br></div><div id="div_role_link'+counter+'">'+full_url+'</div>' 
                +"<br><input type='text' value='"+full_url+"' id='input_candidate"+counter+"' readonly>"
            +'<button id="button_candidate'+counter+'" '
            +'onclick="copy_link(\'candidate'+ counter+'\')">'
            +"Copy link</button>"
                +'<div class="btn btn-delete" onclick="removeElementRole(\'role'+ counter + '\','
                +'{type:\'role\',role:\''+name+'\'})">'
                +'<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"'
                +'width="20" height="20"'
                +'viewBox="0 0 192 192"'
                +'style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,192v-192h192v192z" fill="none"></path><g fill="#d680eb"><g id="surface1"><path d="M35.16,24.24l-10.92,10.92l60.84,60.84l-61.2,61.32l10.8,10.8l61.32,-61.2l61.2,61.2l10.92,-10.92l-61.2,-61.2l60.84,-60.84l-10.92,-10.92l-60.84,60.84z"></path></g></g></g></svg>'
                +'</div>';
    return html;
}

//check if reg exists already
function is_reg_exists(new_reg)
{
    var to_return = false;
    for(var i=0;i<regs.length;i++)
    {
        if(regs[i].reg ==new_reg)
        {
            to_return=true;
            break;
        }
    }

    return to_return;
}

//ajax calls to get the list updated
function insert_org_role(new_role)
{
           $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['insertRole'],
                        arguments:new_role 
                            },
                success: function(data) {   
                    data = JSON.parse(data);

                document.getElementById("input_candidate"+(role_counter-1)).value=data;  
                //also complete the role array with the right data
                new_role['full_url']=data;
                roles.push(new_role);   
                }
            });
}

function deactivate_org_role(old_role)
{
           $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['deactivateRole'],
                        arguments: old_role,
                            },
                success: function(data) {                    
                   // alert("done"+data);
                }
            });
}
