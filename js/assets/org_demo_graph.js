//used for some reason the graph filters need to be re-evaluated every time
function setSelectorFilter(select,filter_name){
	var selector = document.getElementById(select);
		graph_filters[filter_name].push(selector[selector.selectedIndex].value);
		graph_filters[filter_name].shift();
}

//used for some reason the graph filters need to be re-evaluated every time not used
function setReferenceSelectorFilter(ref_filters,filter_id,value){
		ref_filters[filter_id]['value']=value;
}

//set the select to the corresponding filter WITHOUT triggering the update
function setFilterSelect(filters){
	setSelectValue("select_gender",filters["gender"]);
	setSelectValue("select_graph_dep",filters["department"]);
	setSelectValue("select_graph_loc",filters["location"]);
	setSelectValue("select_tenure",filters["tenure"]);

}


function setSelectValue(select_name, value)
{
	var selector = document.getElementById(select_name);
	for(var i=0;i<selector.length;i++)
		{
			if(selector.options[i].value ==value)
			{
				selector.selectedIndex = i;
				break;
			}
		}
}

//set the DATA of the graph displayed
function setGraphVector(new_array){
	graph_vectors = new_array;
}



function setVectorSortingDesc(chart_id, vector_data){
	vectors_sorted_desc = getSortedVectors(vector_data);
	
	replace_bar_dataset(chart_id,vector_data, vectors_sorted_desc);	
	
	
}

