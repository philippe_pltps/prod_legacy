<?php

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}

// Initialize the session
include "db_helper/session.php"; //Include PHP MySQL sessions
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

  include 'db_helper/db_util.php';
  
  

?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My profile</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/styles_profile.css">
    <script src="js/addons/Chart.bundle.js"></script>
    <script src="js/functions/definitions.js"></script>
    <script src="js/functions/org_polar.js"></script>

    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">
</head>
<body>
    <div class="background">

        <div class="bg-div">
            <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
            <nav>
            </nav>  
        </div>
        <div class="page-header">
            <h1>Hi, <b><?php echo htmlspecialchars($_SESSION["email"]); ?></b><br>Welcome!</h1>
        </div>
        
            <?php
            
            $status = get_profile_status($_SESSION["id"]);

                //if profile is completed then 
            if($status['status']=="None" )
            {
                echo "<h2>You don't have a profile";
                echo '<br><br><a class="button-4" href="step1.php" >Create a profile</a></h2>';
            } else {
                if($status['status']=="complete")
                {
                    $profile = select_data(4,'guid' ,$status['guid']);
                    echo "<h2>Your current profile, thanks for using the Platypus</h2>";
                    //print_r($profile);
                    echo '<div class="chart-container" style="position: relative; width:60vw; margin:0 auto;">';
                    echo '<canvas id="userChart"></canvas>';
                    echo '</div>';
                    
                    $field_array = json_encode($profile);
                    echo "<script>";
                    echo "var field_array = ". $field_array.";\n";
                    echo "</script>";
                } elseif($status['status']==1)
                {
                    echo "<h2>You stopped at step 1";
                    echo '<br><a class="button-4" href="step1.php">Restart from Step 1</a></h2>';
                    $_SESSION['guid']=$status['guid'];
                } elseif($status['status']==2  )
                {
                    echo "<h2>You stopped at step ".$status['status']."</h2>";
                    echo '<br><a class="button-4" href="step'.$status['status'].'.php" >Continue from Step '.$status['status'].'</a></h2>';
                    $_SESSION['calling']="welcome";
                    $_SESSION['guid']=$status['guid'];
                } elseif( $status['status']==3 )
                {
                    echo "<h2>You stopped at step ".$status['status']."</h2>";
                    echo '<br><a class="button-4" href="step'.($status['status']+1).'.php">Continue from Step '.$status['status'].'</a></h2>';
                    $_SESSION['calling']="welcome";
                    $_SESSION['guid']=$status['guid'];
                }
            }        
            ?>
              
            
        
            <a class="button-2" href="reset-password.php" >Reset Your Password</a>
            <a class="button-3" href="logout.php">Sign Out of Your Account</a>
    </div>

<script>  
    create_chart("userChart", "nonumbers");
    //add the dataset
    replace_dataset(0,field_array);     
    </script>
</body>
</html>