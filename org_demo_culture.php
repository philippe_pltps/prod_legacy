<?php
// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}
// Initialize the session
include "db_helper/session.php"; //Include PHP MySQL sessions
// Unset all of the session variables

 //for the organisation page better use our own org i

?>

<script>

	<?php //load the data
	include 'db_helper/db_util.php';
			//get the graph avg list
	//used for the demo org graphs
if(!isset($_SESSION['orgid']))
			{
				$_SESSION=[];
			 $_SESSION["orgid"] = -1;
			}
			echo "var orgid=".$_SESSION["orgid"].";\n";
	
			/*$results =get_avg_all_profiles($_SESSION["orgid"]);
			$graph_vectors = json_encode($results);			
			echo "var graph_vectors =  ". $graph_vectors.";\n";*/
	//used for the demo profile
	$employees =  get_all_employees($_SESSION["orgid"]);
	$json_employees = json_encode($employees);
	echo "var employees=". $json_employees.";\n";


	
	//org creaiton date

	$today = new DateTime();
	$creation_ts = new DateTime(get_orgid_from_id($_SESSION["orgid"])['creation_ts']);
	$month_diff= $creation_ts->diff($today);
	//echo "var slider_length = ".$month_diff->m.";\n";

		$max_tenure =get_max_tenure( $_SESSION["orgid"]);
			$json_max_tenure = json_encode($max_tenure);
			echo "var max_tenure =  ". $json_max_tenure.";\n";	
				
			$parent_loc_dep = get_parent_location_department($_SESSION["orgid"]);
			$json_parent_loc_dep = json_encode($parent_loc_dep);
			echo "var parent_loc_dep= ". $json_parent_loc_dep.";\n";
	?>

</script>

<!DOCTYPE html>
<html>

<head>

	<meta charset="UTF-8">

	<title>Organization analysis page</title>

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style_demo.css">
    <link rel="stylesheet" href="css/bootstrap-slider.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/ionicons.min.css">
      
	<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
  	<script src="js/addons/Chart.bundle.js"></script>
  	<script src="js/assets/org_demo_graph.js"></script>
 	<script src="js/addons/bootstrap-slider.js"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

 	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
	<link rel="manifest" href="favicon/site.webmanifest">
 
</head>


<body>

	<div class="background">
		
		<div class="bg-div">
		    <button type="button" id="sidebarCollapse" class="btn btn-info" style="margin-top:8px">
               <i class="ion-android-menu"></i>
               <span></span>
           	</button>
		    <img class="logo-img" src="images/platypus_logo.png" width="167" height="55" ALT="align box" ALIGN=CENTER>
		   	<a href="http://theplatypus.io" target="_blank"><img class="" src="images/piechart.svg" width="55" height="55" ALT="align box" align="center" style="float:right"></a>
		</div>

		<div class="wrapper">
		    <!-- Sidebar -->
		    <nav id="sidebar" class="active">
		        <div class="sidebar-header">
		        </div>
		        <ul class="list-unstyled components">
		            <p></p>
		            <li >
		                <a href="org_demo_dashboard.php" >Dashboard</a>
		                <!--<a href="org_demo_culture.php" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Culture</a>-->
		                <ul class="collapse list-unstyled" id="homeSubmenu">
		                </ul>
		            </li>
		            <li class="active">
		                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">Culture</a>
		                <ul class="collapse list-unstyled show" id="homeSubmenu">
		                    <li id="li_split" class="submenu active_tab active">
		                        <a href="#">Split</a>
		                    </li>
		                    <li id="li_sample" class="submenu">
		                        <a href="#">Sample</a>
		                    </li>
		                    <li id="li_timeline" class="submenu">
		                        <a href="#">Timeline</a>
		                    </li>
		                </ul>
		            </li>
		            <li>
		            	<a href="org_demo_recruitment.php" >Recruitment</a>
		                <!--<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Recruitment</a>-->
		                <ul class="collapse list-unstyled" id="pageSubmenu">
		                    <li id="li_candidate" class="submenu active_tab active">
		                        <a href="#">Candidate</a>
		                    </li>
		                    <li id="li_setup" class="submenu">
		                        <a href="#">Setup Structure</a>
		                    </li>
		                </ul>
		            </li>
		        </ul>
		        <div id="div_container_def" style="margin:10px; font-weight: 500; font-size:large">
		        	Definition:<br>
		        	<div id="div_definition"></div>
		    	</div>
		    </nav>

			<div class="container">
			  	<div class="row">
			    	<div class="column"  id='div_column_0'>

				    	<!--<a href="org_open_reg.php" class="btn btn-primary">Setup this structure</a>
				    	<a href="org_profiles.php" class="btn btn-primary">Org profiles</a>-->
				    	<div id='div_vector_org_polar' class="block">
							<div id="div_select_graph_loc_dep" class="select_graph_loc_dep">
								<select id="select_graph_loc" name="select_graph_loc" onchange="changeGraphFilter(0,'location',this)">		
								</select> 
							    <select id="select_graph_dep" name="select_graph_dep" onchange="changeGraphFilter(0,'department',this)"> 
								</select> 
							</div>
							<div id ="div_select_tenure" class="div_select_tenure">
								<select  id="select_tenure" class='select_tenure' onchange="changeGraphFilter(0,'tenure',this)">
									<option value="0" disabled selected>Minimum Tenure</option>
								</select>
							</div>
							
							<div id="div_select_gender" class="div_select_gender">
								<select  id="select_gender" class='select_gender' onchange="changeGraphFilter(0,'gender',this)">
									<option value="A" disabled selected>Gender</option>
									<option value="A">All</option>
								      <option value="F">Female</option>
								      <option value="M">Male</option>
								</select>
							</div>
							<div id="div_canvas">
							    <canvas id="orgChart"></canvas>
							</div>
						</div>
					</div>

					<div id="div_column_1" class="column" >
				    	<div id="div_vector_org_ranking" class="block">
							<canvas id="vectorOrgChart" ></canvas>
						</div>
						
					</div>
					<div id='div_parking' style="display:none;">
						<div id="div_comp_column" class="block">
							<div id="div_select_comp_graph_loc_dep" class="select_graph_loc_dep">
								<select id="select_comp_graph_loc" name="select_comp_graph_loc" onchange="changeGraphFilter(1,'location',this)">
								</select> 
						    			
				    			<select id="select_comp_graph_dep" name="select_comp_graph_dep" onchange="changeGraphFilter(1,'department',this)">        
							    </select> 
							</div>
							<div id ="div_select_comp_tenure" class="div_select_tenure">
								<select  id="select_comp_tenure" class='select_tenure' onchange="changeGraphFilter(1,'tenure',this)">
								<option value="0" disabled selected>Minimum Tenure</option>
								</select>
							</div>
							
							<div id="div_select_comp_gender" class="div_select_gender">
								<select  id="select_comp_gender" class='select_gender' onchange="changeGraphFilter(1,'gender',this)">
									<option value="A" disabled selected>Gender</option>
									<option value="A">All</option>
								      <option value="F">Female</option>
								      <option value="M">Male</option>
								</select>
							</div>
							<div id="div_comp_canvas">
							    <canvas id="orgCompChart"></canvas>
							</div>
							<div id="div_vector_comp_ranking">
								<canvas id="vectorCompChart" ></canvas>
							</div>
						</div>
						<div id='div_employee_profiles' class="block">

						</div>
						<div id='div_timeline_slider' class="block">
							Contributors <span id='span_contributors'> </span> <br>
							Date <span id='span_date'> </span> <br>
							<div id="div_button_play" class="btn btn-primary">Play</div>
								<input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="<?php echo $month_diff->m; ?>" data-slider-step="1" data-slider-value="<?php echo $month_diff->m; ?>"/>
						</div>
						<div id='div_profiles_filters' class="block">
							<div id="div_select_profiles_loc_dep" class="select_graph_loc_dep">
								<select id="select_profiles_loc" name="select_profiles_loc" onchange="changeProfilesFilter(this)">		      
								</select> 
							    <select id="select_profiles_dep" name="select_profiles_dep" onchange="changeProfilesFilter(this)"> 
								</select> 
							</div>
							<div id ="div_select_profiles_tenure" class="div_select_tenure">
								<select  id="select_profiles_tenure" class='select_tenure' onchange="changeProfilesFilter(this)">
									<option value="0" disabled selected>Minimum Tenure</option>
								</select>
							</div>
							<div id="div_select_profiles_gender" class="div_select_gender">
								<select  id="select_profiles_gender" class='select_gender' onchange="changeProfilesFilter(this)">
									<option value="A" disabled selected>Gender</option>
									<option value="A">All</option>
								      <option value="F">Female</option>
								      <option value="M">Male</option>
								</select>
							</div>
						</div>
						<div id="div_timeline_vector_bar" class="block">
							<canvas id="timelineVectorBarChart" ></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script src="js/functions/definitions.js"></script>
<script src="js/functions/org_polar.js"></script>
<script src="js/functions/calculation.js"></script>
<script src="js/functions/tools.js"></script>
<script src="js/functions/bar_comparaison.js"></script>
<script src="js/functions/bar_vector_timeline.js"></script>
<script>
	var var_type='culture';
</script>
<script src="js/functions/timer_minute.js"></script>
<script>
current_page='culture';
setup_split();


/*****************initialization of the top menu***************/
$("#li_split").on("click", setup_split);
$("#li_sample").on("click", setup_sample);
$("#li_timeline").on("click", setup_timeline);
$("#div_button_play").on("click", play_timeline);

$('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
//



function setup_split(){
 	screen_split();
	submenu_activate('#li_split');
}

function setup_sample(){
	screen_sample();
 	submenu_activate("#li_sample");
}

function setup_timeline(){
	screen_timeline();
 	submenu_activate("#li_timeline");
}

function submenu_activate(tab_id){
	$('.active_tab').removeClass('active');
	$('.active_tab').removeClass('active_tab');
	$(tab_id).addClass('active_tab');	
	$(tab_id).addClass('active');	
}


function screen_split(){
	is_timeline=false;
	current_page='culture_split';
	$('#div_parking').append($('.block'));
	$('#div_employee_profiles').append($('.employee_brick'));
	$('#div_column_0').append($('#div_vector_org_polar'));
	$('#div_column_0').append($('#div_vector_org_ranking'));
	$('#div_column_1').append($('#div_comp_column'));
}


function screen_sample(){
	is_timeline=false;
	current_page='culture_sample';
	$('#div_parking').append($('.block'));
	//no need to hide the employee bricks we are adding them them
	
	$('#div_column_0').append($('#div_profiles_filters'));
	update_profiles_graphs('div_employee_profiles', filtered_employees);
}

function screen_timeline(){
	current_page='culture_timeline';
	$('#div_parking').append($('.block'));
	$('#div_employee_profiles').append($('.employee_brick'));
	$('#div_column_0').append($('#div_vector_org_polar'));
	$('#div_column_1').append($('#div_timeline_slider'));
	$('#div_column_1').append($('#div_vector_org_ranking'));
	is_timeline=true;
	graph_filter_call(0,orgid,graph_filters);	
	
}

function polar_culture_click(index){
	$('#div_column_0').append($('#div_timeline_vector_bar'));
	replace_timeline_bar_dataset(vector_definitions.vectors[index].name,timeline_array)
}

var timeline_counter=3;
function play_timeline(is_auto){
	//=0;
	if(is_auto!=1 )
	{
		//restart counter
		timeline_counter=0;
		//disable button
		$('#div_button_play').unbind( "click" );
	}
	//timeline_counter=0;
	let t;
	if(timeline_counter<timeline_array.length-1)//with 3 it will be executed 4 times
	{
		set_timeline(timeline_array,timeline_counter);
		$('#ex1').slider('setValue',timeline_counter);
		//trigger next in 1 sec
		t = setTimeout(next_timeline, 800);	
	} else {
		//re attach the event click to the button
		$("#div_button_play").on("click", play_timeline);
	}
	
}
//action to replay every second
function next_timeline(){
	timeline_counter++;
	$('#ex1').slider('setValue',timeline_counter);
	set_timeline(timeline_array,timeline_counter)
	play_timeline(1);
	
}

//all chart stuff

		//initialize the chart
		create_chart("orgChart", "numbers_6");
		create_chart("orgCompChart", "numbers_6");

		//initialize bar chart org
		create_bar_chart("vectorOrgChart");
		create_bar_chart("vectorCompChart");

	/***********apply and manage filters for graph side***************/
	//setup arrays for graph filter
	var graph_filters={gender:['A'],department:['All'],location:['All'],tenure:[0]};
	var graph_comp_filters={gender:['A'],department:['All'],location:['All'],tenure:[0]};
	var handle_filters= JSON.parse(JSON.stringify(graph_filters)); 


	//for test purposes as Dixa
	
	var is_timeline=false;
	var slider_value=0;
	var timeline_array=[];
	var filtered_employees=employees;

	populate_select(parent_loc_dep,'select_graph_loc','All','Location');
	populate_select(parent_loc_dep,'select_comp_graph_loc','All','Location');
	populate_select(parent_loc_dep,'select_profiles_loc','All','Location');

	populate_select(removeDuplicatesInChild(parent_loc_dep),'select_graph_dep','All','Department');
	populate_select(removeDuplicatesInChild(parent_loc_dep),'select_comp_graph_dep','All','Department');
	populate_select(removeDuplicatesInChild(parent_loc_dep),'select_profiles_dep','All','Department');

	populateSelectTenure("select_tenure",max_tenure['max']);
	populateSelectTenure("select_comp_tenure",max_tenure['max']);
	populateSelectTenure("select_profiles_tenure",max_tenure['max']);
	
	//enecessary, for some readon, the select profile gender need to be selected otherwise its value si set to null
	//$('#select_profiles_gender').val('A');


	graph_filter_call(0,orgid,graph_filters);
	//setup the comparaison chart to be the initial one as well
	graph_filter_call(1,orgid,graph_comp_filters);
	//setup the different lists
	
//initialize slider
$('#ex1').slider({
    formatter: function(value) {

        slider_value=value;
        //if timeline array not null (initiated)
        if(timeline_array.length>0)
        {
        	set_timeline(timeline_array,slider_value);
        }
		//used in the impact of a person see recruitment part        
        //replace_dataset_merge(2,0,1,slider_value/3)
    }
});

function set_timeline(array_data,slider_value){
	//$('#span_contributors').html(array_data[slider_value]['count_profiles']);
    $('#span_date').html(array_data[slider_value]['timeline_date']);
    replace_dataset(0,array_data[slider_value]);	
    setVectorSortingDesc(0,array_data[slider_value]);
}


//all profiles stuff
var profiles_filters = [];
//add the 3 initial filters corresponding to locationm department and role
profiles_filters.push({'id':0,'vector':'location','operator':'!=','value':'nofudingloc'});
profiles_filters.push({'id':1,'vector':'department','operator':'!=','value':'nofudingdep'});
profiles_filters.push({'id':2,'vector':'tenure','operator':'>=','value':0});
profiles_filters.push({'id':3,'vector':'gender','operator':'!=','value':'nofudingrole'});

create_profiles_graphs('div_employee_profiles', employees);

//create all the 
function create_profiles_graphs(div_parent,arg_employees)
	{
		let html ='';
		for(let i=0; i<getMin(arg_employees.length,20);i++)
		{
			html = '<div id="div_canvas'+i+'" style="margin-top:40px;">'+
			'<canvas id="empChart'+i+'"></canvas>'+
			'</div><span id="span_profile'+i+'">Employee '+(i+1)+'</span> '+
			'<div id="div_vector_ranking'+i+'">'+
			'<canvas id="vectorEmpChart'+i+'" ></canvas>'+
			'</div>';
	        addElementWithClass(div_parent, 'div', 'employee_profile_'+i,html,'employee_brick');     
	        create_chart('empChart'+i, "numbers_6");
			create_bar_chart('vectorEmpChart'+i);
			//remember that 1 and 2 are the first charts in split
	        replace_dataset(i+2,arg_employees[i]);
	        //adjust the ranking list
	        let vectors_sorted_desc = getSortedVectors(arg_employees[i]);
	        replace_bar_dataset(i+2,arg_employees[i], vectors_sorted_desc);
		}	
	}

function update_profiles_graphs(div_parent,arg_employees)
	{
		
		
		$('.employee_brick').addClass('hide_profile');
		for(let i=0; i<getMin(arg_employees.length,20);i++)
		{
			//remember that 1 and 2 are the first charts in split
	        replace_dataset(i+2,arg_employees[i]);
	        //adjust the ranking list
	        let vectors_sorted_desc = getSortedVectors(arg_employees[i]);
	        replace_bar_dataset(i+2,arg_employees[i], vectors_sorted_desc);
	        $('#employee_profile_'+i).removeClass('hide_profile');
		}
		
		let toggle = 0;
		$('.employee_brick').not('.hide_profile').each(function(){
			$('#div_column_'+(toggle%2)).append($(this));
			$('#span_profile'+toggle).html('Employee '+(toggle+1));
			toggle++;
		});
		$('#div_employee_profiles').append($('.hide_profile'));
		//$('#div_employee_profiles').append($('.hide_profile'));	

	}


//set the tenure when changin the select
function changeGraphFilter(chartId,graph_filter,element) 
{
	//knowing that chart 0 is the org, 1 is candidate and 2 is comparaison chart
	var selector = document.getElementById(element.id);
	//add to the end and remove the first element
	//graph_filters[graph_filter]
	if(chartId==0)
	{
		if(element.id=='select_graph_loc')
		{
			funnel_list(element.id, 'location', 'select_graph_dep','department');	
		}
		graph_filters[graph_filter].push(selector.options[selector.selectedIndex].value);
		graph_filters[graph_filter].shift();
	
	//actualize the graph
		graph_filter_call(chartId,orgid,graph_filters);			
	} else {
		if(chartId==1)
		{
			if(element.id=='select_comp_graph_loc')
			{
				funnel_list(element.id, 'location', 'select_comp_graph_dep','department');
			}
			graph_comp_filters[graph_filter].push(selector.options[selector.selectedIndex].value);
			graph_comp_filters[graph_filter].shift();
			
			//actualize the graph
			graph_filter_call(chartId,orgid,graph_comp_filters);			
		}
	}
}

function changeProfilesFilter(element){
	//display the funnel
	//to rework
	if(element.id=='select_profiles_loc')
	{
		funnel_list(element.id, 'location', 'select_profiles_dep','department');	
	}
	if($("#select_profiles_loc").children("option").filter(":selected").val()=='All')
	{
		profiles_filters[0].value = 'feawfaewfewa';
		profiles_filters[0].operator = '!=';
	} else {
		profiles_filters[0].value = $('#select_profiles_loc').val();
		profiles_filters[0].operator = '==';
	}
	if($("#select_profiles_dep").children("option").filter(":selected").val()=='All')
	{
		profiles_filters[1].value = 'feawfaewfewa';
		profiles_filters[1].operator = '!=';
	} else {
		profiles_filters[1].value = $('#select_profiles_dep').val();
		profiles_filters[1].operator = '==';
	}
	
	profiles_filters[2].value = Number($('#select_profiles_tenure').val());

	if($("#select_profiles_gender").children("option").filter(":selected").val()=='A')
	{
		profiles_filters[3].value = 'feawfaewfewa';
		profiles_filters[3].operator = '!=';
	} else {
		profiles_filters[3].value = $('#select_profiles_gender').val();
		profiles_filters[3].operator = '==';
	}
	
	//actualize candidates
	filtered_employees = applyFilters(employees,profiles_filters);	
	
	//recreate all the graphs and distribute them
	console.log(filtered_employees);
	if(filtered_employees.length<4)
	{
		alert("The selected set contains less than 4 individuals than we cannot show to keep ThePlatypus Anonymous :) please try another subset");
	} else {
		update_profiles_graphs('div_employee_profiles', filtered_employees);	
	}
	

 
}

//ajax call for the filtered set
	function graph_filter_call(chartId,orgId,graph_filters) {

		let request_type = '';
		if(!is_timeline)
		{
			request_type = 'graph_filter_call';
		} else {
			request_type = 'graph_filter_timeline';
		}
            $.ajax({
                type: "POST",
                url: "ajax_graph_filter.php",
                datatype: "json",
                data: {requestType: [request_type],
                		graph_filters:graph_filters,
                		orgId:orgId
            				},
                success: function(data) {  
                	//alert(data)             ;     
                    data = JSON.parse(data);
                    //alert(data['count_profiles']);
                    //console.log(data['count_profiles']);
                    if(data['count_profiles']<4){
						alert("The selected set contains less than 4 individuals than we cannot show to keep ThePlatypus Anonymous :) please try another subset");
						setFilterSelect(handle_filters);
						 
                    } else {
                    	//adjust the graph
	                    if(!is_timeline)
	                    {
	                    	replace_dataset(chartId,data);
	                        setVectorSortingDesc(chartId,data);
	                        if(chartId==0)
	    	                    {
	    	                    	//adjust the ranking list
	    	                    //adjust the data of the graph
	    	                    setGraphVector(data);
	    	                    
	    						handle_filters= JSON.parse(JSON.stringify(graph_filters)); 	
	    	                    }
	                    } else {
	                    	let initial_size = data.length;
	                    	let counter_delete = 0;
	                    	for(let index in data)
	                    	{
	                    		if(data[index].count_profiles<4)
	                    		{
	                    			delete data[index];	
	                    			counter_delete++;
	                    		}
	                    	}
	                    	console.log(data,data.length);
	                    	if(counter_delete==initial_size)
	                    	{
	                    		alert("The selected set contains less than 4 individuals than we cannot show to keep ThePlatypus Anonymous :) please try another subset");
	                    		
	                    	} else {
	                    		timeline_array = data;
	                    	    $('#span_date').html(timeline_array[slider_value]['timeline_date']);
		                    	replace_dataset(0,timeline_array[slider_value]);	
		                    	setVectorSortingDesc(0,timeline_array[slider_value]);	
	                    	}
	                    	
	                    }
                    }
                    	

                }
            });
        }

</script>
     
	


</body>

</html>