<!DOCTYPE html>
<html>

	

	    <?php
	  include 'db_helper/db_util.php';
		//	start a session to keep the data received in page1
		  //display received values
	      /*if (isset($_POST['balance'])) {
	        echo '<div style="border:1px solid #fc0; padding:10px;margin-top:25px;">';
	        echo 'The values returned were:  '.implode(', ',array_values($_POST));
	        echo '</div>';
	      }*/
	      //put them into an array
	      //get into an array with keys print_r($results);
		  
		include "db_helper/session.php"; //Include PHP MySQL sessions
	    if(empty($_SESSION['guid']))
	      	{
	        header("Location: https://www.theplatypus.io"); 
	        exit();
	     	} else 
	      	{
	      		//setup the result anyway and clear the calling 
	      		$_SESSION['calling']='';
	      		$results =  get_single_profile(1,$_SESSION['guid']);
	      		echo '<script>';
	      		$json_result = json_encode($results); 
				echo "var results=". $json_result.";\n";
	      		echo '</script>';
	      		$guid = $_SESSION['guid'];
	      		//REDIECT if need be
	      		//redirect_to($guid,2);
	      		//unstable for the moment

	      		if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["form_proof_2"]))
                {
                	$user_status = get_profile_status($_SESSION["id"]);
		            if($user_status["status"]==2 || isset($_SESSION['is_test']) || $_SESSION['is_test']=='true' )
		            {
		              //get the raw answers into the db
		              insert_answers_step2($_POST, $guid);      
		              foreach($_POST as $field => $value) 
		                {
		                  //all exepct form proof 2    
		                  if($field !='form_proof_2' && $field !='input_timer')
		                  {
		                    $split = explode("_",$_POST[$field]);          
		                    $results[$field] = $results[$field] + (($split[0]-$split[1])/$split[0]);
		                  }               
		                }
		          //egt the fields adjusted into the db
		              insert_fields_data($results,2, $guid);  
		              insert_user_profile_status($guid, $_SESSION["id"], 3);
		              insert_probe_stepX_data(2,['input_timer' => $_POST['input_timer']], $guid );  
		              header("Location: step4.php"); 
		              echo print_r($_POST);
                       exit();

		            }else {
		            echo'</script>An error occured please contact support@theplatypus.io<script>';
		          }
                }	
	      	}
?>
<head>

	<meta charset="UTF-8">

	<title>Platypus Prioritisation</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/styles_cand.css">
    <script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
  	<script src="js/functions/timer.js"></script>
  	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
	<link rel="manifest" href="favicon/site.webmanifest">
</head>

<body>

	<div class="bck">
		<div class="bg-div">
		    <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
		    <a href="http://theplatypus.io" target="_blank"><img class="logo-img" src="images/piechart.svg" width="60" height="65" ALT="align box" align="center" style="float:right"></a>
		    <nav>
		    </nav>
        </div>
		<h1>Prioritisation</h1>
		<h4>Please rank the following vectors, top being most valued - simply drag to move<br><br>

		  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		  <div id="col1" class="column-m">
	      
	      </div>
	      <div id="col2" class="column-m">
	      
	      </div>
	      <div id="col3" >
	      		<div id="div_definition"></div>
	      </div>
	      <div id='priority_tables'>
	      <?php
	      $row_set = 0;
	      

	      for ($x = max($results); $x >=0; $x--) 
	        {
	          //$key = array_keys($_SESSION,$x);
			      $key = array_keys($results,$x);
	          $size = sizeof($key);
	          

	          if($size>1)
	          {
	          	echo '<div id="group_'.$row_set.'" class="div_group">';
	            echo "<i class='no_margin_left'>". number_format((float)$x, 0, '.', '') . " points</i>";
	            $default_selected=0;
	            $counter=0;
	            
	            echo '<ul  id="ul_sort_'.$x.'" class="js-sortable no_padding_left" aria-dropeffect="move">';
	            foreach($key as $order => $field) // foreach field
	            {
	            	echo '<li class="mb1 sorting list_input f_'.$field.'" style="position: relative; z-index: 10; " draggable="true" role="option" aria-grabbed="false">'.strtoupper($field).'</li>';
	            }	        	
	        	echo '</ul>';

	            
		            echo "<br></div>";
		            $row_set++;
		        }
	        }
	        echo "</div>";		      
			?>
			<input type="text" name="form_proof_2" value="form_proof_2" style="display:none">
			<input type="number" name="input_timer" id="input_timer" value="0" style="display:none">
			<div id="div_input_list">

			</div> 
			
			</h4>
				<div class="footer">
                    <ul class="progressbar">
                        <li class="active">POINT ALLOCATION</li>
                        <li class="active">PRIORITISATION</li>
                        <li >REFLECTION</li>
                        <li >FINISH</li>
                        <button type="submit" class="button-3" >NEXT</button>
                    </ul>
                </div>
        </form>
        
	</div>

</body>


</html>

	<script src="js/addons/html5sortable.js"></script>
	<script src="js/functions/tools.js"></script>
	<script src="js/functions/definitions.js"></script>
	<script>

//set all our list as sortable
	sortable('.js-sortable', {
	forcePlaceholderSize: true,
	placeholderClass: 'list_placeholder', 
	hoverClass: 'list_hover' 
	});

$(document).ready(function(){  
    $("body").mousemove(function(){  
        	$('#div_definition').html('Definition:<br>'+get_definition($('.list_hover').html()));
    })
    });  
    
	
	function setup_input(){
		let serialized = sortable('.js-sortable', 'serialize')
		var id="";
		var value='';
		var html="";
		var div_input = document.getElementById("div_input_list");
		div_input.innerHTML='';
		//for each sortable list
		for (var h=0;h<serialized.length;h++)
		{
			//for each item within that list
			for(var i=0;i<serialized[h].items.length;i++)
			{
				//console.log(extract_content(serialized[h].items[i].html));
				id=extract_content(serialized[h].items[i].html);
				value=serialized[h].items.length+"_"+(i+1);
				html = "<input type='text' name='"+id.toLowerCase()+"' value='"+value+"'style='display:none'></input>";
				addElement("div_input_list", 'div', "div_"+id.toLowerCase(),html);
				//at post it becomes $_POST[id] => extractContent(serialized[0].items[i].html)
			}
		}
		
	}

	function extract_content(li) {
	  var span = document.createElement('span');
	  span.innerHTML = li;

	  //console.log(span.innerHTML);
	  return span.textContent || span.innerText;
	};

	//execute the code when a js sortable is updated
	function initiate_sortable_list(){
		//run once
		setup_input();
		for(var i=0; i<sortable('.js-sortable').length; i++)
		{
			//setup a callback for each list
			sortable('.js-sortable')[i].addEventListener('sortupdate', function(e) {
				setup_input();
			});
		}
	}
	initiate_sortable_list();

	function setupColumns(){
		let list_group = document.getElementsByClassName('div_group');
		console.log(list_group.length);
		let half = Math.ceil(list_group.length/2);
		console.log(half);
		for(let i=0; i< half;i++)
		{
			$('#col1').append($('#group_'+i));	
		}
		for(let i=half; i< list_group.length;i++)
		{
			$('#col2').append($('#group_'+i));	
		}
		
	}
	setupColumns()


	</script>