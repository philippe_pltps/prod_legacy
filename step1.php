
    <?php    

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}


  include 'db_helper/db_util.php';
  include "db_helper/session.php"; //Include PHP MySQL sessions

  if(empty($_SESSION['id']))
        {
            header("Location: https://www.theplatypus.io"); 
            exit();
        } else 
        {
            $guid = getGUID();
            if(empty($_SESSION['guid']))
            {
                $_SESSION['guid'] = $guid;
                insert_user_profile_status($guid, $_SESSION["id"], 1);
                //no matter who calls clear the calling
                $_SESSION['calling']='';
            }else{
                $guid = $_SESSION['guid'];
                //redirect if not at the right spot
                //redirect_to($guid,1);
                //unstable for the moment
                $_SESSION['calling']='';

                if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["form_proof_1"]))
                {
                    $guid = $_SESSION['guid'];
                    $results = array();   
                    $probe = array();           
                    //differenciate if info for the probe or for the actual profile
                        foreach($_POST as $field => $value) 
                        {
                            if($field != 'input_timer' &&
                                $field != 'input_plus' &&
                                $field != 'input_minus' &&
                                $field != 'form_proof_1'
                                )
                            {
                                $results[$field] = $value;
                            }
                            else 
                            {
                                $probe[$field] = $value;
                            }
                           
                      //no more session in use
                      //$_SESSION[$field] = $value;
                        }
                        $user_status = get_profile_status($_SESSION["id"]);
                       if($user_status["status"]==1 || isset($_SESSION['is_test']) || $_SESSION['is_test']=='true' )
                       {
                        //put results into db
                        insert_fields_data($results, 1,$guid);
                    //adjust the status of the profile
                        insert_user_profile_status($guid, $_SESSION["id"], 2);
                    //send the data about the probe
                        insert_probe_step1_data($probe, $guid );  
                        header("Location: step2.php"); 
                        exit();
                       } else {
                        echo'</script>An error occured please contact support@theplatypus.io<script>';
                      }
                }        
  
            }


            
        }
    ?>

<!DOCTYPE html>
<html>

<head>

	<meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/styles_cand.css">
   
	<title>Platypus point allocation</title>


  <script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
  <script src="js/functions/incrementing.js"></script>
  <script src="js/functions/timer.js"></script>
  <meta name="viewport" content="width=device-width, user-scalable=yes">
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">

</head>

<body >
<!--<div id="div_point_left" class="fade" >-->
<!--<div id="div_point_left"  >
    <span id="span_points_left">30</span>
</div>-->
	<div class="background">
        <div class="bg-div">
            <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
            <a href="http://theplatypus.io" target="_blank"><img class="logo-img" src="images/piechart.svg" width="60" height="65" ALT="align box" align="center" style="float:right"></a>
            <nav>
            </nav>
        </div>
        
		<h1 class="h1_step1">What do you Value?</h1>
            <!--You have <span id="span_points">30</span> points left</h1>-->
           <!-- <h1>   a </h1>-->
        <h3>Let’s create your Platypus Print<br> You have a choice of 12 Vectors, elements of every Company that are split into four Families: People, Workplace, Behaviour and Organisation<br> 
            
            <h4><b>• You have a total of 30 points to distribute and there are no restrictions</b><br>
            • Placement should reflect the weight of importance you hold on each vector<br>
            • Take your time and think about what you really value - what’s important to you?<br>
            <?php
            echo ((starts_with($_SESSION['type'],'candidate') ) ? '• Be HONEST!<br>' :  '• Be HONEST! Everything you submit is 100% Anonymous<br>' );
            ?>
            </h4>
        </h3>
        <h6>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        	
        	
            <?php
                    $array_families[0] = array();
$array_families['people'][0] = 'teamwork';
$array_families['people'][1] = 'communication';
$array_families['people'][2] = 'structure';
$array_families['workplace'][0] = 'flexibility';
$array_families['workplace'][1] = 'perks';
$array_families['workplace'][2] = 'diversity';
$array_families['organisation'][0] = 'mission';
$array_families['organisation'][1] = 'career';
$array_families['organisation'][2] = 'compensation';
$array_families['behaviour'][0] = 'leadership';
$array_families['behaviour'][1] = 'development';
$array_families['behaviour'][2] = 'social';

$array_families = custom_shuffle($array_families);

$array_families['people'] = custom_shuffle($array_families['people']);
$array_families['workplace'] = custom_shuffle($array_families['workplace']);
$array_families['organisation'] = custom_shuffle($array_families['organisation']);
$array_families['behaviour'] = custom_shuffle($array_families['behaviour']);

$counter =0;
foreach($array_families as $family => $array_fields)
{
    echo '<div class="family_'.$family.'">';
        foreach($array_families[$family] as $index => $field)
            {
                $definition = get_definition($field);
                echo '<div class="numbers-row">';                
                
                 echo '<div class="button_spot">';
            echo '<div class="dec button_points dec_'.$family.'"  data-pltps="-"></div>';
            echo '<input type="number" class="step1_input" name="'.$field.'" id="'.$field.'" value="0" readonly>';
            echo '<div class="inc button_points inc_'.$family.'"  data-pltps="+"></div>';
            
              echo '</div>';
                echo '<label for="name"><b>'.strtoupper($field).'</b><br>';
                echo '<div class="definition">'.$definition.'</div>';
                echo '</label>';                
              echo '</div>';
            }
    echo '</div>';
}

            ?>

            <input type="number" name="input_timer" id="input_timer" value="0" style="display:none">
            <input type="number" name="input_plus" id="input_plus" value="0" style="display:none">
            <input type="number" name="input_minus" id="input_minus" value="0" style="display:none">
            <input type="text" name="form_proof_1" value="form_proof_1" style="display:none">
              <!--<div class="buttons" id="button_submit" style="display:none">
                <input type="submit" value="Go to next step" id="button_submit" class="btn btn-primary">
              </div>-->
</h6>
<div class="pointsleft">
   <p class="points_text"> POINTS LEFT </p>
   <p class="thirty"><span id="span_points">30</span></p>
</div>
                <div class="footer">
                    <ul class="progressbar">
                        <li class="active">POINT ALLOCATION</li>
                        <li>PRIORITISATION</li>
                        <li>REFLECTION</li>
                        <li>FINISH</li>
                        <button id="button_submit" type="submit" class="button-disabled" disabled>NEXT</button>
                    </ul>
                </div>

            </form>
        


	</div>

</body>

<script>
   /* function scrolling(){
        var div_point_left = document.getElementById('div_point_left');
    var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    //console.log(scrollTop);  
   // document.body.classList.add('fade');      
    if(scrollTop>=60)
        {
            //div_point_left.style.display="block";
            div_point_left.classList.remove('fade');
            div_point_left.classList.add('nonfade');    
        } else {
            div_point_left.classList.remove('nonfade');
            div_point_left.classList.add('fade');    
        }
    }*/

    </script>

</html>