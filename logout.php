<?php

// Initialize the session
include "db_helper/session.php"; //Include PHP MySQL sessions
 
// Unset all of the session variables
$_SESSION = array();
 
// Destroy the session.
session_unset();
session_destroy();
 
// Redirect to login page
header("location: index.php");
exit;

?>