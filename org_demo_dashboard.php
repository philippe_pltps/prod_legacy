<?php
// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}
// Initialize the session
include "db_helper/session.php"; //Include PHP MySQL sessions
// Unset all of the session variables

 //for the organisation page better use our own org i

?>

<script>
	<?php //load the data
	include 'db_helper/db_util.php';
if(!isset($_SESSION['orgid']))
			{
				$_SESSION=[];
			 $_SESSION["orgid"] = -1;
			}
			echo "var orgid=".$_SESSION["orgid"].";\n";
	
			/*$results =get_avg_all_profiles($_SESSION["orgid"]);
			$graph_vectors = json_encode($results);			
			echo "var graph_vectors =  ". $graph_vectors.";\n";*/
	//used for the demo profile
	$employees =  get_all_employees($_SESSION["orgid"]);
	$json_employees = json_encode($employees);
	echo "var employees=". $json_employees.";\n";

$parent_loc_dep = get_parent_location_department($_SESSION["orgid"]);
			$json_parent_loc_dep = json_encode($parent_loc_dep);
			echo "var parent_loc_dep= ". $json_parent_loc_dep.";\n";
	
	//org creaiton date
	//used for timeline
	$today = new DateTime();
	$creation_ts = new DateTime(get_orgid_from_id($_SESSION["orgid"])['creation_ts']);
	$month_diff= $creation_ts->diff($today);
	//echo "var slider_length = ".$month_diff->m.";\n";
	$max_tenure =get_max_tenure( $_SESSION["orgid"]);
			$json_max_tenure = json_encode($max_tenure);
			echo "var max_tenure =  ". $json_max_tenure.";\n";	
				
				$stats=get_client_stats( $_SESSION["orgid"]);
				$json_stats = json_encode($stats);
			echo "var stats=". $json_stats.";\n";	

	?>
</script>
	



<!DOCTYPE html>
<html>

<head>

	<meta charset="UTF-8">

	<title>Organization analysis page</title>

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style_demo.css">
    <link rel="stylesheet" href="css/bootstrap-slider.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/ionicons.min.css">
      
	<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
  	<script src="js/addons/Chart.bundle.js"></script>
  	<script src="js/assets/org_demo_graph.js"></script>
 	<script src="js/addons/bootstrap-slider.js"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

 	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
	<link rel="manifest" href="favicon/site.webmanifest">
 
</head>


<body>

	<div class="background">
		
		<div class="bg-div">
		    <button type="button" id="sidebarCollapse" class="btn btn-info" style="margin-top:8px">
               <i class="ion-android-menu"></i>
               <span></span>
           	</button>
		    <img class="logo-img" src="images/platypus_logo.png" width="167" height="55" ALT="align box" ALIGN=CENTER>
		   	<a href="http://theplatypus.io" target="_blank"><img class="" src="images/piechart.svg" width="55" height="55" ALT="align box" align="center" style="float:right"></a>
		</div>

		<div class="wrapper">
		    <!-- Sidebar -->
		    <nav id="sidebar" class="active">
		        <div class="sidebar-header">
		        </div>
		        <ul class="list-unstyled components">
		            <p></p>
		            <li class="active">
		                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">Dashboard</a>
		                <ul class="collapse list-unstyled show" id="pageSubmenu">
		                </ul>
		            </li>
		            <li class="li_non_active">
		                <a href="org_demo_culture.php" >Culture</a>
		                <!--<a href="org_demo_culture.php" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Culture</a>-->
		                <ul class="collapse list-unstyled" id="homeSubmenu">
		                    <li>
		                        <a href="#">Dashboard</a>
		                    </li>
		                    <li>
		                        <a href="#">Split</a>
		                    </li>
		                    <li>
		                        <a href="#">Sample</a>
		                    </li>
		                    <li>
		                        <a href="#">Timeline</a>
		                    </li>
		                </ul>
		            </li>
		            <li  class="li_non_active">
		            	<a href="org_demo_recruitment.php" >Recruitment</a>
		                <!--<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Recruitment</a>-->
		                <ul class="collapse list-unstyled" id="pageSubmenu">
		                    <li id="li_candidate" class="submenu active_tab active">
		                        <a href="#">Candidate</a>
		                    </li>
		                    <li id="li_setup" class="submenu">
		                        <a href="#">Setup Structure</a>
		                    </li>
		                </ul>
		            </li>
		        </ul>
		        <div id="div_container_def" style="margin:10px; font-weight: 500; font-size:large">
		        	Definition:<br>
		        	<div id="div_definition"></div>
		    	</div>
		    </nav>

			<div class="container">
			  	<div class="row">
			    	<div class="column"  id='div_column_0'>

				    	<!--<a href="org_open_reg.php" class="btn btn-primary">Setup this structure</a>
				    	<a href="org_profiles.php" class="btn btn-primary">Org profiles</a>-->
				    	<div id='div_vector_org_polar' class="block">
							<div id="div_select_graph_loc_dep" class="select_graph_loc_dep">
								<select id="select_graph_loc" name="select_graph_loc" onchange="changeGraphFilter(0,'location',this)">		
								</select> 
							    <select id="select_graph_dep" name="select_graph_dep" onchange="changeGraphFilter(0,'department',this)"> 
								</select> 
							</div>
							<div id ="div_select_tenure" class="div_select_tenure">
								<select  id="select_tenure" class='select_tenure' onchange="changeGraphFilter(0,'tenure',this)">
									<option value="0" disabled selected>Minimum Tenure</option>
								</select>
							</div>
							
							<div id="div_select_gender" class="div_select_gender">
								<select  id="select_gender" class='select_gender' onchange="changeGraphFilter(0,'gender',this)">
									<option value="A" disabled selected>Gender</option>
									<option value="A">All</option>
								      <option value="F">Female</option>
								      <option value="M">Male</option>
								</select>
							</div>
							<div id="div_canvas">
							    <canvas id="orgChart"></canvas>
							</div>
						</div>
					</div>

					<div id="div_column_1" class="column" >
				    	<div id="div_vector_org_ranking" class="block">
							<canvas id="vectorOrgChart" ></canvas>
						</div>
						
					</div>
					<div id='div_parking' style="display:none;">
						<?php
//necessary to display the different elements or not
	echo "<div id='div_onboard_stats' class='block'>";
				
	const MAX_NON_STARTED = 25; 

		  	echo "<table class='table' style='border: 1px solid black;'>";
		  	echo "<tr><th>Complete</th><th>On going</th><th>Not started</th><th>Total</th></tr><tr>";
		  	echo "<td>".$stats["complete"]."</td>";
		  	echo "<td>".$stats["notcomplete"]."</td>";
		  	echo "<td>".$stats["notstarted"]."</td>";
		  	echo "<td>".$stats["total"]."</td>";

		  	echo "</tr></table>";

		  	//List of emails
		  	$nonstarted = get_all_nonstarted($_SESSION["orgid"]);
		  	//echo print_r($nonstarted);
		  	$notcomplete = get_all_notcomplete($_SESSION["orgid"]);
		  	//echo print_r($notcomplete);

		  	if(sizeof($nonstarted)>MAX_NON_STARTED)
		  	{
		  		echo "There are more than ".MAX_NON_STARTED." that haven't started.";
		  	} else
		  	{
		  		if(sizeof($nonstarted)<=0) {
		  		echo "All started";
			  	} else {
			  		echo '<div id="div_non_started"><ul>Non started users';
			  			foreach($nonstarted as $index => $user){
			  				echo '<li>'.$user['email'].'</li>';
			  		}	  		
			  		echo '</ul></div>';
			  	}
			  	if(sizeof($notcomplete)<=0) {
		  		echo "";
			  	} else {
			  		echo '<div id="div_not_completed"><ul>Not complete users';
			  		foreach($notcomplete as $index => $user){
			  			echo '<li>'.$user['email'].'</li>';
			  		}
			  		echo '</ul></div>';
			  	}
			}
	echo "</div>";
	?>
					</div>
				</div>
			</div>
		</div>
	</div>
<script src="js/functions/definitions.js"></script>
<script src="js/functions/org_polar.js"></script>
<script src="js/functions/calculation.js"></script>
<script src="js/functions/tools.js"></script>
<script src="js/functions/bar_comparaison.js"></script>
<script>
	var var_type='dashboard';
</script>
<script src="js/functions/timer_minute.js"></script>
<script>
current_page='dashboard';


//all chart stuff

		//initialize the chart
		create_chart("orgChart", "numbers_6");
		

		//initialize bar chart org
		create_bar_chart("vectorOrgChart");

	/***********apply and manage filters for graph side***************/
	//setup arrays for graph filter
	var graph_filters={gender:['A'],department:['All'],location:['All'],tenure:[0]};
	var handle_filters= JSON.parse(JSON.stringify(graph_filters)); 
	//for test purposes as Dixa
	
	var slider_value=0;
	var timeline_array=[];
	var filtered_employees=employees;

	
	populate_select(parent_loc_dep,'select_graph_loc','All','Location');
	populate_select(removeDuplicatesInChild(parent_loc_dep),'select_graph_dep','All','Department');
	
	populateSelectTenure("select_tenure",max_tenure['max']);


//if all profile completed. However this should be replaced with a config of the org that has completed its first step instead
if(stats.complete_full >= stats.total_full*0.9)
{
	screen_dashboard();	
} else {
	screen_onboard();
}



/*****************initialization of the top menu***************/

$("#li_dahsboard").on("click", setup_dashboard);


$('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
//

function setup_dashboard(){
	screen_dashboard();
	submenu_activate('#li_dahsboard');
}


function submenu_activate(tab_id){
	$('.active_tab').removeClass('active');
	$('.active_tab').removeClass('active_tab');
	$(tab_id).addClass('active_tab');	
	$(tab_id).addClass('active');	
}

function screen_dashboard(){
	current_page='dashboard';
	$('#div_parking').append($('.block'));
	$('#div_column_0').append($('#div_vector_org_polar'));
	$('#div_column_1').append($('#div_vector_org_ranking'));
	graph_filter_call(0,orgid,graph_filters);	
}

function screen_onboard(){
	current_page='dashboard_onboard';
	$('#div_parking').append($('.block'));
	$('#div_column_0').append($('#div_onboard_stats'));
	$('.li_non_active').hide();
}

	


//all profiles stuff
var profiles_filters = [];
//add the 3 initial filters corresponding to locationm department and role
profiles_filters.push({'id':0,'vector':'location','operator':'!=','value':'nofudingloc'});
profiles_filters.push({'id':1,'vector':'department','operator':'!=','value':'nofudingdep'});
profiles_filters.push({'id':2,'vector':'tenure','operator':'>=','value':0});
profiles_filters.push({'id':3,'vector':'gender','operator':'!=','value':'nofudingrole'});


//set the tenure when changin the select
function changeGraphFilter(chartId,graph_filter,element) 
{
	//knowing that chart 0 is the org, 1 is candidate and 2 is comparaison chart
	var selector = document.getElementById(element.id);
	//add to the end and remove the first element
	//graph_filters[graph_filter]
	if(chartId==0)
	{
		if(element.id=='select_graph_loc')
		{
			funnel_list(element.id, 'location', 'select_graph_dep','department');	
		}
		graph_filters[graph_filter].push(selector.options[selector.selectedIndex].value);
		graph_filters[graph_filter].shift();
	
	//actualize the graph
		graph_filter_call(chartId,orgid,graph_filters);			
	} 
}



//ajax call for the filtered set
	function graph_filter_call(chartId,orgId,graph_filters) {
		setSelectorFilter("select_gender","gender");
		setSelectorFilter("select_tenure","tenure");
		setSelectorFilter("select_graph_dep","department");
		setSelectorFilter("select_graph_loc","location");
		let request_type = '';

			request_type = 'graph_filter_call';

            $.ajax({
                type: "POST",
                url: "ajax_graph_filter.php",
                datatype: "json",
                data: {requestType: [request_type],
                		graph_filters:graph_filters,
                		orgId:orgId
            				},
                success: function(data) {  
                	//alert(data)             ;     
                    data = JSON.parse(data);
                    //alert(data['count_profiles']);
                    //console.log(data['count_profiles']);
                    if(data['count_profiles']<4){
						alert("The selected set contains less than 4 individuals than we cannot show to keep ThePlatypus Anonymous :) please try another subset");
						setFilterSelect(handle_filters);
						 
                    } else {
                    	//adjust the graph
                    	replace_dataset(chartId,data);
                        setVectorSortingDesc(chartId,data);
    	                    	//adjust the ranking list
    	                    //adjust the data of the graph
    	                    setGraphVector(data);
    	                    
    						handle_filters= JSON.parse(JSON.stringify(graph_filters)); 	
                    
                    }
                    	

                }
            });
        }

</script>
     
	


</body>

</html>