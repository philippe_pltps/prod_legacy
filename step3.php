<?php
// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}
?>
<script>
  <?php
  include 'db_helper/db_util.php';
  include "db_helper/session.php"; //Include PHP MySQL sessions

  if(empty($_SESSION['guid']))
        {
        header("Location: https://www.theplatypus.io"); 
        exit();
      } else 
        {
          //Redirect

            $_SESSION['calling']='';          
            $results = select_data(3,'guid',$_SESSION['guid']);
            $guid = $_SESSION['guid'];
            //insert_probe_stepX_data(3,[$_POST['input_timer']], $guid );  
            //REDIECT if need be
            //redirect_to($guid,3);
            //unstable for the moment

            $field_array = json_encode($results);
            echo "var field_array = ". $field_array.";\n";

            if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["form_proof_3"]))
                {
                  insert_probe_stepX_data(3,['input_timer' => $_POST['input_timer']], $guid );  
                  header("Location: step4.php"); 
                       exit();
                }
        }   
  ?>
  </script>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>The Platypus</title>
  <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <link rel="stylesheet" href="css/styles.css">    
  <link rel="stylesheet" href="css/phil_style.css">
  <script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>  
  <script src="js/addons/Chart.bundle.js"></script> 
  <script src="js/functions/timer.js"></script>
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
  <link rel="manifest" href="favicon/site.webmanifest">
</head>


<body>
	<div class="background">
		<h1>Your Platypus print</h1>
    <h2>Boom! And here you have your first Platypus Print. <br>
      Now you can see what you value most within an organization.<br>
      Just mouse over each vector for more information.<br>
      <div id="div_definition"><br>
      </div>
    </h2>

    <div class="chart-container" style="position: relative; width:60vw; 
    margin:-90px auto 0 auto">
      <canvas id="userChart" ></canvas>
    </div>
    
    <div class="row">
        <div class="column">
          <div class="family_people">
            <h4>People</h4>
            <p>Teamwork</p>
            <p>Communication</p>
            <p>Hierachy</p>
          </div>
        </div>
        <div class="column">
          <div class="family_workplace">
            <h4>Workplace</h4>
            <p>Work Flexibility</p>
            <p>Wellness</p>
            <p>Diversity</p>
          </div>
        </div>
        <div class="column">
          <div class="family_organisational">
            <h4>Organisational</h4>
            <p>Mission & Values</p>
            <p>Career Progression</p>
            <p>Compensation</p>
          </div>
        </div>  
        <div class="column">
          <div class="family_behaviour">
            <h4>Behaviour</h4>
            <p>Leadership</p>
            <p>Personal and Skills Development</p>
            <p>Internal Social Life & Community</p>
          </div>
        </div>
      </div>  
    


    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
      <input type="text" name="form_proof_3" value="form_proof_3" style="display:none">
      <input type="number" name="input_timer" id="input_timer" value="0" style="display:none">
      <div class="footer">
        <ul class="progressbar">
          <li class="active">POINT ALLOCATION</li>
          <li class="active">PRIORITISATION</li>
          <li class="active">PROFILE</li>
          <li>REFLECTION</li>
          <li>FINISH</li>
          <button type="submit" class="button-3">NEXT</button>
        </ul>
      </div>
    </form>
	</div>

</body>
<script src="js/functions/definitions.js"></script>
<script src="js/functions/org_polar.js"></script>
<script>  
    create_chart("userChart", "numbers");
    //add the dataset
    replace_dataset(0,field_array);     
    </script>
</html>