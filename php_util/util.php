<?php

//include "../db_helper/db_util.php";

/*
function redirect_to($guid, $current_page)
{

	session_start();
	//echo "qrf;";
  //TODO to change
	//$status = get_profile_status_guid($guid);
	//echo print_r($status);
  //header("location: ".$status);
	//if status == complete redirect to login
	if($status["status"]=="complete" )
	{
		header("location: index.php");
		exit;
	} elseif(($status["status"]=="None" || $status["status"]==1) && $current_page!=1 ) {

		$_SESSION['calling']='redirect';
		header("location: step1.php");
		exit;
	} elseif($status["status"] != $current_page) {
		
		$_SESSION['calling']='redirect';
		//echo "location: step".$status["status"].".php";
		header("location: step".$status["status"].".php");
		exit;
		} 

}*/

//yuseful to generate a guid
function getGUID(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
        return $uuid;
    }
}

//yuseful to generate random string
function getRandString($length){
  mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
  $charid = strtoupper(md5(uniqid(rand(), true)));
        return substr($charid, 0, $length);
}

//useful to shuffle an array
function custom_shuffle($my_array ) {

  $copy = array();
  while (sizeof($my_array)>0) {
    // takes a rand array elements by its key
    $element = array_rand($my_array);
    // assign the array and its value to an another array
    $copy[$element] = $my_array[$element];
    //delete the element from source array
    unset($my_array[$element]);
  }
  return $copy;
}

//useful transformation to get % of diff between 2 profiles
function getMatch($array1, $array2){
  $percent = 0;
  $counter = 0;
  $sum=0;
  
  foreach($array1 as $field => $value)
  { 
    if(empty($array2[$field]))
    {
    } else 
    {
      if($field != "id" && $field != "guid")
      {
        if($value >= $array2[$field] && $value != 0)
          {
            $percent = 100 - (($value - $array2[$field]) * 100 / $value);
          }
        if($value < $array2[$field] && $array2[$field] != 0)
          {
           $percent = 100 - (($array2[$field] - $value) * 100 / $array2[$field]);
          }
        
        $counter ++;
        $sum = $sum + $percent;
      }
    }    
    
  }

  return ($sum / $counter);
}


//useful definitions
function get_definition($field)
{
  $definition ="arf";
  switch($field)
        {
            case"communication":
            $definition='Clear & effective, courteous and proactive - via multiple channels';
                break;
            case"teamwork":
            $definition='Whether independent or collaborative in nature - between individuals, teams and departments';
                break;
            case"structure" :
            $definition='A Clear and defined Organisational Structure and Hierarchy - regardless of type: Entrepreneurial, Professional, Divisional or Innovative';
                break;
            case"flexibility":
            $definition='The option of flexible working hours and ability to work remotely or from home';
                break;
            case"perks" :
            $definition='Access to office comforts, healthy food and snacks as well as mental and physical programs';
                break;
            case"diversity":
            $definition='Accept each individual\'s differences, embraces their strengths and provides opportunities for all staff to achieve their full potential';
                break;
            case"mission":
            $definition='Visible and clear company values and Mission';
                break;
            case"career" :
            $definition='Moving forward, being promoted, finding new challenges, opportunities to "climb the ladder" within the Organisation';
                break;
            case"compensation" :
            $definition='Access to financial incentives whether commission or bonuses, salary reviews and Pensions';
                break;
            case"leadership":
            $definition='Visible and Accessible Leaders within an Organisation';
                break;
            case"development":
            $definition='Opportunities for internal and external personal development - new skills and learning programs';
                break;
            case"social":
            $definition='Group activities, social events based on shared interests';
                break;
        }
        return $definition;
}

function send_recovery_pwd_mail($to, $code)
{
  $subject = 'The platypus - Reset password';
  
   
  // Compose a simple HTML email message
  $message = '<html><body>';
  $message .= '<h1 style="color:#f40;">Hi '.$to.'</h1>';
  $message .= '<p>We have received a request to reset your Platypus password.<br>';
  $message .="To reset your password, use this link.<br>";
  $message .="It will self-destruct after 24hours.<br>";
  $message .="Reset Your Password<br>";
  $message .="https://theplatypus.io/reset-password?code=".$code."<br></p>";
  $message .= '</body></html>';
   
  // Sending email
  return send_mail($to, $subject, $message);
}

function send_mail($to, $subject, $message){
  $from = constants::NOREPLY;

  // To send HTML mail, the Content-type header must be set
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  // Create email headers
  $headers .= 'From: '.$from."\r\n".
      'Reply-To: '.$from."\r\n" .
      'X-Mailer: PHP/' . phpversion();

  return mail($to, $subject, $message, $headers);
}

function starts_with($string, $startString) 
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
} 

function merge_array_unique($array_data,$attribute)
{
  $arr = [];
  foreach ($array_data as $index => $value) {
    if(!in_array($value[$attribute],$arr))
    {

      array_push($arr, $value[$attribute]);
    }
  }
  return $arr;
}

?>
