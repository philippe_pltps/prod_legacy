<?php

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}

// Initialize the session

include "db_helper/session.php"; //Include PHP MySQL sessions
$_SESSION=[];
//session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    if($_SESSION["orgid"] !=0){
        header("location: org_demo_dashboard.php");
        exit;
    } else {
            header("location: welcome.php");
    exit;
    }
                                

}
?>
<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script> 
<?php
// Include config file
include "db_helper/db_util.php";
 
// Define variables and initialize with empty values
$email = $password = "";
$email_err = $password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 $conn = connection();
    // Check if email is empty
    if(empty(trim($_POST["email"]))){
        $email_err = "Please enter email.<br>";
    } else{
        $email = trim($_POST["email"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.<br>";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate credentials
    if(empty($email_err) && empty($password_err)){
        //all the db_util tog et the data from that email before comparing with the
        $user_info = get_user_info($email);

        if($user_info[0]!="Nothing")
        {
            if(password_verify($password, $user_info["password"]))
            {
                //1 check if consetn exists then insert and 
                //check if form with consent
                
                //after login in, check that the user has the latest consent
                $consent_info = get_last_consent($user_info["id"]);
                if($consent_info[0]=='Nothing' && empty($_POST["consent"]))
                {
                    echo '<script>';
                    echo '$(window).load(function () {';
                    echo '$("#box_doc").show();';
                    echo '});';
                    echo '</script>';
                } 
                if($consent_info[0]=='Nothing' && !empty($_POST["consent"]))
                {
                    insert_user_consent($user_info["id"]);
                }
                    
                //could have been updated by now
                $consent_info = get_last_consent($user_info["id"]);
                if($consent_info[0]!='Nothing')
                {
                    // Store data in session variables
                    $_SESSION["loggedin"] = true;
                    $_SESSION["id"] = $user_info["id"];
                    $_SESSION["email"] = $email;                            
                    
                    // Redirect user to welcome page or org page
                    update_last_connection($user_info["id"]);


                    
                    if($user_info["orgid"] !=0)
                    {
                        $_SESSION["orgid"] = $user_info["orgid"];
                        if($user_info["last_connection"]=='0000-00-00 00:00:00')
                        {
                            $_SESSION['calling']='first_connection';
                            header("location: reset-password.php");    
                        } else {
                            header("location: org_demo_dashboard.php");    
                        }
                        
                    } else 
                    {
                        header("location: welcome.php");
                    }
                }
                
                
            } else{
                // Display an error message if password is not valid
                $password_err = "Password does not match.<br>";
            }
        } else {
            //echo "Oops! Something went wrong. Please try again later.";
            $email_err='This user does not exist<br>';
        }       
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login to Theplatypus</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/styles_cand.css">
        <link rel="stylesheet" href="css/popup_box.css">
  <script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script> 
  <link rel="stylesheet" href="css/cookie_bar.css">
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">


</head>
<body>
    <?php
    include "php_util/cookie_bar.php";
    
    ?>
    <div class="background">
        <div class="bg-div">
            <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
            <nav>
            </nav>
        </div>
        <h1>Login</h1>
        <h2>Please fill in your credentials to login.</h2>
        <div class="card">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                    <div class="banner-content">
                        <div class="signup-form-box">
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                                <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                                    <h3 class="bold">Email</h3>
                                    <span class="help-block"><?php echo $email_err; ?></span>
                                    <input id="input_email"  type="text" name="email" class="form-control" value="<?php echo $email; ?>">
                                </div>    
                                <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                                    <h3 class="bold">Password</h3>
                                    <span class="help-block"><?php echo $password_err; ?></span>
                                    <input type="password" name="password" class="form-control">
                                    <div id="div_forgot_p" class="trigger_popup_fricc">Forgot your password?</div>

                                </div>
                                <div class="form-group">
                                    <button type="submit" class="button-2">LOGIN</button>
                                </div>
                                <p>Don't have an account? <a href="signup.php">Sign up now</a>.</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    
    <div id='box_email' class="hover_bkgr_fricc">
        <div class="popup_box popup_box_email">
            <div class="popupCloseButton">X</div>
            <h3>
                INSTRUCTION SENT
            </h3>
            <p>We sent instructions to change your password to <span id="span_email"></span>, please check both your inbox and spam folder.</p>
        </div>
    </div>
    <div id='box_doc' class="hover_bkgr_fricc">
        <div class="popup_box popup_box_doc">
            <div w3-include-html="assets/eula.html" class='scroll_box_doc'></div>
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <input type="text" name="email" class="form-control" value="<?php echo $email; ?>" style="display: none;">
                <input  type="password" name="password" class="form-control" value="<?php echo $password; ?>" style="display: none;">
                <input  type="password" name="consent" class="form-control" value="<?php echo getRandString(15) ?>" style="display: none;">
            <button class="button-2"> ACCEPT </button>
            <a href="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"  class="button-2"> REFUSE </a>
            </form>
        </div>
    </div>
</body>

<script src="js/functions/cookie_bar.js"></script>
<script src="js/functions/tools.js"></script>
<script>
includeHTML();
</script>
<script type="text/javascript">


$(window).load(function () {
    $("#div_forgot_p").click(function(){
        var email_value = document.getElementById("input_email").value;
        var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
        if (re.test(email_value))
        {
            $('#box_email').show();
            document.getElementById("span_email").innerHTML =email_value;     
            send_pwd_email(email_value);
        }

       //document.getElementById("input_email").value;
    });


    $("#div_doc").click(function(){
        $('#box_doc').show();
    });

    $('.hover_bkgr_fricc').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
    $('.popupCloseButton').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
});


function send_pwd_email(email) {
            $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['sendPwdEmail'],
                        arguments:[email]
                            },
                success: function(data) {  
                    //console.log(data);
                    
                }
            });
        }

</script>


</html>