<?php

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}

//destroy previous session
// Initialize the session
include "db_helper/session.php"; //Include PHP MySQL sessions
// Unset all of the session variables
//$_SESSION = array();
//session_unset()
// Destroy the session.
//session_destroy();

$_SESSION=[];


// Initialize the session
//$_SESSION = new Session();   //Start a new PHP MySQL session
// Include config file
include "db_helper/db_util.php";


//test for employee is 0CC639A42432B071E83A8D2055478927///demo_employee
//test for candidat eis BDEFE76C03E19E3B60704AAE3DB00576///demo_candidate

?>

<script>
 !function(){var r,e,n,s=window.legal=window.legal||[];if(s.SNIPPET_VERSION="1.2.5",r="https://widgets.legalmonster.com/v1/legal.js",!s.__VERSION__)if(s.invoked)window.console&&console.error&&console.error("Legal.js snippet included twice on page.");else{for(s.invoked=!0,s.methods=["document","signup","user","ensureConsent","enableImpliedConsent"],s.factory=function(n){return function(){var e=Array.prototype.slice.call(arguments);return e.unshift(n),s.push(e),s}},e=0;e<s.methods.length;e++)n=s.methods[e],s[n]=s.factory(n);s.load=function(e,n){var t,o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=r,(t=document.getElementsByTagName("script")[0]).parentNode.insertBefore(o,t),s.__project=e,s.__loadOptions=n||{}}}}();
    legal.load("txaiCH8hffd4sB851CmbrMkF");
    
</script>
<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script> 
<?php


echo "<script>";
if(!empty($_GET['orgpage']))
{
    $org_data = get_orgid($_GET['orgpage']);
} elseif(!empty($_GET['page'])){
    $org_data = get_orgid($_GET['page']);
}


//then get the role from the candidate type should be "candidate_".id
//echo $org_data['type'];
$split = explode("_",$org_data['type']);
echo "var role_data = [];\n";
if(!empty($split[1]))
{
    //role_id i either a reg either an employee id depending
    $org_data['role_id'] = $split[1];
    $org_data['type'] = $split[0];
    if($org_data["type"] == "candidate" )
    {
        $role_data =get_role_info_from_id($org_data['role_id']);
        $json_role_data = json_encode($role_data); 
        echo "role_data=". $json_role_data.";\n";    
    }
    echo "var link_type='".$org_data['type']."';\n";
}

//echo $org_data;
if((empty($org_data) || ($role_data['is_active']==0 && $org_data['type']=='candidate')) 
    && $_GET['page']!='demo_candidate' && $_GET['page']!='demo_employee')
{
    header("Location: index.php");
    exit;
}

//$json_org_data = json_encode($org_data); 
//echo "var org_data=". $json_org_data.";\n";


echo "</script>";
 
// Define variables and initialize with empty values
$email =  $name ="";
$email_err = $name_err ="";
$existing_email ="";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Check if email is empty
    if($org_data["type"] == "candidate" ) 
    {
        if(empty(trim($_POST["name"]))){
        $name_err = "Please enter a name.";
        } else{
            $name = trim($_POST["name"]);
        }    
        if(empty(trim($_POST["email"]))){
            $email_err = "Please enter an email.";
        }
        // invalid emailaddress
        if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL))
        {
           $email_err = "Please enter a valid email address"; 
        }
        $param_email = trim($_POST["email"]);
        if( $_GET['page']!='demo_candidate')
        {
            $user_id = get_user_id($param_email);    
        }
        
        $email = trim($_POST["email"]); 
            if(!empty($user_id) ) //if existing user just link the user with a new candidacy and make an email error so that the person cannot go further and do not display the form
            {
                $email_err = "  ";
                $existing_email='yes';
                insert_candidate_info($user_id, $org_data["orgid"],$role_data['location'],$role_data['department'], $role_data['role_name']);
            }
    }
    
    //if no error and type is employee
    if((!empty($email) && empty($email_err) && empty($name_err) && empty($user_id)) || $org_data["type"] == "employee") 
    {
        if(($_POST['legalmonster-consent']=='on' && $org_data["type"] == "candidate") || $org_data["type"] == "employee") 
        {
            //after the second form
            if(empty($_POST["consent"]))
            {
                echo '<script>';
                echo '$(window).load(function () {';
                echo '$("#box_doc").show();';
                echo '});';
                echo '</script>';
            } 
            else 
            {
                //section for getting a user_id or retrieve an existing one
                if($org_data["type"] == "candidate")
                {
                    if($_GET['page']!='demo_candidate')
                    {
                        $user_id=insert_user($email, $name);      
                    } else {
                        $user_id=insert_user('dummy','dummy');      
                    }
                    
                    $status['status']="None";
                } else {
                    //get the id of the employee bqsed on the email
                    
                    $employee_data = get_employee_userid_from_id($org_data['role_id']);
                    $user_id=$employee_data['userid'];
                    $email = $employee_data['email'];
                    if($user_id==0 || $_GET['page']=='demo_employee') //if the employee has an userID =0 then insert a new user
                    {
                        //here the name does not matter as it is an employee
                        if( $_GET['page']!='demo_employee')
                        {
                            $user_id=insert_user($employee_data['email'], $employee_data['email']);    
                        } else {
                            $user_id=insert_user('dummy','dummy');  
                        }
                        
                        $status['status']="None";
                    } else {
                        $status = get_profile_status($user_id); 
                    }
                }
                //in case no problem : lod with idenfier as user_id
                echo '<script>';
                //echo 'legal.user({identifier: "'.$user_id.'"});';
                //echo 'legal.load("txaiCH8hffd4sB851CmbrMkF", {identifier: "'.$user_id.'"});';
                echo '</script>';

                //all user should have a user id by now
                if(!empty($user_id) && $user_id!=0)
                {
                    //this person has give consent for sure because post consent is not empty
                    if($_GET['page']!='demo_employee' &&  $_GET['page']!='demo_candidate')
                    {
                        insert_user_consent($user_id);    
                    }
                    
                    if($org_data["type"] == "employee")
                    {//link employee and user id
                        $affected_rows = update_employee_info($user_id, $org_data["orgid"],$email);
                        if($affected_rows==0){
                            //aka used the employee page without having a registered email in employee
                         //   insert_candidate_info($user_id, $org_data["orgid"], "","");
                        }
                    } else {
                        //setup a candidate with info

                        insert_candidate_info($user_id, $org_data["orgid"],$role_data['location'],$role_data['department'], $role_data['role_name']);
                    }   
                    //if complete error message
                    if($status['status']=="complete")
                    {
                        $email_err = "You have already complete your profile, if you want to consult it go to https://theplatypus.io with email and password. Thank you";
                    } else{
                        //otherwise log the person at the right step
                        $_SESSION["loggedin"] = true;
                        $_SESSION["id"] = $user_id;
                        $_SESSION["email"] = $email;    
                        $_SESSION["source"] = $org_data["orgid"];
                        $_SESSION["type"] = $org_data["type"];
                        if($email=='test@theplatypus.io')
                        {
                            $_SESSION['is_test']='true';
                        }
                        if($status['status']=="None")
                        {
                            header("location: step1.php");
                        } else{
                            $_SESSION["guid"] = $status['guid'];
                            $_SESSION["calling"] = "candidate";
                            switch($status['status']){
                                case 1: header("location: step1.php");
                                break;
                                case 2: header("location: step2.php");
                                break;
                                case 3: header("location: step4.php");
                                break;
                                default: header("location: step1.php");
                                break;
                            } 
                        }
                    }
                } else{
                echo "Something went wrong.";
                }
            }
        } else 
        {//this if for phishing, normally the next button will not be displayed if not checked
            //in case no user id problem but laod
            echo "An error occured, please try again";
            echo '<script>';
          //  echo 'legal.load("txaiCH8hffd4sB851CmbrMkF");';
            echo '</script>';
        }
    }
} else {
    //in case no post then jsut load 
    echo '<script>';
    //echo 'legal.load("txaiCH8hffd4sB851CmbrMkF");';
    echo '</script>';
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $org_data["name"]. ' '. $org_data["type"]; ?> page</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/styles_cand.css">
    <script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>  
      <link rel="stylesheet" href="css/cookie_bar.css">
          <link rel="stylesheet" href="css/popup_box.css">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/site.webmanifest">
</head>
<body>
        <?php
    include "php_util/cookie_bar.php";
    
    ?>
    <div class="bck">
        <div class="bg-div">
            <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
            <a href="http://theplatypus.io" target="_blank"><img class="logo-img" src="images/piechart.svg" width="60" height="65" ALT="align box" align="center" style="float:right"></a>
            <nav>
            </nav>
        </div>
        <h1>Let's create your Platypus Print</h1>
        <h2>
            <?php 
            echo (($org_data["type"] == "candidate" && sizeof($role_data)>0) 
                ? "Thanks for applying to the ".$role_data['role_name']." role at ".$org_data['name']."<br>"
                : '');

            

            echo (($org_data["type"] == "candidate") 
                ? $org_data["name"]." has teamed up with Platypus to help better understand what is important to you<br>" 
                : "ThePlatypus has teamed up with ".$org_data["name"]." to help better understand what is important to you - the Employees<br>");
            echo 'Why? <b>Because everyone adds to the Culture and Values of an Organisation</b><br>';
            echo '<p class="fifteen">The questionnaire takes approximately 10 mins to complete' ;
            echo (($org_data["type"] == "candidate") ? '' :' and it is anonymous!');
            echo '</p> ';
            ?>
            
        </h2>
        <div class="card">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                    <div class="banner-content">
                        <div class="signup-form-box">
                            <h3 class="bold"><?php 
                            if(($org_data["type"] == "candidate") )
                            {
                                echo ($existing_email!='yes') ? 'Please complete the following to proceed' : 'Thank you for using Platypus. Because you already have a profile, we will sent it further to the recruiting team for the '.$role_data['role_name']. ' role';    
                            }
                            
                            ?></h3>
                            <form action="<?php 
                            if(!empty($_GET['orgpage']))
                            echo htmlspecialchars($_SERVER["PHP_SELF"]."?orgpage=".$_GET['orgpage']); 
                            else 
                            echo htmlspecialchars($_SERVER["PHP_SELF"]."?page=".$_GET['page']); 
                            ?>" method="post" <?php echo ($existing_email!='yes') ? '' : 'style="display:none;"'  ?> >
                                <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>" <?php echo ($org_data["type"] == "candidate") ? '' : 'style="display:none;"'  ?> >
                                    <input type="name" name="name"  value="<?php echo $name; ?>" placeholder="name">
                                    <span class="help-block"><?php echo $name_err; ?></span>
                                </div>  
                                <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>" <?php echo ($org_data["type"] == "candidate") ? '' : 'style="display:none;"'  ?> >
                                    <input id=input_email type="text" name="email"  value="<?php echo $email; ?>" 
                                    placeholder="mail@mail.com" >
                                </div>    
                                <span class="help-block"><?php echo $email_err; ?></span>
                                <br>
                                <?php
                                //special for candidates and when consent notice not displayed
                                if($org_data["type"] == "candidate" && empty($_POST['consent']) )
                                {
                                    echo '<script>';
                                    echo 'legal.signup("signup-consent", {outerBorderWidth:0, outerPaddingSize:0,});';
                                    echo '</script>';
                                    echo '<div id="signup-consent"></div>    ';
                                }
                                
                                ?>
                                <p class="small">
                                None of your personal information will be shared with any third parties. <br>You can request access to this information at any time via
                                <a href="mailto:support@theplatypus.io">support@theplatypus.io </a> 
                                <span class="bold"> </span>
                                </p>
                                <?php
                                //do not display if your email exists already
                                echo ($existing_email!='yes') ? '<button type="submit" class="button-2">Start</button>':'';
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>   
    <div id='box_doc' class="hover_bkgr_fricc">
        <div class="popup_box popup_box_doc">
            <div w3-include-html="assets/eula.html" class='scroll_box_doc'></div>
            <form action="<?php 
                            if(!empty($_GET['orgpage']))
                            echo htmlspecialchars($_SERVER["PHP_SELF"]."?orgpage=".$_GET['orgpage']); 
                            else 
                            echo htmlspecialchars($_SERVER["PHP_SELF"]."?page=".$_GET['page']); 
                            ?>" method="post">
                <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" style="display: none;">
                <input  type="text" name="email" class="form-control" value="<?php echo $email; ?>" style="display: none;">
                <input  type="password" name="consent" class="form-control" value="<?php echo getRandString(15) ?>" style="display: none;">
                <input  type="text" name="legalmonster-consent" class="form-control" value="on" style="display: none;">
            <button class="button-7"> ACCEPT </button>
            <a href="<?php 
                            if(!empty($_GET['orgpage']))
                            echo htmlspecialchars($_SERVER["PHP_SELF"]."?orgpage=".$_GET['orgpage']); 
                            else 
                            echo htmlspecialchars($_SERVER["PHP_SELF"]."?page=".$_GET['page']); 
                            ?>"  class="button-2"> REFUSE </a>
            </form>
        </div>
    </div>
</body>
</html>
<script src="js/functions/cookie_bar.js"></script>
<script src="js/functions/tools.js"></script>
<script>
includeHTML();
</script>
<script>

</script>