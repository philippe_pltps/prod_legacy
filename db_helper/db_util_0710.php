

<?php



// Include config file
include "../../initialisation.php";
include "php_util/util.php";

//helper choose table
function switch_tables($step){
	switch($step){
			case 2: 
				$db_table = 'pltps_fields_answers';
				break;
			case 3: 
				$db_table = 'pltps_fields_preferences';
				break;
			case 4: 
				$db_table = 'pltps_fields_coeff';
				break;			
		}
	
	return $db_table;
}

/**********************INSERT****************************************************************************************************************/
/**********************INSERT****************************************************************************************************************/
/**********************INSERT****************************************************************************************************************/
/**********************INSERT****************************************************************************************************************/
/**********************INSERT****************************************************************************************************************/
//insert a new reg
function insert_org_role($orgid, $role)
{   
  $mysqli = connection();               
  $stmt = $mysqli->prepare("INSERT into pltps_org_regs (orgid,role_name,locdepid,is_active,creation_ts)
  values 
  (?,?, 
    (SELECT id from pltps_org_departments 
    where orgid = ? 
    and locationid = (select id from pltps_org_locations where orgid= ? and name = ?) 
    and name = ? 
     LIMIT 1),1,NOW()
  )");
  $stmt->bind_param("isiiss",$orgid,$role['role_name'],$orgid,$orgid, $role['location'] , $role['department']);
  $stmt->execute();
  $mysqli->insert_id;
  $stmt->close();
  $latest_id = $mysqli->insert_id;

  //now input a code into the link page
  $type =  "candidate_".$latest_id;
  $code = getRandString(32);
  $full_url = "https://theplatypus.io/candidate.php?page=".$code;
  $sql = "INSERT into pltps_org_links (orgid,code,full_url,type) values (?,?, ?,?)";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("isss",$orgid,$code,$full_url, $type);
  $stmt->execute();
  $stmt->close();
  //return get_link($orgid,$type);
  return $full_url;
}

function insert_org_location($orgid,$name)
{  
  $mysqli = connection();  
  $sql = "INSERT INTO pltps_org_locations (orgid,name) VALUES (?,?)";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("is", $orgid, $name);
  $stmt->execute();
  return $mysqli->insert_id;
  $stmt->close();
}

function insert_org_department($orgid,$name,$location)
{  
  $mysqli = connection();  
  $sql = "INSERT INTO pltps_org_departments (orgid,name,locationid) VALUES (?,?,(select id from pltps_org_locations where orgid=? and name = ?))";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("isis", $orgid, $name, $orgid, $location);
  $stmt->execute();
  return $mysqli->insert_id;
  $stmt->close();
}

//insert data from the questions into the answer
/***
data array is the data to put (fields and values
value to check will be the element of the table which will give the go to insert into the db. e.g. 'balance' if it is there then we continue, can help do the steps and continuity
step is which page are we in in order to isolate the different answers
***/
function insert_fields_data($data_array, $value_to_check, $step, $guid)

{  
  if (isset($data_array[$value_to_check])){
        
		//put the data into strings with comma separated values

        foreach($data_array as $field => $value) 
        {
          if($field !='guid' && $field !='id')
          {
            $fields = $fields . $field . ",";
      $values = $values . $value . ",";            
          }			
        }		
		
		$db_table = switch_tables($step);
	
		$conn = connection();			  			  
			  $sql = "insert into ".$db_table." (".$fields."guid) values (".$values."'".$guid."');";  
        if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
		mysqli_close($conn);
		return $guid;
      }
}

//insert answers data from the step2
/***
insert for each the field, the size of the set, the priority choosen and the guid of that questionnnaire
this could eb useful if we decide to change the number of points allocated per priority
***/
function insert_answers_step2($data_array, $guid)
{  	
	$conn = connection();			  			  
	foreach($data_array as $field => $value) 
        {			
          if($field !='input_timer' && $field !='form_proof_2')
          {
            $split = explode("_",$_POST[$field]);
            $sql = "insert into pltps_answers_step2 (field, size, priority, guid) values ('".$field."', ".$split[0].",".$split[1].",'".$guid."');";  
            if(!mysqli_query($conn, $sql))               
              {
                 //echo "Error: " . $sql . "" . mysqli_error($conn);
                echo 'error';
              }   
          } 
        }	        
		mysqli_close($conn); 
}

function insert_answers_step4($vector, $guid)
{   
  $mysqli = connection();  
  //$stmt = $mysqli->prepare("INSERT into pltps_answers_step4 (vector, guid) values (?, ?)");
  if (!($stmt = $mysqli->prepare("INSERT into pltps_answers_step4 (vector, guid) values (?, ?)"))) {
    echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
}
if (!$stmt->bind_param("ss", $vector,$guid)) {
    echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
}
if (!$stmt->execute()) {
    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
}
$stmt->close();
      
}


//insert probe data for step1
function insert_probe_step1_data ($data_array, $guid)
{
  $conn = connection();               
  $sql = "insert into pltps_probe_step1 (guid, timer, click_plus, click_minus) values 
  ('".$guid."', ".$data_array['input_timer'].",".$data_array['input_plus'].",".$data_array['input_minus'].");";  
  if(!mysqli_query($conn, $sql))               
    {
      //echo "Error: " . $sql . "" . mysqli_error($conn);
      echo 'error';
    }                 
    mysqli_close($conn);
}

//insert probe data for step1
function insert_probe_stepX_data ($step, $data_array, $guid )
{

  $mysqli = connection();  
  $sql = "INSERT into pltps_probe_step".$step." (guid, timer) values (?,?)";  
  if (!($stmt = $mysqli->prepare($sql))) {
      echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }
  if (!$stmt->bind_param("si", $guid,$data_array['input_timer'] )) {
      echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  if (!$stmt->execute()) {
      echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  $stmt->close();
}




//insert current status
/***
insert for each the field, the size of the set, the priority choosen and the guid of that questionnnaire
this could eb useful if we decide to change the number of points allocated per priority
***/
function insert_user_profile_status($guid, $userid, $step)
{  
  
  $conn = connection();               
          if($step ==1)
          {
            $sql = "insert into pltps_candidate_profiles (guid, userid, update_ts, status) values ('".$guid."', ".$userid.",NOW(),'".$step."');";  
          } else {
            $sql = " update pltps_candidate_profiles set status ='".$step."', update_ts=NOW() where guid ='".$guid."'";
          }
            if(!mysqli_query($conn, $sql))               
              {
                 //echo "Error: " . $sql . "" . mysqli_error($conn);
                echo 'error';
              }           
             
    mysqli_close($conn);      
}

//insert candidate infos
/***
insert for each the field, the size of the set, the priority choosen and the guid of that questionnnaire
this could eb useful if we decide to change the number of points allocated per priority
***/
function insert_candidate_info($userid, $orgid, $location, $department,$role_name)
{  
  $mysqli = connection();  
  $sql = "INSERT into pltps_candidate_info (userid, orgid, location, department, role) 
    values 
    (?,?,?,?,?)";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("iisss", $userid, $orgid, $location, $department,$role_name);
  $stmt->execute();
  return $mysqli->insert_id;
  $stmt->close();

}

//insert user for candidate page without pwd
/***
insert for each the field, the size of the set, the priority choosen and the guid of that questionnnaire
this could eb useful if we decide to change the number of points allocated per priority
***/
function insert_user($email,$name)
{  
  $mysqli = connection();  
  $stmt = $mysqli->prepare("INSERT INTO pltps_users (email,name,creation_ts) VALUES (AES_ENCRYPT(?,?),AES_ENCRYPT(?,?), NOW())");
  $key = constants::MYKEY;
  $stmt->bind_param("ssss", $email,$key, $name, $key);
  $stmt->execute();
  return $mysqli->insert_id;
  $stmt->close();
}

//insert user with pwd from signup page
/***
insert for each the field, the size of the set, the priority choosen and the guid of that questionnnaire
this could eb useful if we decide to change the number of points allocated per priority
***/
function insert_user_pwd($email,$pwd)
{  
  $mysqli = connection();  
  $sql = "INSERT INTO pltps_users (email, password,creation_ts) VALUES (AES_ENCRYPT(?,?),?, NOW())";  
  if (!($stmt = $mysqli->prepare($sql))) {
      echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }
  $key = constants::MYKEY;
  if (!$stmt->bind_param("sss", $email,$key, $pwd)) {
      echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  if (!$stmt->execute()) {
      echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  return $mysqli->insert_id;
  $stmt->close();

}

//insert a pwd recovery request
/***
***/
function insert_pwd_request($email)
{  
  
  $code = getRandString(32);

  update_pwd_request($email);
 
  $mysqli = connection();  
  $sql = "INSERT INTO pltps_pwd_recovery (
      userid, code,creation_ts,is_active) VALUES ((select id from pltps_users where email =AES_ENCRYPT(?,?) limit 1), ?, NOW(),1)";
  $stmt = $mysqli->prepare($sql);
  $key = constants::MYKEY;
  $stmt->bind_param("sss", $email, $key, $code);
  $stmt->execute();
  $stmt->close();

  return $code;
  $stmt->close();
}


//insert user consent
/***
update
***/
function insert_user_consent($userid)
{  
  $mysqli = connection();  
  $stmt = $mysqli->prepare("INSERT INTO pltps_users_eula (userid, consent_ts) VALUES (?, NOW())");
  
  $stmt->bind_param("i", $userid);
  $stmt->execute();
  return $mysqli->insert_id;
  $stmt->close();
}

function insert_employee_hire($userid)
{
  $mysqli = connection();             
  $sql="INSERT into pltps_employee_info (userid,orgid,is_active, department, location, tenure, creation_ts)
SELECT 
userid, orgid, 1, AES_ENCRYPT(department,?),AES_ENCRYPT(location,?), 0, NOW()
from 
pltps_candidate_info where userid =?";  
$key = constants::MYKEY;
  $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("ssi",$key,$key, $userid);
  $stmt->execute();
  return $stmt->affected_rows;
  $stmt->close();
}

function insert_survey_answers($survey_id,$userid,$array_answers)
{
  $mysqli = connection();             
  foreach($array_answers as $vector => $value)
  {
    $sql="INSERT into pltps_survey_answers (surveyid,userid,vector,rating)
    values (?,?,?,?)";  

    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("iisi",$survey_id,$userid,$vector,$value);
    $stmt->execute();  
  }
  $stmt->close();
}

function insert_user_usage($user_id,$type)
{
    $mysqli = connection();               
  $sql = "INSERT INTO pltps_user_usages 
    (userid, value,type)
    values (?,1,?)";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("is", $user_id, $type);
  $stmt->execute();
  $stmt->close();
}


/**********************UPDATE****************************/
/**********************UPDATE****************************/
/**********************UPDATE****************************/
/**********************UPDATE****************************/
/**********************UPDATE****************************/
/***
Update pwd from finish page if decide to keep the profile
***/
function update_user_pwd($userid, $pwd)
{     
             
    mysqli_close($conn);    
      $mysqli = connection();               
  $stmt = $mysqli->prepare("UPDATE pltps_users SET password = ? WHERE id = ?");
  $stmt->bind_param("si",$pwd, $userid);
  $stmt->execute();
  $stmt->close();
}

function update_last_connection($userid)
{
  $mysqli = connection();               
  $stmt = $mysqli->prepare("UPDATE pltps_users SET last_connection =NOW() WHERE id = ?");
  $stmt->bind_param("i", $userid);
  $stmt->execute();
  $stmt->close();

//also update the date and iteration of the connection
  $mysqli = connection();               
  $sql = "INSERT INTO pltps_user_connections 
    (userid, connection_ts)
    values (?,NOW())";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $userid);
  $stmt->execute();
  $stmt->close();

}


function update_employee_info($userid, $orgid, $email)
{
  $mysqli = connection();               
  $stmt = $mysqli->prepare("UPDATE pltps_employee_info set userid =? where orgid= ? and email=AES_ENCRYPT(?,?) and userid=0");
  $key = constants::MYKEY;
  $stmt->bind_param("iiss",$userid, $orgid, $email, $key);
  $stmt->execute();
  return $stmt->affected_rows;
  $stmt->close();
}

//deactivate data for a role
function deactivate_org_role($orgid, $role)
{   

    $mysqli = connection();               
  $stmt = $mysqli->prepare("update pltps_org_regs set is_active =0 where orgid= ? 
  and role_name=?");
  $stmt->bind_param("is",$orgid, $role);
  $stmt->execute();
  $stmt->close();
}


//deactivate link for survey
function deactivate_survey_link($code)
{   

    $mysqli = connection();               
  $stmt = $mysqli->prepare("update pltps_survey_links set is_active =0 where code= ? ");
  $stmt->bind_param("s",$code);
  $stmt->execute();
  $stmt->close();
}

//de-active all the links for an email
function update_pwd_request($email)
{  
  $mysqli = connection();  
  //first set all other links to not active
  $stmt = $mysqli->prepare("UPDATE pltps_pwd_recovery set is_active=0 where userid = (select id from pltps_users where email =AES_ENCRYPT(?,?) limit 1)");
  $key = constants::MYKEY;
  $stmt->bind_param("ss", $email, $key);
  $stmt->execute();
  $stmt->close();
}

function update_candidate_check($userid, $is_selected)
{
  $mysqli = connection();               
  $stmt = $mysqli->prepare("UPDATE pltps_candidate_info set is_selected = ? where userid =?");
    $stmt->bind_param("ii",$is_selected, $userid);
  $stmt->execute();
  return $stmt->affected_rows;
  $stmt->close();
}



function update_org_anonymous($orgid, $is_anonymous)
{
  $mysqli = connection();               
  $stmt = $mysqli->prepare("UPDATE pltps_org_configuration set anonymous = ? where orgid =?");
    $stmt->bind_param("ii",$is_anonymous, $orgid);
  $stmt->execute();
  return $stmt->affected_rows;
  $stmt->close();
}

/**********************DELETE****************************/
/**********************DELETE****************************/
/**********************DELETE****************************/
/**********************DELETE****************************/
/**********************DELETE****************************/





/**********************SELECT****************************/
/**********************SELECT****************************/
/**********************SELECT****************************/
/**********************SELECT****************************/
/**********************SELECT****************************/


//used in the company page to get the link 
function get_all_links($orgid){
  $conn = connection();
  $sql = "SELECT code, full_url, default_email, type
  FROM pltps_org_links
  where orgid=".$orgid." order by type asc;";  
  if(!mysqli_query($conn, $sql))               
    {
      //echo "Error: " . $sql . "" . mysqli_error($conn);
      echo 'error';
    }     
  $retval = mysqli_query($conn, $sql);

  $full_list=[];
  while ($row = mysqli_fetch_assoc($retval)) 
  {
    array_push($full_list,$row);
  }
  return  $full_list;
}

function get_link($orgid, $type)
{   

$sql = "SELECT code, full_url
  FROM pltps_org_links
  where orgid=? and type =? order by id asc LIMIT 1;";
  $mysqli = connection();
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("is", $orgid, $type);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
  return $result;

}


//used in the candidate page to auto complete the form
function get_employee_info($orgid, $email){
  $mysqli = connection();
$sql = "SELECT 
pei.id,pei.userid,orgid,pei.is_active,creation_ts,
AES_DECRYPT(department,?) as department,
AES_DECRYPT(location,?) as location,
tenure as tenure,
startdate as startdate,
AES_DECRYPT(gender,?) as gender,
AES_DECRYPT(email,?) as email,
AES_DECRYPT(pel.level,?) as level,
AES_DECRYPT(pel.title,?) as title
 FROM pltps_employee_info as pei
 left join pltps_employee_level as pel on pei.id = pel.employeeid
 WHERE email =AES_ENCRYPT(?,?)  and orgid = ? and pei.is_active=1";
 //echo $sql;
$stmt = $mysqli->prepare($sql);
$key = constants::MYKEY;
$stmt->bind_param("ssssssssi", $key, $key, $key, $key, $key, $key, $email, $key, $orgid);
$stmt->execute();
$result = $stmt->get_result()->fetch_assoc();
      if(!$result) {
        return ["Nothing"];
        //return $sql;
      } else {
        return $result;
      }
}

//used in the login to check that the hashed pwd is good
function get_user_info($email){
  $sql = "SELECT id, AES_DECRYPT(email,?) as email, password, orgid,last_connection FROM pltps_users WHERE email =AES_ENCRYPT(?,?)";
  $mysqli = connection();
  $key = constants::MYKEY;
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("sss", $key, $email, $key);
$stmt->execute();
$result = $stmt->get_result()->fetch_assoc();
      if(!$result) {
        return ["Nothing"];
      } else {
        return $result;
      }

}


function get_last_consent($userid){
  $sql = "SELECT id, consent_ts FROM pltps_users_eula WHERE userid=?";
  $mysqli = connection();

  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i",$userid);
$stmt->execute();
$result = $stmt->get_result()->fetch_assoc();
      if(!$result) {
        return ["Nothing"];
      } else {
        return $result;
      }

}

//get user id from email
/****
***/
function get_user_id($email)
{  

  $sql = "SELECT id FROM pltps_users WHERE email =AES_ENCRYPT(?,?)
        order by id desc
        LIMIT 1";
  $mysqli = connection();
  $stmt = $mysqli->prepare($sql);
  $key = constants::MYKEY;
  $stmt->bind_param("ss", $email, $key);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
      if(!$result) {
        return "";
      } else {
        return $result["id"];
      }
}


//get employee id from email
/****
***/
function get_employee_id($email)
{  

  $sql = "SELECT id FROM pltps_employee_info WHERE email =AES_ENCRYPT(?,?)
        order by id desc
        LIMIT 1";
  $mysqli = connection();
  $stmt = $mysqli->prepare($sql);
    $key = constants::MYKEY;
  $stmt->bind_param("ss", $email, $key);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
      if(!$result) {
        return "";
      } else {
        return $result["id"];
      }
}

//get employee userid from email
/****
***/
function get_employee_userid($email)
{  

  $sql = "SELECT userid FROM pltps_employee_info WHERE email =AES_ENCRYPT(?,?)
        order by userid desc
        LIMIT 1";
  $mysqli = connection();
  $stmt = $mysqli->prepare($sql);
  $key = constants::MYKEY;
  $stmt->bind_param("ss", $email, $key);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
      if(!$result) {
        return "";
      } else {
        return $result["userid"];
      }
}

//get employee userid from id
/****
***/
function get_employee_userid_from_id($id)
{  

  $sql = "SELECT userid,AES_DECRYPT(email,?) as email FROM pltps_employee_info WHERE id= ?
        order by userid desc
        LIMIT 1";
  $mysqli = connection();
  $stmt = $mysqli->prepare($sql);
  $key = constants::MYKEY;
  $stmt->bind_param("si", $key, $id);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
      if(!$result) {
        return "";
      } else {
        return $result;
      }
}



//check the select of a specific state for a specific id
/****
$step is the current steps and will determine which table to extract data from
$id_to_use is there to define if we are using the unique auto increment 'id' or the 'guid' of a line (good to follow up on the process)
$var_guid is the value of the id or the guid
***/
function select_data($step, $id_to_use ,$var_guid)
{  
	$db_table = switch_tables($step);
	
		$conn = connection();
			  $sql = "select 
        id, guid,
CAST(communication AS DECIMAL(10,2))as 'communication',
CAST( teamwork AS DECIMAL(10,2))as 'teamwork',
 CAST(structure AS DECIMAL(10,2))as 'structure',
 CAST(flexibility AS DECIMAL(10,2))as 'flexibility',
 CAST(perks AS DECIMAL(10,2))as 'perks',
CAST( diversity AS DECIMAL(10,2))as 'diversity',
CAST(mission AS DECIMAL(10,2))as 'mission',
CAST( career AS DECIMAL(10,2))as 'career',
CAST( compensation AS DECIMAL(10,2))as 'compensation',
CAST( leadership AS DECIMAL(10,2))as 'leadership',
CAST( development AS DECIMAL(10,2))as 'development',
CAST( social AS DECIMAL(10,2)) as 'social' 
         from ".$db_table." where ".$id_to_use." = '".$var_guid."' order by id desc;";  

        if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
			  $retval = mysqli_query($conn, $sql);
        return  mysqli_fetch_assoc($retval);
               
}




//check if person has a graph
//get the staus and the guid of a user
/****
***/
function get_profile_status($userid)
{   
  
    $conn = connection();
        $sql = "select * from pltps_candidate_profiles where userid =".$userid." 
        order by id desc
        LIMIT 1;";  
       if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
        $retval = mysqli_query($conn, $sql);

        $user_status = mysqli_fetch_assoc($retval);
        if(!isset($user_status['guid']))
      {
        return  ["status"=>"None"];          //nothing as kto create a profile
      } else {
        return ["status"=>$user_status['status'], "guid"=>$user_status['guid']];
      }
}

//get the staus and the guid of a user
/****
***/
function get_profile_status_guid($guid)
{   

$sql = "SELECT status from pltps_candidate_profiles where guid =?
        order by id desc
        LIMIT 1;";
  $mysqli = connection();
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("s", $guid);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
      if(!$result) {
        return "None";
      } else {
        return $result["status"];
      }

}

//get full list of users
function get_all_candidates($orgid)
{   
  $full_list=[];
    $conn = connection();
        $sql = "SELECT pu.id,";
        //avoid displaying names and email in demo page

              $sql = $sql . "AES_DECRYPT(pu.name,'".constants::MYKEY."') as name,AES_DECRYPT(pu.email,'".constants::MYKEY."') as email,pu.creation_ts,";
       $sql = $sql. "pci.department, 
       pci.location,
       pci.role,
       pci.is_selected,
 CAST(communication AS DECIMAL(10,2))as 'communication',
CAST( teamwork AS DECIMAL(10,2))as 'teamwork',
 CAST(structure AS DECIMAL(10,2))as 'structure',
 CAST(flexibility AS DECIMAL(10,2))as 'flexibility',
 CAST(perks AS DECIMAL(10,2))as 'perks',
CAST( diversity AS DECIMAL(10,2))as 'diversity',
CAST(mission AS DECIMAL(10,2))as 'mission',
CAST( career AS DECIMAL(10,2))as 'career',
CAST( compensation AS DECIMAL(10,2))as 'compensation',
CAST( leadership AS DECIMAL(10,2))as 'leadership',
CAST( development AS DECIMAL(10,2))as 'development',
CAST( social AS DECIMAL(10,2)) as 'social' 

        from pltps_users as pu
inner join pltps_candidate_profiles as pcp on pu.id = pcp.userid
inner join pltps_candidate_info as pci on pci.userid = pcp.userid
inner join pltps_fields_coeff as pfc on pcp.guid = pfc.guid
left join pltps_org_regs as por on por.orgid=pci.orgid and por.role_name = pci.role
        where pcp.status ='complete' and pci.is_selected < 2 and pci.orgid=".$orgid;
        if($orgid !=-1) // all orgs in demo page
        {
          $sql = $sql . " and pci.role<>'';";
        } else {
          $sql = $sql . ";";
        }
//echo $sql;
       if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo "error";
            }     
        $retval = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_assoc($retval)) {
      array_push($full_list,$row);
    }
        return  $full_list;
}


function get_candidate_chunks($orgid, $index, $length)
{   
  $full_list=[];
    $conn = connection();
        $sql = "SELECT pu.id,
        AES_DECRYPT(pu.name,'".constants::MYKEY."') as name,AES_DECRYPT(pu.email,'".constants::MYKEY."') as email,pu.creation_ts,
        pci.department, 
             pci.location,
             pci.role,
             pci.is_selected,
       CAST(communication AS DECIMAL(10,2))as 'communication',
      CAST( teamwork AS DECIMAL(10,2))as 'teamwork',
       CAST(structure AS DECIMAL(10,2))as 'structure',
       CAST(flexibility AS DECIMAL(10,2))as 'flexibility',
       CAST(perks AS DECIMAL(10,2))as 'perks',
      CAST( diversity AS DECIMAL(10,2))as 'diversity',
      CAST(mission AS DECIMAL(10,2))as 'mission',
      CAST( career AS DECIMAL(10,2))as 'career',
      CAST( compensation AS DECIMAL(10,2))as 'compensation',
      CAST( leadership AS DECIMAL(10,2))as 'leadership',
      CAST( development AS DECIMAL(10,2))as 'development',
      CAST( social AS DECIMAL(10,2)) as 'social' 

              from pltps_users as pu
      inner join pltps_candidate_profiles as pcp on pu.id = pcp.userid
      inner join pltps_candidate_info as pci on pci.userid = pcp.userid
      inner join pltps_fields_coeff as pfc on pcp.guid = pfc.guid
      left join pltps_org_regs as por on por.orgid=pci.orgid and por.role_name = pci.role
              where pcp.status ='complete' and pci.is_selected < 2 and pci.orgid=".$orgid;
        if($orgid !=-1) // all orgs in demo page
        {
          $sql = $sql . " and pci.role<>''";
        } else {
          $sql = $sql . "";
        }
         $sql.= " limit ".$index.",".$length.";";
//echo $sql;
       if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo "error";
            }     
        $retval = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_assoc($retval)) {
      array_push($full_list,$row);
    }
        return  $full_list;
}


//get avg of all profiles, return only one element
/****
***/
function get_avg_all_profiles($orgid)
{   
  
    $conn = connection();
  

        $sql = "SELECT CAST(AVG(communication) AS DECIMAL(10,2)) as 'communication',
 CAST(AVG(teamwork) AS DECIMAL(10,2)) as 'teamwork',
 CAST(AVG(structure) AS DECIMAL(10,2)) as 'structure',
 CAST(AVG(flexibility) AS DECIMAL(10,2)) as 'flexibility',
 CAST(AVG(perks) AS DECIMAL(10,2)) as 'perks',
 CAST(AVG(diversity) AS DECIMAL(10,2)) as 'diversity',
 CAST(AVG(mission) AS DECIMAL(10,2)) as 'mission',
 CAST(AVG(career) AS DECIMAL(10,2))  as 'career',
 CAST(AVG(compensation) AS DECIMAL(10,2)) as 'compensation',
 CAST(AVG(leadership) AS DECIMAL(10,2)) as 'leadership',
 CAST(AVG(development) AS DECIMAL(10,2)) as 'development',
 CAST(AVG(social) AS DECIMAL(10,2)) as 'social' 
 FROM pltps_fields_coeff as pfc 
 left join pltps_candidate_profiles as pcp on pfc.guid = pcp.guid
 left join pltps_candidate_info as pci on pci.userid = pcp.userid
  where 1  "
 ."and pcp.status='complete'";
         if($orgid !="arf")
        {
          $sql = $sql . " and pci.orgid=".$orgid." and pci.role='';";
        } else {
          $sql = $sql . ";";
        }
//echo $sql;
        if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
        $retval = mysqli_query($conn, $sql);
        return  mysqli_fetch_assoc($retval);
}

/****
get an avg of all profiles according to the fitlers
return only one element
***/
function get_avg_all_profiles_filtered($orgid,$filter_gender,$filter_tenure, $filter_department, $filter_location)
{   
  $today = new DateTime();
    $conn = connection();
    //setup where for genders M|F
        $sql = "SELECT 
  CAST(AVG(communication) AS DECIMAL(10,2)) as 'communication',
 CAST(AVG(teamwork) AS DECIMAL(10,2)) as 'teamwork',
 CAST(AVG(structure) AS DECIMAL(10,2)) as 'structure',
 CAST(AVG(flexibility) AS DECIMAL(10,2)) as 'flexibility',
 CAST(AVG(perks) AS DECIMAL(10,2)) as 'perks',
 CAST(AVG(diversity) AS DECIMAL(10,2)) as 'diversity',
 CAST(AVG(mission) AS DECIMAL(10,2)) as 'mission',
 CAST(AVG(career) AS DECIMAL(10,2)) as 'career',
CAST(AVG(compensation) AS DECIMAL(10,2)) as 'compensation',
 CAST(AVG(leadership) AS DECIMAL(10,2)) as 'leadership',
 CAST(AVG(development) AS DECIMAL(10,2)) as 'development',
 CAST(AVG(social) AS DECIMAL(10,2)) as 'social' ,
 count(pfc.id) as count_profiles
 FROM pltps_fields_coeff as pfc 
 left join pltps_candidate_profiles as pcp on pfc.guid = pcp.guid
 inner join pltps_employee_info as pei on pcp.userid = pei.userid 
 where 1  "
 ."and pei.is_active=1 "
 ."and pcp.status='complete'"
 .get_where_from_filters($filter_gender,$filter_tenure, $filter_department, $filter_location,$today). " "
 ." and pei.orgid=".$orgid.";";

//echo $sql;
        if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
        $retval = mysqli_query($conn, $sql);
        return  mysqli_fetch_assoc($retval);
}

//get full list of users
function get_all_employees($orgid)
{   
  $full_list=[];
    $conn = connection();
    $key = constants::MYKEY;
    $today = new DateTime();
        $sql = "SELECT pu.id,
 CAST(communication AS DECIMAL(10,2))as 'communication',
CAST( teamwork AS DECIMAL(10,2))as 'teamwork',
 CAST(structure AS DECIMAL(10,2))as 'structure',
 CAST(flexibility AS DECIMAL(10,2))as 'flexibility',
 CAST(perks AS DECIMAL(10,2))as 'perks',
CAST( diversity AS DECIMAL(10,2))as 'diversity',
CAST(mission AS DECIMAL(10,2))as 'mission',
CAST( career AS DECIMAL(10,2))as 'career',
CAST( compensation AS DECIMAL(10,2))as 'compensation',
CAST( leadership AS DECIMAL(10,2))as 'leadership',
CAST( development AS DECIMAL(10,2))as 'development',
CAST( social AS DECIMAL(10,2)) as 'social' ,
AES_DECRYPT(pei.location,'".$key."') as 'location',
AES_DECRYPT(pei.department,'".$key."') as 'department',
AES_DECRYPT(pei.gender,'".$key."') as 'gender',
pei.tenure + datediff( '".$today->format('Y-m-d')."', pei.creation_ts)/(30.416667) as tenure

        from pltps_users as pu
inner join pltps_candidate_profiles as pcp on pu.id = pcp.userid
inner join pltps_fields_coeff as pfc on pcp.guid = pfc.guid
inner join pltps_employee_info as pei on pei.userid = pu.id

        where pcp.status ='complete' and pei.orgid=".$orgid.";";
      
//echo $sql;
       if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
        $retval = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_assoc($retval)) {
      array_push($full_list,$row);
    }
        return  $full_list;
}


function get_where_from_filters($filter_gender,$filter_tenure, $filter_department, $filter_location, $date){
  $where_to_return = '';
  $key = constants::MYKEY;
  switch($filter_gender[0]){
      case 'A': 
        $where_to_return =  $where_to_return.'';
        break;
      default :
        $where_to_return =$where_to_return. " and  AES_DECRYPT(pei.gender,'".$key."') in ('".$filter_gender[0]."')";
        break;
      }

     //setup where for tenure
    $where_to_return =$where_to_return. " and  (pei.tenure + FLOOR(datediff('".$date."' , pei.creation_ts)/(30.416667)))/12 >= ".$filter_tenure[0];    

    //setu where for departments if only one seleciton
    switch($filter_department[0]){
      case 'All': 
        $where_to_return =$where_to_return. '';
        break;
      default :
        $where_to_return =$where_to_return. " and  AES_DECRYPT(pei.department,'".$key."') in ('".$filter_department[0]."')";
        break;
      }

    //setu where for departments if only one seleciton
    switch($filter_location[0]){
      case 'All': 
        $where_to_return =$where_to_return. '';
        break;
      default :
        $where_to_return =$where_to_return. " and  AES_DECRYPT(pei.location,'".$key."') in ('".$filter_location[0]."')";
        break;
      }
      return $where_to_return;
}

/****
get  profiles according to the fitlers
return a table
***/
function get_all_profiles_filtered($orgid,$filter_gender,$filter_tenure, $filter_department, $filter_location, $today)
{   
$key = constants::MYKEY;
    $conn = connection();
        $sql = "SELECT 
 communication,
 teamwork,
 structure,
 flexibility,
 perks,
 diversity,
 mission,
 career,
 compensation,
 leadership,
 development,
 social,
 pei.tenure + datediff( '".$today."', pei.creation_ts)/(30.416667) as tenure,
 AES_DECRYPT(pel.level,'".$key."') as level
 FROM pltps_fields_coeff as pfc 
 left join pltps_candidate_profiles as pcp on pfc.guid = pcp.guid
 inner join pltps_employee_info as pei on pcp.userid = pei.userid 
 left join pltps_employee_level as pel on pei.id = pel.employeeid
 where 1  "
  ."and pei.is_active=1 "
 ."and pcp.status='complete'"
 .get_where_from_filters($filter_gender,$filter_tenure, $filter_department, $filter_location, $today). " "
 ." and pei.orgid=".$orgid.";";
//echo $sql;
        if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
            //return an array
            $retval = mysqli_query($conn, $sql);
        $full_list=[];
 while ($row = mysqli_fetch_assoc($retval)) {
      array_push($full_list,$row);
    }
    if(sizeof($full_list)>0)
    {
        return  avg_with_factors($full_list,'');
    } else {
      return return_empty(sizeof($full_list),'YYYY-MM-DD');
    }
}


function get_all_profiles_filtered_date($orgid,$filter_gender,$filter_tenure, $filter_department, $filter_location, $date){
  //insert data to hte location and dep   
  $mysqli= connection();
  $arr = [];

  $key = constants::MYKEY;
  //echo 'arf';
  $sql = "SELECT 
 communication, teamwork, structure, flexibility, perks, diversity, mission, career, compensation, leadership, development, social,
 pei.tenure + datediff(? , pei.creation_ts)/(30.416667) as tenure, 
 AES_DECRYPT(pel.level,?) as level
 FROM pltps_fields_coeff as pfc 
 left join pltps_candidate_profiles as pcp on pfc.guid = pcp.guid
 inner join pltps_employee_info as pei on pcp.userid = pei.userid 
 left join pltps_employee_level as pel on pei.id = pel.employeeid
 where 1  "
  ."and pei.is_active=1 "
 ."and pcp.status='complete'"
 .get_where_from_filters($filter_gender,$filter_tenure, $filter_department, $filter_location, $date). " "
 ." and pei.orgid=? "
 ." and pei.creation_ts <= ? ;"; 

  $stmt = $mysqli->prepare($sql);
  //$param = "%{$_POST['user']}%";
  $param_orgid = $orgid;
  $stmt->bind_param("ssis", $date, $key, $param_orgid, $date);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()) 
  {

    $arr[] = $row;
  }
  if(sizeof($arr)>=1)
  {
    return avg_with_factors($arr, $date);  
  } else {
    return return_empty(sizeof($arr),$date);
  }
  
  $stmt->close();
}

function return_empty($count,$date)
{
  $null_table=[];
    $null_table['factor_sum']=0;
  $null_table['communication']=0;
  $null_table['teamwork']=0;
  $null_table['structure']=0;
  $null_table['flexibility']=0;
  $null_table['perks']=0;
  $null_table['diversity']=0;
  $null_table['mission']=0;
  $null_table['career']=0;
  $null_table['compensation']=0;
  $null_table['leadership']=0;
  $null_table['development']=0;
  $null_table['social']=0;
  $null_table['count_profiles']=$count;
  $null_table['timeline_date']= $date;
  return $null_table;
}

function avg_with_factors($arrayData, $date)
{
  
  $avg_table =[];
   $avg_table['factor_sum']=0;
  $avg_table['communication']=0;
  $avg_table['teamwork']=0;
  $avg_table['structure']=0;
  $avg_table['flexibility']=0;
  $avg_table['perks']=0;
  $avg_table['diversity']=0;
  $avg_table['mission']=0;
  $avg_table['career']=0;
  $avg_table['compensation']=0;
  $avg_table['leadership']=0;
  $avg_table['development']=0;
  $avg_table['social']=0;
  
  //ramping up from 0 tp 3 month
  

//
  for ($i=0; $i<sizeof($arrayData);$i++)
  {
    //facotr ramp up
    $factor_ramp_up=min(1, FLOOR($arrayData[$i]['tenure'])/3);
    if($factor_ramp_up==0)
    {
      $factor_ramp_up=0.000001;
    }
    $factor_tenure =1;
    $factor_level =1;
    //factor ternue
    $factor_tenure = FLOOR($arrayData[$i]['tenure']/12)/2;
    if($factor_tenure<1)
    {
      $factor_tenure= 1;
    } elseif($factor_tenure>2.5){
      $factor_tenure =2.5;
    }

    //factor level
    switch($arrayData[$i]['level'])
    {
      case 'C':$factor_level=4; break;
      case 'VP':$factor_level=3; break;
      case 'Dir':$factor_level=2.5; break;
      case 'Man':$factor_level=2; break;
      case 'TL':$factor_level=1.5; break;
      default: $factor_level=1;
    }
    
    //factor ramp up affect the overall factors
    $factor_total = $factor_ramp_up*($factor_level * $factor_tenure);

    foreach($avg_table as $field => $value)
    { 
      if($field!= 'factor_sum' )
      {
        $avg_table[$field]+= $arrayData[$i][$field]*$factor_total;
      } else {
        $avg_table['factor_sum']+=$factor_total;
      }
    }

  }
  //then devide by factor
  foreach($avg_table as $field => $value)
  { 
    if($field!= 'factor_sum' )
    {
      $avg_table[$field]= round($value / $avg_table['factor_sum'],2);
    }
  }

  //append the inital count to the avg_table
  $avg_table['count_profiles']=sizeof($arrayData);
  $avg_table['timeline_date']= $date;
  return $avg_table;
}


function get_max_tenure($orgid){
   $conn = connection();
   $key = constants::MYKEY;
        $sql = "SELECT max(FLOOR(tenure/12)) as max
 FROM pltps_employee_info as pei
 where 1 "
  ."and pei.is_active=1 ";
 if($orgid !="arf")
        {
          $sql = $sql . " and pei.orgid=".$orgid.";";
        } else {
          $sql = $sql . ";";
        } 

        if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
        $retval = mysqli_query($conn, $sql);
        return  mysqli_fetch_assoc($retval);
}


function get_all_loc_dep($orgid){
  //insert data to hte location and dep   
  $mysqli= connection();
  $sql = "SELECT id, location, department 
  FROM pltps_org_loc_dep
  where orgid= ? order by location, department asc";  
  $arr = [];
   
  $stmt = $mysqli->prepare($sql);
  //$param = "%{$_POST['user']}%";
  $param_orgid = $orgid;
  $stmt->bind_param("i", $param_orgid);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()) 
  {
    $arr[] = $row;
  }
  return $arr;
  $stmt->close();
}


//used in demo_recruitment
function get_parent_location_department($orgid)
{
 $mysqli= connection();
  $arr = [];
  $sql ="SELECT pol.id as id, pol.name as name from pltps_org_locations as pol where orgid=? ";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $orgid);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()) 
  {
    $arr[$row['name']] = get_departments_from_locations($orgid,$row['name']);
  } 

  return $arr;

}
//used rithg above
function get_departments_from_locations($orgid,$location )
{
  $mysqli= connection();
  $arr = [];
  $sql ="SELECT pod.name as name from pltps_org_departments as pod where orgid=? 
    and locationid= (select id from pltps_org_locations where orgid=? and name =? limit 1)";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("iis", $orgid, $orgid, $location);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()) 
  {
    $arr[] = $row['name'];
  }
  return $arr;
  $stmt->close();
}

//used in demo_recruitment
function get_org_roles($orgid){


  $mysqli= connection();
  $arr = [];
  $sql ="SELECT por.reg, por.role_name, polo.name as location, pod.name as department,  pol.full_url
  FROM pltps_org_regs as por
  left join pltps_org_departments as pod on por.locdepid = pod.id
  left join pltps_org_locations as polo on polo.id = pod.locationid
  left join pltps_org_links as pol on por.id = RIGHT(pol.type, (CHAR_LENGTH(pol.type)-LOCATE('_',pol.type)) )
  where por.is_active=1 
  and por.orgid=? order by por.role_name asc";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $orgid);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()) 
  {
    $arr[] = $row;
  }
  return $arr;
  $stmt->close();


}



function get_orgid($orgpage){
   $conn = connection();
        $sql = "SELECT orgid, type, default_email, poi.name
 FROM pltps_org_links as pol
 inner join pltps_org_info as poi on pol.orgid = poi.id
 WHERE pol.code='".$orgpage."';";  

        if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
        $retval = mysqli_query($conn, $sql);
        return  mysqli_fetch_assoc($retval);
}

function get_orgid_from_id($orgid){
   $conn = connection();
        $sql = "SELECT id, name,creation_ts
 FROM pltps_org_info 
 WHERE id=".$orgid.";";  

        if(!mysqli_query($conn, $sql))               
            {
               //echo "Error: " . $sql . "" . mysqli_error($conn);
              echo 'error';
            }     
        $retval = mysqli_query($conn, $sql);
        return  mysqli_fetch_assoc($retval);
        //return $sql;
}

//used in candidate
function get_role_info_from_id($role_id)
{   

$sql = "SELECT por.role_name, polo.name as location, pod.name as department,  por.is_active
        from pltps_org_regs as por
        left join pltps_org_departments as pod on por.locdepid = pod.id
        left join pltps_org_locations as polo on polo.id = pod.locationid
        where por.id = ?
        order by por.id desc
        LIMIT 1;";
  $mysqli = connection();
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $role_id);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
  return $result;

}
//used in login
function get_pwd_recovery_data($code)
{   
    $mysqli = connection();

  $sql = "SELECT pu.id, ppr.creation_ts, AES_DECRYPT(pu.email,?) as email
from pltps_pwd_recovery as ppr
inner join pltps_users as pu on pu.id = ppr.userid
where ppr.code = ? and ppr.is_active =1";
  $stmt = $mysqli->prepare($sql);
  $key = constants::MYKEY;
  $stmt->bind_param("ss", $key, $code);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
  return $result;

}
//used in setup 
function get_org_anonymous($orgid)
{   
    $mysqli = connection();

  $sql = "SELECT orgid, anonymous from pltps_org_configuration as pog
where orgid = ?";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $orgid);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
  return $result;

}

/********Support function at onboarding**********/
//all 3 used in dashboard when not complete
function get_all_notcomplete($orig){
  $mysqli= connection();
  $arr = [];
  $sql ="SELECT AES_DECRYPT(email,?) as email FROM pltps_employee_info  as pei
  left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
  where pei.userid !=0 and orgid=? 
  and pcp.status!='complete' 
  and pei.is_active=1 ";
  $stmt = $mysqli->prepare($sql);
  $key = constants::MYKEY;
  $stmt->bind_param("si", $key, $orig);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()) 
  {
    $arr[] = $row;
  }
  return $arr;
  $stmt->close();
}

function get_all_nonstarted($orig){
  $mysqli= connection();
  $arr = [];
  $sql ="SELECT AES_DECRYPT(email,?) as email 
  FROM pltps_employee_info as pei 
  left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
  where 
  (pei.userid =0  or pcp.status is null)
  and orgid=?
  and pei.is_active=1 ";
  $stmt = $mysqli->prepare($sql);
  $key = constants::MYKEY;
  $stmt->bind_param("si", $key, $orig);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()) 
  {
    $arr[] = $row;
  }
  return $arr;
  $stmt->close();
}



function get_client_stats($orig){
  $mysqli= connection();
  $arr = [];
  $sql ="SELECT count(pei.id) as notstarted FROM pltps_employee_info as pei left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
  where 
  (pei.userid =0  or pcp.status is null)
  and orgid=?
  and pei.is_active=1 ";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $orig);
  $stmt->execute();
  $arr = $stmt->get_result()->fetch_assoc();
  $notstarted = $arr["notstarted"];
  $stmt->close();

  $arr = [];
  $sql ="SELECT count(pei.id) as complete FROM pltps_employee_info  as pei
  left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
  where pei.userid !=0 and orgid=? and pcp.status='complete'"
   ." and pei.is_active=1 ";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $orig);  
  $stmt->execute();
  $arr = $stmt->get_result()->fetch_assoc();
  $complete = $arr["complete"];
  $stmt->close();

  $arr = [];
  $sql ="SELECT count(pei.id) as notcomplete FROM pltps_employee_info  as pei
  left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
  where pei.userid !=0 and orgid=? 
  and pcp.status!='complete'
  and pcp.status is not null
  and pei.is_active=1 ";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $orig);  
  $stmt->execute();
  $arr = $stmt->get_result()->fetch_assoc();
  $notcomplete = $arr["notcomplete"];
  $stmt->close();

  $arr = [];
  $sql ="SELECT count(id) as total FROM pltps_employee_info  as pei
  where orgid=? "
   ." and pei.is_active=1 ";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $orig);  
  $stmt->execute();
  $arr = $stmt->get_result()->fetch_assoc();
  $total = $arr["total"];
  $stmt->close();

  return ["notstarted" => $notstarted,"notcomplete"=> $notcomplete, "complete" => $complete, "total" => $total];
}

////////SURVEY SECTION

function get_survey_results($surveyid){
  $mysqli= connection();

  $arr = [];
  $sql ="SELECT count(id) as audience FROM pltps_survey_links as psl
  where surveyid = ?";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $surveyid);
  $stmt->execute();
  $arr = $stmt->get_result()->fetch_assoc();
  $audience = $arr["audience"];

  $arr = [];
  $sql ="SELECT count(distinct(userid)) as answers FROM pltps_survey_answers as psa
  where surveyid =?";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $surveyid);  
  $stmt->execute();
  $arr = $stmt->get_result()->fetch_assoc();
  $answers = $arr["answers"];

  //should get the list of vectors beforehand and query for each individually
 $arr = [];
  $sql ="SELECT count(id) as counts, vector, rating 
  FROM pltps_survey_answers as psa
  where surveyid = ?
  group by vector, rating
  ";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i", $surveyid);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()) 
  {
    $arr[] = $row;
  }
  return ["audience" => $audience,"answered"=> $answers, "answers" => $arr];
}



function get_survey_code_info($code){
$mysqli= connection();
  $arr = [];
  $sql ="SELECT 
  psl.userid,
  psv.surveyid, psv.vector, psv.audience_dep,psv.audience_loc, psv.audience_tenure,psv.audience_gender,psv.audience_type
  from pltps_survey_links as psl
  left join pltps_survey_vectors as psv on psl.surveyid = psv.surveyid
  where psl.code = ? and psl.is_active=1";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("s", $code);
  $stmt->execute();
  $result = $stmt->get_result();
  while($row = $result->fetch_assoc()) 
  {
    $arr[] = $row;
  }
  return $arr[0];
  $stmt->close();
}

function get_all_surveys($orgid, $status){
$mysqli= connection();
  $arr = [];

$clause = implode(',', array_fill(0, count($status), '?')); 
$types = str_repeat('s', count($status)); 
$types .= 'i'; 
$fullArr = array_merge($status, array($orgid)); 
$args = array_merge(array($types),$fullArr);
  $sql ="SELECT 
  psi.id,
  psi.hour,
  psi.next_run,
  psi.status,
   psv.vector, psv.audience_dep,psv.audience_loc, psv.audience_tenure,psv.audience_gender,psv.audience_type
  from pltps_survey_info as psi
  left join pltps_survey_vectors as psv on psi.id = psv.surveyid
  where  1
  and psi.status in ($clause)
  and psi.orgid=?
   and psi.is_active=1
   order by psi.next_run, psi.hour asc
   ";


  if(!$stmt = $mysqli->prepare($sql))
  {
    return 'error prepare (' . $stmt->errno . ") " . $stmt->error;
  }

  //$stmt->bind_param($types, ...$fullArr); //4 placeholders to 
  call_user_func_array(array($stmt, 'bind_param'), $args); 
  $stmt->execute();
 $result = $stmt->get_result();

  while($row = $result->fetch_assoc()) 
  {
    $arr[] = $row;
  }
  return $arr;
  $stmt->close();
}


function persist_survey($orgid,$survey_date,$survey_hour,$vector,$location,$department,$tenure,$gender){
  $mysqli= connection();
  $sql ="INSERT into pltps_survey_info 
  (orgid, is_active,hour, next_run, status)
  values 
  (?,1,?,?,'ready')";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("iis", $orgid,$survey_hour,$survey_date);
  $stmt->execute();
  $stmt->close();
  $surveyid= $mysqli->insert_id;

  $sql ="INSERT into pltps_survey_vectors 
  (surveyid,vector, audience_loc, audience_dep, audience_tenure, audience_gender)
  values 
  (?,?,?,?,?,?)";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("isssis", $surveyid,$vector,$location,$department,$tenure,$gender);
  $stmt->execute();
  $stmt->close();
  return $location.$department;
}

/*
while ($row = mysql_fetch_assoc($retval)) {
        echo '<tr>';
        echo "<th>".$row[user]."</th>";
        echo "<th>".$row[scheduled_time]."</th>";      
        echo "<th>".$row[country]."</th>";
        echo "<th>".$row[city]."</th>";        
        echo "<th>".$row[status]."</th>";    
        echo '<tr>';
      }
*/

/*
$retval = mysql_query( $sql, $conn );   
then echo mysql_result($retval, 0, $lang) ;
    or	
    	while ($row = mysql_fetch_assoc($retval)) {

    	 	echo "<li><a href='index.php'>".  $row[$lang]. "</a></li>";		    
		}*/




?>