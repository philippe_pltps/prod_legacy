<?php
// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}
// Initialize the session
include "db_helper/session.php"; //Include PHP MySQL sessions
// Unset all of the session variables


?>

<script>
			<?php
			//prepare data for the js
			include 'db_helper/db_util.php';

			echo "var is_test ='';\n";
			/*if(isset($_GET['setup']))
			{
				if($_GET['setup']=='xanadu')
				{
					echo "is_test='setup';\n";
				}
			}*/

			if(!isset($_SESSION['orgid']))
			{
				$_SESSION=[];
			 $_SESSION["orgid"] = -1;
			}
			echo "var orgid=".$_SESSION["orgid"].";\n";

			$org_anonymous= get_org_anonymous($_SESSION["orgid"])['anonymous'];
			$json_org_anonymous = json_encode($org_anonymous);
			echo "var org_anonymous = ". $json_org_anonymous.";\n";

?>
</script><script>
<?php
			echo "var graph_vectors=[];\n";

			
			$max_tenure =get_max_tenure( $_SESSION["orgid"]);
			$json_max_tenure = json_encode($max_tenure);
			echo "var max_tenure =  ". $json_max_tenure.";\n";	

			$parent_loc_dep = get_parent_location_department($_SESSION["orgid"]);
			$json_parent_loc_dep = json_encode($parent_loc_dep);
			echo "var parent_loc_dep= ". $json_parent_loc_dep.";\n";

			$all_roles = get_org_roles($_SESSION["orgid"]);
			$roles = json_encode($all_roles);
			echo "var roles= ". $roles.";\n";
		?>
</script>
<!DOCTYPE html>
<html>

<head>

	<meta charset="UTF-8">

	<title>Organization analysis page</title>

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style_demo.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/bootstrap-slider.css">
    <link rel="stylesheet" href="css/popup_box.css">
      
	<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
  	<script src="js/addons/Chart.bundle.js"></script>
  	<script src="js/assets/org_demo_graph.js"></script>
  	<script src="js/assets/org_demo_candidate.js"></script>
  	<script src="js/assets/org_demo_setup.js"></script>
 	<script src="js/addons/bootstrap-slider.js"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/site.webmanifest">
</head>


<body>

	<div class="background">
		

		<div class="bg-div">
		    <button type="button" id="sidebarCollapse" class="btn btn-info" style="margin-top:8px">
               <i class="ion-android-menu"></i>
               <span></span>
           	</button>
		    <img class="logo-img" src="images/platypus_logo.png" width="167" height="55" ALT="align box" ALIGN=CENTER>
		   	<a href="http://theplatypus.io" target="_blank"><img class="" src="images/piechart.svg" width="55" height="55" ALT="align box" align="center" style="float:right"></a>
		</div>

		<div class="wrapper">
		    <!-- Sidebar -->
		    <nav id="sidebar" class="active">
		        <div class="sidebar-header">
		        </div>
		        <ul class="list-unstyled components">
		            <p></p>
		            <li >
		                <a href="org_demo_dashboard.php" >Dashboard</a>
		                <!--<a href="org_demo_culture.php" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Culture</a>-->
		                <ul class="collapse list-unstyled" id="homeSubmenu">
		                </ul>
		            </li>
		            <li >
		                <a href="org_demo_culture.php" >Culture</a>
		                <!--<a href="org_demo_culture.php" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Culture</a>-->
		                <ul class="collapse list-unstyled" id="homeSubmenu">
		                    <li>
		                        <a href="#">Dashboard</a>
		                    </li>
		                    <li>
		                        <a href="#">Split</a>
		                    </li>
		                    <li>
		                        <a href="#">Sample</a>
		                    </li>
		                    <li>
		                        <a href="#">Timeline</a>
		                    </li>
		                </ul>
		            </li>
		            <li class="active">
		                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">Recruitment</a>
		                <ul class="collapse list-unstyled show" id="pageSubmenu">
		                    <li id="li_candidate" class="submenu active_tab active">
		                        <a href="#">Candidate</a>
		                    </li>
		                    <li id="li_setup" class="submenu">
		                        <a href="#">Setup Structure</a>
		                    </li>
		                </ul>
		            </li>
		        </ul>
		        <div id="div_container_def" style="margin:10px; font-weight: 500; font-size:large">
		        	Definition:<br>
		        	<div id="div_definition"></div>
		    	</div>
		    </nav>

			<div class="container">
			  	<div class="row">
				    <div class="col column"   id='div_column_0'>
				    	<div id='div_cand_column' class="block">
					    	<div id='div_loading_candidates'>
					  			LOADING
					  		</div>
					    	<div id='div_all_candidates' style="display:none;">
						    	<div id="div_filters_candidates">
								    <div id="div_filters_selects">
								    	<!--Should be cleaned-->
						    			<select id="select_candidate_loc" name="select_candidate_loc" onchange="changeCandidateFilter('location',this)" style="display:none;">		
									    </select> 			    			
						    			<select id="select_candidate_dep" name="select_candidate_dep" onchange="changeCandidateFilter('department',this)" style="display:none;">			
									    </select>
									    <select id="select_candidate_role" name="select_candidate_role" onchange="changeCandidateFilter('role',this)">			      
									    </select> 
						    		</div>
						    	</div>
						    	<div id="div_filters_vectors">
						    		<h4>Add a filter</h4>
						    		<div id="filter_template">
						    			<select id="select_vector" name="select_vector"  class="filter_select">
									      <option value="communication">communication</option>
									      <option value="teamwork">teamwork</option>
									      <option value="structure">structure</option>    
									      <option value="flexibility">flexibility</option>
									      <option value="perks">perks</option>
									      <option value="diversity">diversity</option>    
									      <option value="mission">mission</option>
									      <option value="career">career</option>
									      <option value="compensation">compensation</option>    
									      <option value="leadership">leadership</option>
									      <option value="development">development</option>
									      <option value="social">social</option>              
									    </select> 
									    is <select id="select_operator" name="select_operator"  class="filter_select">
									      <option value=">">greater</option>
									      <option value="<">smaller</option>    
									    </select> 
									    than 
									    <select id="select_value" name="select_value"  class="filter_select">		      
									    	<option value="0">0</option>
									    	<option value="1">1</option>
									    	<option value="2">2</option>
									    	<option value="3">3</option>
									    	<option value="4">4</option>
									    	<option value="5">5</option>
									    	<option value="6">6</option>
									    	<option value="7">7</option>
									    	<option value="8">8</option>
									    	<option value="9">9</option>
									    	<option value="10">10</option>
									    </select> 
									</div>
						    		<div id="button_apply" class="btn btn-primary">Apply</div>	
						    		<div id="filter_list">
						    		</div>
						    	</div>

						    	<div id="div_current_candidates" style="display:none;">
									List of filtered cadidates : <span id="span_count"></span>
						  		</div>
						  		<div id="div_input_sreach">
						  			<input id="input_search" type="text" placeholder="Search for names or emails" >
						  		</div>
						  		<!--<div id="div_fortest_matching">
								  	<select id="select_matching" name="select_matching" onchange="changeMatching(-1)" style='display:none;'>
											      <option value="standard">standard</option>
											      <option value="surface">surface</option>
											      <option value="top3">top3</option>         
											      <option value="emphasis">emphasis</option>  
											      <option value="top3_emphasis" selected='selected'>top3_emphasis</option>         
											      <option value="triple_distance" selected='selected'>triple_distance</option>  
								    </select> 
							  	</div>-->
						  		<div id="div_nbr_candidate">
						  			<select id="select_nbr_candidates" name="select_nbr_candidates" onchange="changeNbr('set')">
									    <option value="20">Show 20</option>
								   		<option value="50">Show 50</option>
								      	<option value="100">Show 100</option>         
								      	<option value="9999">Show All</option>         
									</select> 
						  		</div>
						  		<div id="div_show_all" style="display:none;">
						  			<label class="container" id="candlist">
						  				<input id="checkbox_checked_only" type="checkbox" name="show_all" value="show" style="width:50px">Checked only
						  			<span class="checkmark"></span>
									</label>
						  		</div>
						      	<div id='div_table_candidates' >
							      	<table id='table_candidates'  class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
										<thead class='thead-dark'>
											<tr>
												<th class="th-sm">
													Users
												</th>
												<!--<th class="th-sm">
													Favorite
												</th>-->
												<!--<th class="th-sm">
													Matching
												</th>-->
											</tr>
										</thead>
										<tbody id=tbody_candidates>				
										</tbody>
									</table>
								</div>
								<div id="div_button_add_candidate" >
									<div id="button_add_candidates" class="btn btn-primary" onclick="changeNbr('add')" ><span id='span_add_candidates'>Show more</span></div>	
								</div>
							</div>
						</div>
				    </div>
			    	<div class="col column"  id='div_column_1'>
			    	<!--  <div id='div_definition'></div>-->
			    		<div id="div_org_column" >
			    			<div id="div_block_org_polar" class="block">
						    	<!--<a href="org_open_reg.php" class="btn btn-primary">Setup this structure</a>
						    	<a href="org_profiles.php" class="btn btn-primary">Org profiles</a>-->

								<div id="div_select_graph_loc_dep" class="select_graph_loc_dep">
									<select id="select_graph_loc" name="select_graph_loc" onchange="changeGraphFilter(0,'location',this)">		      
									</select> 
								    <select id="select_graph_dep" name="select_graph_dep" onchange="changeGraphFilter(0,'department',this)"> 
									</select> 
								</div>
								<div id ="div_select_tenure" class="div_select_tenure">
									<select  id="select_tenure" class='select_tenure' onchange="changeGraphFilter(0,'tenure',this)">
										<option value="0" disabled selected>Min. Tenure</option>
									</select>
								</div>
								
								<div id="div_select_gender" class="div_select_gender">
									<select  id="select_gender" class='select_gender' onchange="changeGraphFilter(0,'gender',this)">
										<option value="A" disabled selected>Gender</option>
										<option value="A">All</option>
									      <option value="F">Female</option>
									      <option value="M">Male</option>
									</select>
								</div>
								<div id="div_canvas">
								    <canvas id="orgChart"></canvas>
								</div>
							</div>

							<div id="div_block_org_bar" class="block">
								<div id="div_vector_org_ranking">
									<canvas id="vectorOrgChart" ></canvas>
								</div>
							</div>
							
							
						</div>
					</div>
					<div id='div_parking' style="display:none;">
						<div id="div_cand_single" class="block">
							<div id="div_intro_org" class="div_intro">
					    		<div id="div_candidate_info"> 
								<p id="candidate_info">Setup</p>
							</div>
					    	</div>
							<div id="button_back2" class="btn btn-primary">Back to List</div>
							<div id="button_predict" class="btn btn-primary">Predict</div>
							<div id="button_hire" class="btn btn-primary">HIRE</div>
							<canvas id="candidateChart" ></canvas>
							<div id="div_vector_candidate_ranking">
							   <canvas id="vectorCandidateChart" ></canvas>
							</div>
							
						</div>
						<div id="div_block_prediction" class='block'>
							<div id="div_prediction_slider">
								From 0 to 3 month<br>
								<div id="div_button_play" class="btn btn-primary">Play</div>
								<input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="3" data-slider-step="1" data-slider-value="0"/>
							</div>
							<div id="div_prediction_canvas">
							    <canvas id="predictionOrgChart"></canvas>
							</div>
						</div>
						<div id="div_block_org_structure" class="block">    
							<!--change<div id="div_org_links"></div>           -->
			                <div id="div_checkbox_anonymous" style="display:none;">
			                	<input id="checkbox_anonymous" type="checkbox" name="anonymous" style="width:50px">Anonymous Data
						  		</div>
			                <div id="div_org_loc" class="org_structure">
			                    <h2>Locations</h2>
		                        <div id="div_loc_template">
		                            <input type="text" name="location" placeholder="New Location" id="input_location" value="">
			                            <!--change <input type="text" name="department" placeholder="Department" id="input_department" value="">-->
			                            <div id="button_add_loc" class="btn btn-primary">+</div>
			                            <span id="span_warning_loc" class="help-block"></span>
			                        </div>
			                    <div id="div_location_list"></div>
				            </div>
				            <div id="div_org_dep" class="org_structure">
			                    <h2>Departments</h2>
		                        <div id="div_dep_template">
		                            <input type="text" name="department" placeholder="New Department" id="input_department" value="">
			                            <!--change <input type="text" name="department" placeholder="Department" id="input_department" value="">-->
			                            <div id="button_add_dep" class="btn btn-primary">+</div>
			                            <span id="span_warning_dep" class="help-block"></span>
			                        </div>
			                    <div id="div_department_list"></div>
				            </div>
				        </div>
				       	<div id="div_block_org_roles" class=block>
				       		<div id="div_role_structure">
				                <h2>Add here your new openings</h2>
				                <div id="role_template">
				                        <!--change <select id="select_loc_dep" name="select_loc_dep">	
				    			      	</select>--> 
				    			      	<!--Reg ID
				                        <input type="text" name="id" id="input_reg_id" value="">-->
				                        <input type="text" name="role" placeholder="Role" id="input_role_name" value="">
				                        <div id="button_add_role" class="btn btn-primary">+</div>
				                        <div id="role_warning" class="help-block"></div>
				                </div>
				                <div id="role_list">
				                </div>
				        	</div>
				       	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id='div_popup_hire' class="popup_box popup_box_doc_small" style="display:none; ">
		<div style="margin-left: 10px;">
			<p class='popuptext'>You are about to hire that person, <br>Do you want to continue?
	            <div id='popup_hire_btn_confirm' class="btn btn-primary"> Confirm </div>
	            <div id='popup_hire_btn_cancel' class="btn btn-primary"> Cancel </div>
	        </p>
	    </div>
    </div>
<script src="js/functions/definitions.js"></script>
<script src="js/functions/org_polar.js"></script>
<script src="js/functions/calculation.js"></script>
<script src="js/functions/tools.js"></script>
<script src="js/functions/bar_comparaison.js"></script>
<script>
	var var_type='recruitment';
</script>
<script src="js/functions/timer_minute.js"></script>
<script>
//variables
var is_predict = false;
var slider_value=0;
var current_candidate_id=0;
var super_cadidate_array=[];
var loader_counter =0;
var chunk_size=50;
var async_load=0;
var timeline_counter=3;
call_service_candidate_chunks(loader_counter,chunk_size);

		//initialize the chart
		create_chart("orgChart", "numbers_6");
		//initialize the chart
		create_chart("candidateChart", "numbers_6");
		create_chart("predictionOrgChart", "numbers_6");

		//initialize bar chart org
		create_bar_chart("vectorOrgChart");
		create_bar_chart("vectorCandidateChart");

	//setup arrays for graph filter
	var graph_filters={gender:['A'],department:['All'],location:['All'],tenure:[0]};
	var handle_filters= JSON.parse(JSON.stringify(graph_filters)); 
	//setup the different lists
	populate_select(parent_loc_dep,'select_graph_loc','All','Location');
	populate_select(removeDuplicatesInChild(parent_loc_dep),'select_graph_dep','All','Department');
	populate_select(parent_loc_dep,'select_candidate_loc','All','Location');
	populate_select(removeDuplicatesInChild(parent_loc_dep),'select_candidate_dep','All','Department');
	populateSelectTenure("select_tenure",max_tenure['max']);

	//var orgid=-1;
	graph_filter_call(0,orgid,graph_filters,0);

/*****************initialization of the top menu***************/

$("#li_candidate").on("click", setup_candidate);
$("#li_setup").on("click", setup_setup);
$("#button_predict").on("click", screen_predict );
$("#button_hire").on("click", popup_hire );
$("#popup_hire_btn_confirm").on("click", popup_hire_confirm );
$("#popup_hire_btn_cancel").on("click", popup_hire_cancel );
$("#div_button_play").on("click", play_timeline);
$('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
//default
function setup_candidate(){
	screen_candidate();
	submenu_activate('#li_candidate');
}

function setup_setup(){
	screen_setup();
 	submenu_activate("#li_setup");
}
//redundant on the culture one
function submenu_activate(tab_id){
	$('.active_tab').removeClass('active');
	$('.active_tab').removeClass('active_tab');
	$(tab_id).addClass('active_tab');	
	$(tab_id).addClass('active');	
}

function screen_candidate(){
	$('#div_parking').append($('.block'));
	
	$('#div_column_0').append($('#div_cand_column'));
	$('#div_column_1').append($('#div_block_org_polar'));
	$('#div_column_1').append($('#div_block_org_bar'));
}

function screen_setup(){
	$('#div_parking').append($('.block'));
	$('#div_column_0').append($('#div_block_org_structure'));
	$('#div_column_1').append($('#div_block_org_roles'));
	$('#div_block_org_roles').hide();
	$('#div_org_dep').hide();
}

function screen_predict(){
	if(!is_predict)
	{
		$('#div_parking').append($('#div_block_org_bar'));
		$('#div_column_1').append($('#div_block_prediction'));
		$("#button_predict").html('Cancel');		
		is_predict = true;
	} else {
		$('#div_parking').append($('#div_block_prediction'));
		$('#div_column_1').append($('#div_block_org_bar'));
		$("#button_predict").html('Predict');		
		is_predict = false;
	}
	
}

function popup_hire(){
	$('#div_popup_hire').show();
}

function popup_hire_cancel(){
	$('#div_popup_hire').hide();
}

function popup_hire_confirm(){
	super_cadidate_array[getIndexId(super_cadidate_array,current_candidate_id)].is_selected =2;
	update_candidate_check(current_candidate_id,2)	;
	insert_employee_hire(current_candidate_id)	;
	populateCandidateTable(applyFilters(super_cadidate_array,candidate_filters));

	current_candidate_id=0;
	all_candidate();
	$('#div_popup_hire').hide();
}

/******function to switch to graph******/
$("#button_back2").on("click", all_candidate);


function single_candidate(candidate_id){
	current_candidate_id=candidate_id;
	$('#div_parking').append($('#div_cand_column'));
	$('#div_column_0').append($('#div_cand_single'));
	$('#div_column_1').append($('#div_block_org_polar'));
	$('#div_column_1').append($('#div_block_org_bar'));

		//replace_candidate_dataset(super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)])
	replace_dataset(1,super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)]);
	setVectorSortingDesc(1, super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)]);
	//replace_dataset_merge(2,0,1);

	//init_prediction_array(super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)]);

//candi
	let html='';
	if(orgid==-1)
	{
		html = 'Id: candidate '+ candidate_id+ '<br>';
				//+'Match at: '+ super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)].match+ '<br>';	
	} else {
		html = 'Name: '+ super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)].name+ ' '
			+'Email: '+ super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)].email+ '<br>'
			//+'Match at: '+ super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)].match+ '<br>'
			+'Applied for Role: '+  super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)].role + '<br>'
			+'In Department: '+ super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)].department
			+' located in: '+ super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)].location+ '<br>';
	}

	//set corresponding org graph filters and actualise
	$('#select_graph_loc').val(super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)].location);
	$('#select_graph_dep').val(super_cadidate_array[getIndexId(super_cadidate_array,candidate_id)].department);
	graph_filter_call(0,orgid,graph_filters,0);

	removeElement( "candidate_info");
	addElement("div_candidate_info", 'p', "candidate_info",html);

}


function all_candidate(){
	$('#div_parking').append($('#div_cand_single'));
	$('#div_parking').append($('#div_block_prediction'));
	$('#div_column_0').append($('#div_cand_column'));

	$('#div_column_1').append($('#div_block_org_polar'));
	$('#div_column_1').append($('#div_block_org_bar'));

	//cancel the predict
	$("#button_predict").html('Predict');	
	is_predict=false;

}
/* disable for now
set_anonymous_checkbox();
function set_anonymous_checkbox(){
	
	if(org_anonymous){
		$( "#checkbox_anonymous" ).prop( "checked", true );
	}
	$("#checkbox_anonymous").change(function() {
			if(this.checked) {
				update_org_anonymous(orgid,1);
				org_anonymous=1;

			} else {
				update_org_anonymous(orgid,0)
				org_anonymous=0;
			}
			filtered_super = applyFilters(super_cadidate_array,candidate_filters);
						populateCandidateTable(filtered_super);	
		});
}*/





	//setup the comparaison chart to be the initial one as well

$('#ex1').slider({
    formatter: function(value) {
        //$('#arf').html('Current value: ' + value);
        //console.log('Current value: ' + value);
        //return 'Current value: ' + value;
        slider_value=value;
        
        replace_dataset_merge(2,0,1,slider_value/3)

    }
});


function play_timeline(is_auto){
	//=0;
	if(is_auto!=1 )
	{
		//restart counter
		timeline_counter=0;
		//disable button
		$('#div_button_play').unbind( "click" );
	}
	//timeline_counter=0;
	let t;
	if(timeline_counter<3)//with 3 it will be executed 4 times
	{
		replace_dataset_merge(2,0,1,timeline_counter/3);
		$('#ex1').slider('setValue',timeline_counter);
		//trigger next in 1 sec
		t = setTimeout(next_timeline, 800);	
	} else {
		//re attach the event click to the button
		$("#div_button_play").on("click", play_timeline);
	}
	
}
//action to replay every second
function next_timeline(){
	timeline_counter++;
	$('#ex1').slider('setValue',timeline_counter);
	replace_dataset_merge(2,0,1,timeline_counter/3);
	play_timeline(1);
	
}


//set the tenure when changin the select
function changeGraphFilter(chartId,graph_filter,element) {
	//knowing that chart 0 is the org, 1 is candidate and 2 is comparaison chart
		var selector = document.getElementById(element.id);
		//add to the end and remove the first element
		//graph_filters[graph_filter]
		let buffer ='';

		//adjusment of the values of the lists
		if(element.id=='select_graph_loc')
		{
			funnel_list(element.id, 'location', 'select_graph_dep','department');
		}
	//trigger the data to retrieve
		if(chartId==0)
		{
			graph_filters[graph_filter].push(selector.options[selector.selectedIndex].value);
			graph_filters[graph_filter].shift();		
			//actualize the graph
			graph_filter_call(chartId,orgid,graph_filters,0);			
		} 

	}

//

//ajax call for the filtered set
	function graph_filter_call(chartId,orgId,graph_filters,iteration) {
		//if less than 4 we need to look at bigger subset
		switch(iteration)
		{
			case 3:$('#select_graph_loc').val('All');
			case 2:$('#select_graph_dep').val('All');
			case 1:
			$('#select_gender').val('All');
			$('#select_tenure').val('0');
		}

		setSelectorFilter("select_gender","gender");
		setSelectorFilter("select_tenure","tenure");
		setSelectorFilter("select_graph_dep","department");
		setSelectorFilter("select_graph_loc","location");
            $.ajax({
                type: "POST",
                url: "ajax_graph_filter.php",
                datatype: "json",
                data: {requestType: ['graph_filter_call'],
                		graph_filters:graph_filters,
                		orgId:orgId
            				},
                success: function(data) {  

              		data = JSON.parse(data);
	                    //alert(data['count_profiles']);
	                    //console.log(data['count_profiles']);
                    if(data['count_profiles']<4){

						iteration+=1;
						if(iteration <=3)
						{
							graph_filter_call(chartId,orgId,graph_filters,iteration);
						} else {
							alert("The selected set contains less than 4 individuals than we cannot show to keep ThePlatypus Anonymous :) please try another subset");
						}
                    } else {

                    	//adjust the graph
                    replace_dataset(chartId,data);
                    replace_dataset_merge(2,0,1,slider_value/3)
                    //now if chartId==0 adjust the list
                    setVectorSortingDesc(chartId,data);
	                setGraphVector(data);
	                
	                	if(async_load==0 || async_load==2)
                		{
                			async_load+=1;	
                		}
	                    if(async_load ==3)
	                    {
	                    	filtered_super = applyFilters(super_cadidate_array,candidate_filters);
		                    //console.log(filtered_super,data);
		                    format_service_matching(filtered_super,data);	
	                    }
						handle_filters= JSON.parse(JSON.stringify(graph_filters)); 	                    
                    }
                	
                }
            });
        }

function format_service_matching(array_candidate,array_org)
{
    let arr_cand = [];
    let cand_counter =0;
    if(array_candidate.length>0)
    {
    	for(let index in array_candidate)
		{
				let obj={};
			obj['id']= parseFloat(index);
			for(vector of vector_definitions.vectors)
			{
				obj[vector.name]=parseFloat(array_candidate[index][vector.name]);
			}
			arr_cand.push(obj);
		}
		let org = {};
		org['id']=1;
		for(vector of vector_definitions.vectors)
			{
				org[vector.name]=parseFloat(array_org[vector.name]);
			}
		to_return = {"candidates" : arr_cand, "company": org};
		//TEST
		call_service_matching(array_candidate,JSON.stringify(to_return));	
    } else {
    	populateCandidateTable(array_candidate);
    }
    
}

function call_service_matching(array_candidate,json_matching)
{

	let settings = {
  async: true,
  crossDomain: true,
  url: "https://matching-dot-platypus-backend.appspot.com/api/algorithms/matching/ver2",
  method: "POST",
  headers: {
    Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NjkzMzA2MDQsIm5iZiI6MTU2OTMzMDYwNCwianRpIjoiZDcxZTllOGEtZGFkNS00MzZlLTg3NTYtZjU0YzM5M2UwYmVjIiwiaWRlbnRpdHkiOiJ0aGVwbGF0eXB1cyIsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.ehInGHeYfhRVyHc1V4uqoU_ZBGyvIFfu9OyTAcgiKOM",
    "Content-Type": "application/json",
  },
  data:json_matching,
  success: function(data) 
  			{    
  			//console.log(data);
            //data = JSON.parse(data);               
                   // alert("done"+data);
                   //check tha tthe indentifier match
                   //console.log(data);
                   //TEST
                   let array_candidate_with_matching = populateArrayMatch(array_candidate,data);
                   //console.log(array_candidate_with_matching);
                   //populate table then
                   populateCandidateTable(array_candidate_with_matching);	
                }
            };
           $.ajax(settings);

} 

function call_service_candidate_chunks(current_counter,chunk_size)
{
           $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['get_candidate_chunks'],
                		arguments:[current_counter,chunk_size]
            				},
                success: function(data) {          
                	data = JSON.parse(data);      
                	super_cadidate_array= super_cadidate_array.concat(data);
                	console.log('size is'+ super_cadidate_array.length);
                	if(data.length==chunk_size)
                	{
                		$('#div_loading_candidates').html($('#div_loading_candidates').html() + '.');
                		current_counter+=chunk_size;
                		call_service_candidate_chunks(current_counter,chunk_size);
                	}     else {
                		$('#div_loading_candidates').hide();
                		$('#div_all_candidates').show();

               		
                		console.log('done');
                		
                	}

                   
                }
            });
} 


	/***************************THIS IS FOR THE CANDIDATE SIDE*****************************/
	/*************************************************/
/*************************************************/
/*************************************************/
/*************************************************/
/*************************************************/
/*************************************************/
/*************************************************/
/*********************
****************************/

/****************************************apply and create vector filters for candidate side ***************/

var filterId = 0;
var candidate_filters = [];
var filtered_super =[];

//add the 3 initial filters corresponding to locationm department and role
candidate_filters.push({'id':filterId,'vector':'location','operator':'!=','value':'nofudingloc'});
filterId++;
candidate_filters.push({'id':filterId,'vector':'department','operator':'!=','value':'nofudingdep'});
filterId++;
candidate_filters.push({'id':filterId,'vector':'role','operator':'!=','value':'nofudingrole'});
filterId++;
candidate_filters.push({'id':filterId,'vector':'is_selected','operator':'<','value':2});
filterId++;

candidate_filters.push({'id':filterId,'vector':'search','operator':'like','value':''});
filterId++;

var max_candidate_display=20;

initialize_buttons('noback');
//select the first role in the list
if(roles.length>0)
	{
		populate_select_roles(roles,'select_candidate_role','All','Select a Role');	
		//changeCandidateFilter('role',document.getElementById('select_candidate_role'));
	}


var counters=[];
counters['location']=0;counters['department']=0;

var current_location='';
var current_department='';

populate_structure(parent_loc_dep, 'div_location_list', 'location')

var button_add_loc= document.getElementById("button_add_loc");
    button_add_loc.onclick = function() {
        let input_location = document.getElementById("input_location");
        let location = input_location.value;
        //check is location is empty
        if(location=="")
        {
            $('#span_warning_loc').html("location is empty");
        } else 
        {
            $('#span_warning_loc').html("");
            let html = "<b >"+ location + "</b>";
            //input_location.value = "";
            input_location.value = "";
            addElement('div_location_list', 'p', 'location'+counters['location'],html);                
            counters['location']++;
            insert_org_location(location);
        }
    }

var button_add_dep= document.getElementById("button_add_dep");
    button_add_dep.onclick = function() {
        let input_department = document.getElementById("input_department");
        let department = input_department.value;
        //check is location is empty
        if(department=="")
        {
            $('#span_warning_dep').html("department is empty");
        } else 
        {
            $('#span_warning_dep').html("");
            let html = "<b >"+ department + "</b>";
            //input_location.value = "";
            input_department.value = "";
            addElement('div_department_list', 'p', 'department'+counters['department'],html);                
            counters['department']++;
            parent_loc_dep[current_location].push(department);
            insert_org_department(department,current_location);
        }
    }

var role_counter=0;
populate_roles('role_list');

var button_add_role= document.getElementById("button_add_role");

    button_add_role.onclick = function() 
    {
        //var input_reg_id = document.getElementById("input_reg_id");
        let input_role_name = document.getElementById("input_role_name");

        //var reg = input_reg_id.value;
        let role_name = input_role_name.value;
        
        if(role_name=="")
        {
            $('#role_warning').html() = "Role name is empty";               
        } else 
        {
            $('#role_warning').html().innerHTML ="";
                //reset the inputs
            input_role_name.value = "";
            //input_reg_id.value = "";
            //add visual element in the list
            addElement('role_list', 'p', 'role'+role_counter,
                get_role_html(role_name,current_location,current_department, role_counter, ""));                
            role_counter++;
            //insert into the db
            insert_org_role( {role_name:role_name,location:current_location,department:current_department});
            
            
        }
    }





$("#checkbox_checked_only").change(function() {
    if(this.checked) {
    	candidate_filters[3] = {'id':3,'vector':'is_selected','operator':'==','value':1};
    } else {
    	candidate_filters[3] = {'id':3,'vector':'is_selected','operator':'<','value':2};
    }
    filtered_super = applyFilters(super_cadidate_array,candidate_filters);	
		populateCandidateTable(filtered_super);
});

$("#input_search").keyup(function () {
	if($("#input_search").val()=='')
	{
		candidate_filters[4] = {'id':4,'vector':'search','operator':'like','value':''};	
	} else {
		candidate_filters[4] = {'id':4,'vector':'search','operator':'like','value':$("#input_search").val()};	
	}
	//console.log(candidate_filters);
	filtered_super = applyFilters(super_cadidate_array,candidate_filters);	
		populateCandidateTable(filtered_super);
});



</script>
     
	


</body>

</html>