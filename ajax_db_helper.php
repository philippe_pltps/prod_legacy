<?php

//relative to the one calling so if called form ajax, relative to that one which fucks up the paths
//solution: put the ajax files in the same folder that the pages
//include "../../../database.class.php"; //Include MySQL database class
//include "../../../mysql.sessions.php"; //Include PHP MySQL sessions
//$session = new Session(); //Start a new PHP MySQL session

include 'db_helper/session.php';

include 'db_helper/db_util.php';
//include '../php_util/util.php';
$input = $_POST["requestType"]; // Gets the intention of the ajax post requesting this code



if ($input[0] == 'insertLoc'){
	$orgid = $_SESSION["orgid"];
	$location = $_POST["location"];
    $output = insertLoc($orgid, $location);
    echo json_encode($output);
} 

if ($input[0] == 'insertDep'){
  $orgid = $_SESSION["orgid"];
  $department = $_POST["department"];
  $location = $_POST["location"];
    $output = insertDep($orgid, $department, $location);
    echo json_encode($output);
} 

if ($input[0] == 'deleteLocDep'){
	$orgid = $_SESSION["orgid"];
	$location = $_POST["location"];
	$department = $_POST["department"];
    $output = deleteLocDep($orgid, $location, $department);
    echo json_encode($output);
} 
if ($input[0] == 'insertRole'){
	$orgid = $_SESSION["orgid"];
	$role = $_POST["arguments"];
    $output = insertRole($orgid, $role);
    echo json_encode($output);
} 
if ($input[0] == 'deactivateRole'){
	$orgid = $_SESSION["orgid"];
	$role = $_POST["arguments"];
    $output = deactivateRole($orgid, $role);
    echo json_encode($output);
} 
if ($input[0] == 'getDeps'){
  $arguments = $_POST["arguments"];
    $output = getDeps($arguments[0], $arguments[1]);
    echo json_encode($output);
} 
if ($input[0] == 'getRoles'){
  $arguments = $_POST["arguments"];
    $output = getRoles($arguments[0], $arguments[1]);
    echo json_encode($output);
} 
if ($input[0] == 'getEmployeeInfo'){
  $arguments = $_POST["arguments"];
    $output = getEmployeeInfo($arguments[0], $arguments[1]);
    echo json_encode($output);
} 


if ($input[0] == 'sendPwdEmail'){
  $arguments = $_POST["arguments"];
    $output = sendPwdEmail($arguments[0]);
    echo json_encode($output);
} 

if ($input[0] == 'update_candidate_check'){
  $arguments = $_POST["arguments"];
    $output = updateCandidateCheck($arguments[0], $arguments[1]);
    echo json_encode($output);
} 

if ($input[0] == 'update_org_anonymous'){
  $arguments = $_POST["arguments"];
    $output = updateOrgAnonymous($arguments[0], $arguments[1]);
    echo json_encode($output);
} 

if ($input[0] == 'insert_employee_hire'){
  $arguments = $_POST["arguments"];
    $output = insertEmployeeHire($arguments[0]);
    echo json_encode($output);
} 

if ($input[0] == 'insert_user_usage'){
  $arguments = $_POST["arguments"];
  if(isset($_SESSION["id"]))
  {
    $output = insertUserUsage($_SESSION["id"], $arguments[0]);
    echo json_encode($output);
  }
} 

if ($input[0] == 'persist_survey'){
  $arguments = $_POST["arguments"];
  $orgid = $_SESSION["orgid"];
    $output = persistSurvey($orgid, $arguments[0],$arguments[1],$arguments[2],$arguments[3],$arguments[4],$arguments[5]
      ,$arguments[6],$arguments[7]);
    echo json_encode($output);
} 

if ($input[0] == 'get_candidate_chunks'){
  $arguments = $_POST["arguments"];
  $orgid = $_SESSION["orgid"];
    $output = factory_get_candidate_chunks($orgid, $arguments[0], $arguments[1]);
    echo json_encode($output);
} 

//--------------------------------- Factories ---------------------------------
function insertLoc($orgid, $location){
   return insert_org_location($orgid, $location);
}

function insertDep($orgid, $department, $location){
   return insert_org_department($orgid, $department, $location);
}

function deleteLocDep($orgid, $location, $department){
   return delete_org_loc_dep($orgid, $location, $department);
}

function insertRole($orgid, $role){
   return insert_org_role($orgid, $role);
}

function deactivateRole($orgid, $role){
   return deactivate_org_role($orgid, $role);
}

function getDeps($orgid, $loc){
   return get_deps($orgid, $loc);
}

function getRoles($orgid, $locdepid){
   return get_loc_dep_roles($orgid, $locdepid);
}

function getEmployeeInfo($orgid, $email){
   return get_employee_info($orgid, $email);
}

function updateCandidateCheck($userid, $ischecked){
  return update_candidate_check($userid, $ischecked);
}

function updateOrgAnonymous($orgid, $ischecked){
 return update_org_anonymous($orgid, $ischecked); 
}

function insertEmployeeHire($userid){
 return insert_employee_hire($userid); 
}

function insertUserUsage($userid,$type){
 return insert_user_usage($userid,$type); 
}

function persistSurvey($orgid,$survey_date,$survey_hour,$vector,$location,$department,$tenure,$gender){
  return persist_survey($orgid,$survey_date,$survey_hour,$vector,$location,$department,$tenure,$gender);
}

function factory_get_candidate_chunks($orgid, $index, $length){
  return get_candidate_chunks($orgid, $index, $length);
}

function sendPwdEmail($email){
  
  $code = insert_pwd_request($email);
  
  send_recovery_pwd_mail($email,$code);
  
  return $code;
}



?>