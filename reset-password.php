<?php

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}
// Initialize the session
include "db_helper/session.php"; //Include PHP MySQL sessions

// Include config file
require_once "db_helper/db_util.php";

if(!empty($_GET['code']))
{
    $user_data = get_pwd_recovery_data($_GET['code']);
    if(sizeof($user_data) ==3 )//for the id and the creation ts
    {
        $_SESSION['id']=$user_data['id'];
        $_SESSION['calling']='recovery';
        //is the link expired:
        $recovery_date = new DateTime($user_data['creation_ts']);
        $current_date = new DateTime();

        $interval = $recovery_date->diff($current_date);
        $is_expired = 'yes';
        if($interval->d <1)
        {
            $is_expired='no';
        }
    } else { //means that more that one user has been found with that code aka with the email given which would 
        echo "An error occured, please contact support@theplatypus.io for further investigations";
    }
} else {
    $is_expired='no';
}

// Check if the user is logged in, if not then redirect to login page
//echo print_r($_SESSION);
if((!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) && 
    ($_SESSION['calling']!='recovery'  || $_SESSION['calling']!='first_connection')
    ){
    //if not logged in, means that the person comes from forgot email thus check the argument given
    header("location: index.php");
    exit;
}
 


 
// Define variables and initialize with empty values
$new_password = $confirm_password = "";
$new_password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate new password
    if(empty(trim($_POST["new_password"]))){
        $new_password_err = "Please enter the new password.";     
    } elseif(strlen(trim($_POST["new_password"])) < 6){
        $new_password_err = "Password must have atleast 6 characters.";
    } else{
        $new_password = trim($_POST["new_password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm the password.";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($new_password_err) && ($new_password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
        
    // Check input errors before updating the database
    if(empty($new_password_err) && empty($confirm_password_err)){
        //setup the password
         if(!empty($new_password)){
            $param_password = password_hash($new_password, PASSWORD_DEFAULT); // Creates a password hash
            update_user_pwd($_SESSION["id"], $param_password);
            if($_SESSION['calling']=='recovery')
            {
                update_pwd_request($user_data['email']);
            }
        }
        //if password is set and not error then just end up the sessions
        if($_SESSION['calling']=='first_connection') 
        {
            header("location: org_demo_dashboard.php");    
        }
        else {
            session_destroy();
            header("location: logout.php");
            exit;
        }
        
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/styles_cand.css">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/site.webmanifest">
</head>
<body>
    <div class="background">
        <div class="bg-div">
            <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
            <nav>
            </nav>
        </div>
        <h1>Reset Password</h1>
        <h2>Please fill out this form to reset your password.</h2>
        <div class="card">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                    <div class="banner-content">
                        <?php 
                            if($is_expired=='no')
                            {
                                echo '<div class="signup-form-box">';
                            } else {
                                $_SESSION['calling']='';
                                $_SESSION['id']='';
                                echo '<h3>This link has expired</h3>';
                                echo '<p>Back to the <a href="index.php">Login page</a>.</p>';
                                echo '<div class="signup-form-box" style="display:none">';
                            }
                        ?>
                        
                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); echo ($_SESSION["calling"]=="recovery")? "?code=".$_GET['code']:""; ?>" method="post"> 
                                <div class="form-group <?php echo (!empty($new_password_err)) ? 'has-error' : ''; ?>">
                                    <h3 class="bold">New Password</h3>
                                    <input type="password" name="new_password" class="form-control" value="<?php echo $new_password; ?>">
                                    <span class="help-block"><?php echo $new_password_err; ?></span>
                                </div>
                                <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                                    <h3 class="bold">Confirm Password</h3>
                                    <input type="password" name="confirm_password" class="form-control">
                                    <span class="help-block"><?php echo $confirm_password_err; ?></span>
                                </div>
                                 <div class="form-group">
                                    <button type="submit" class="button-2">Confirm</button>
                                </div>  
                                <?php
                                if($_SESSION['calling']!='first_connection') 
                                    {
                                    echo '<a href="welcome.php" style="height: 30px;">Cancel</a>';
                                    }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</body>
</html>