<?php

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}
include "db_helper/session.php"; //Include PHP MySQL sessions

echo '<script>';

include 'db_helper/db_util.php';
const PREFERENCE = 0.5; 
     
      if(empty($_SESSION['guid']))
      {
        header("Location: https://www.theplatypus.io"); 
        exit();
      } else {
        //Redirect
          
        $_SESSION['calling']=''; 
        $results = get_single_profile(2,$_SESSION['guid']);
        $guid= $_SESSION['guid'];    
        //REDIECT if need be
           // redirect_to($guid,3);
        //unstable for the moment

        if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["form_proof_4"]))
        {
          $user_status = get_profile_status($_SESSION["id"]);
          if($user_status["status"]==3 || isset($_SESSION['is_test']) || $_SESSION['is_test']=='true' )
          {
              $guid = $_SESSION['guid'];
              //if less than 1 add 0.15 otherwise *0,15
              if($_POST["select_vector"] !='none')
              {
                if($results[$_POST["select_vector"]]<1)
                {
                  $results[$_POST["select_vector"]]+=0.15;  
                } else {
                  $results[$_POST["select_vector"]]*=1.15;  
                }
              }
              

              insert_answers_step4($_POST["select_vector"],$guid);
              insert_fields_data($results,3, $guid); 
              insert_user_profile_status($guid, $_SESSION["id"], 'complete'); 
              insert_probe_stepX_data(4,['input_timer' => $_POST['input_timer']], $guid );  
              $redirect = "Location: ";
              //$_SESSION['guid']='';
            if(isset($_SESSION["source"])){
              $redirect= $redirect .'finish.php';
              } else {
                $redirect= $redirect .'welcome.php';
              }
              
              header($redirect); 
                        exit();
          } else {
            echo'</script>An error occured please contact support@theplatypus.io<script>';
          }
        }



        $field_array = json_encode($results);
        echo "var field_array = ". $field_array.";\n";
      }

  echo '</script>';
?>

<!DOCTYPE html>
<html>


<head>

	<meta charset="UTF-8">

<title>Platypus Print & Reflection</title>

  <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <link rel="stylesheet" href="css/styles_cand.css">
 <script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>  
  <script src="js/addons/Chart.bundle.js"></script>
  <script src="js/functions/timer.js"></script>
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
  <link rel="manifest" href="favicon/site.webmanifest">
</head>



<body>

   <div class="bck">
    <div class="bg-div">
      <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
      <a href="http://theplatypus.io" target="_blank"><img class="logo-img" src="images/piechart.svg" width="60" height="65" ALT="align box" align="center" style="float:right"></a>
      <nav>
          
      </nav>
    </div>

    <h1>Reflection</h1>
    <h2 class="no_margin_bot">Upon reflection you might feel that a certain vector should be given a higher value<br>You can now give your Platypus Print a final touch  
        
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"  method="post">
      <div id="point_adjust">       
        Increase one vector of your choice:
        <select id="select_vector" name="select_vector" onchange="changeVector(this)">
          <option value="none" selected>NONE</option>
          <option value="communication">COMMUNICATION</option>
          <option value="teamwork">TEAMWORK</option>
          <option value="structure">STRUCTURE</option>    
          <option value="flexibility">FLEXIBILITY</option>
          <option value="perks">PERKS</option>
          <option value="diversity">DIVERSITY</option>    
          <option value="mission">MISSION</option>
          <option value="career">CAREER</option>
          <option value="compensation">COMPENSATION</option>    
          <option value="leadership">LEADERSHIP</option>
          <option value="development">DEVELOPMENT</option>
          <option value="social">SOCIAL</option>   
        </select>
      </div>   
      </h2>
      <div id='div_main_row'>
        <div class="col-md-4" id='column_0'>
        </div>
        <div class="col-md-4" id='column_1'>
        </div>
        <div class="col-chart">
          <div class="chart-container" >
            <canvas id="userChart"></canvas>
          </div>
        </div>
        
      </div>
      <div class="footer">
        <ul class="progressbar">
          <li class="active">POINT ALLOCATION</li>
          <li class="active">PRIORITISATION</li>
          <li class="active">REFLECTION</li>
          <li>FINISH</li>
          <input type="text" name="form_proof_4" value="form_proof_4" style="display:none">
          <input type="number" name="input_timer" id="input_timer" value="0" style="display:none">
          <button id='button_next' type="submit" class="button-3" >NEXT</button>
        </ul>
      </div>
    </form>
  </div>
</body>

  <script src="js/functions/definitions.js"></script>
  <script src="js/functions/tools.js"></script>
  <script>
    var page='step4';
    let html ='';
    let order= 3
    for (let i =0; i<family_definitions.families.length ;i++ )
    {
      html='<div class="family_'+family_definitions.families[order].name+' reflection_card">'
      +'<h5 class="no_margin_bot">'+family_definitions.families[order].name.toUpperCase()+'</h5>';
        for(let j=0; j<vector_definitions.vectors.length;j++)
        {
          if(vector_definitions.vectors[j].family==order)
          {
            html=html + '<p id="vector_'+j+'" class="definition_reflection reflection_vector">'+jsUcfirst(vector_definitions.vectors[j].name)+'<br>'+vector_definitions.vectors[j].definition+'</p>';
          }
        }
      html = html+'</div>';
      addElementWithClass('column_'+Math.floor(i/2), 'div', 'div_familiy_'+family_definitions.families[order].name, html, 'column');

      switch(order)
      {
        case 3 : order= 2;
        break;  
        case 2 : order= 0;
        break;  
        case 0 : order= 1;
        break;  
      }

    }
        
  </script>

<script src="js/functions/org_polar.js"></script>
<script>  
  console.log(vector_definitions, family_definitions);
  create_chart("userChart", "nonumbers");
    //add the dataset
    replace_dataset(0,field_array);     
    </script>

<script>
var full_table={"people":["communication","teamwork","structure"],
"workplace":["flexibility","perks","diversity"],
"organisation":["mission","career","compensation"],
"behaviour":["leadership","development","social"]
};

var current_vector='none';
current_page='step4';

function changeVector(){
  //first remove the points
  if(current_vector!='none')
    {
      if(field_array[current_vector]<1)
      {
        field_array[current_vector]=Number(field_array[current_vector])-Number(0.15);  
      } else {
        field_array[current_vector]/=1.15;  
      }
    }
  //then get the new one and add points
  current_vector=$("#select_vector").children("option").filter(":selected").val();
  if(current_vector!='none')
  {
    if(field_array[current_vector]<1)
    {
      field_array[current_vector]=Number(field_array[current_vector])+Number(0.15);  
    } else {
      field_array[current_vector]*=1.15;  
    }
  }
  replace_dataset(0,field_array); 
}

function change_family(element) {
  var select_1 = document.getElementById("select_1");
  var select_2 = document.getElementById("select_2");

  select_1.options.remove(0);
  select_1.options.remove(0);
  select_1.options.remove(0);
  select_2.options.remove(0);
  select_2.options.remove(0);


  for(var i=0;i<3;i++)
  {
    var opt = document.createElement('option');
        opt.value = full_table[element[element.selectedIndex].value][i];
        opt.innerHTML =  full_table[element[element.selectedIndex].value][i];
        select_1.appendChild(opt);
  }
  for(var i=1;i<3;i++)
  {
    var opt = document.createElement('option');
        opt.value = full_table[element[element.selectedIndex].value][i];
        opt.innerHTML =  full_table[element[element.selectedIndex].value][i];
        select_2.appendChild(opt);
  }
}

function change_important(element){
  var select_family = document.getElementById("select_family");
  var select_2 = document.getElementById("select_2");

  select_2.options.remove(0);
  select_2.options.remove(0);

  for(var i=0;i<3;i++)
  {
    if(full_table[select_family[select_family.selectedIndex].value][i] != element[element.selectedIndex].value)
    {
    var opt = document.createElement('option');
        opt.value = full_table[select_family[select_family.selectedIndex].value][i];
        opt.innerHTML =  full_table[select_family[select_family.selectedIndex].value][i];
        select_2.appendChild(opt);  
      }    
  }
}


/*var add_button= document.getElementById("button_adjust");

add_button.onclick = function() {
  var selector = document.getElementById("select_1");
  var text = selector[selector.selectedIndex].value;
  field_array[text] ++;
  selector = document.getElementById("select_2");
  text = selector[selector.selectedIndex].value;
  field_array[text] --;
  replace_dataset(field_array);
  document.getElementById("point_adjust").style.display = "none";  
}*/
</script>



</html>