<?php
// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}
// Initialize the session
include "db_helper/session.php"; //Include PHP MySQL sessions
// Unset all of the session variables

 //for the organisation page better use our own org id
 

?>

<script>
			<?php
			//prepare data for the js
			include 'db_helper/db_util.php';

			echo "var is_test ='';\n";
			/*if(isset($_GET['setup']))
			{
				if($_GET['setup']=='xanadu')
				{
					echo "is_test='setup';\n";
				}
			}*/

			if(!isset($_SESSION['orgid']))
			{
				$_SESSION=[];
			 $_SESSION["orgid"] = -1;
			}
			echo "var orgid=".$_SESSION["orgid"].";\n";

?>
</script><script>
<?php
	
	$max_tenure =get_max_tenure( $_SESSION["orgid"]);
	$json_max_tenure = json_encode($max_tenure);
	echo "var max_tenure =  ". $json_max_tenure.";\n";	

	$parent_loc_dep = get_parent_location_department($_SESSION["orgid"]);
	$json_parent_loc_dep = json_encode($parent_loc_dep);
	echo "var parent_loc_dep= ". $json_parent_loc_dep.";\n";

/*	$gen_results = get_survey_general_latest(2);
	$json_gen_results = json_encode($gen_results);
	echo "var gen_results= ". $json_gen_results.";\n";*/

	//get ready surve
	$surveys_ready= get_all_surveys($_SESSION["orgid"], ['ready']);
	$json_surveys_ready = json_encode($surveys_ready);
	echo "var surveys_ready= ". $json_surveys_ready.";\n";

	$surveys_current= get_all_surveys($_SESSION["orgid"], ['ongoing','complete']);
	$json_surveys_current = json_encode($surveys_current);
	echo "var surveys_current= ". $json_surveys_current.";\n";
	//retrieve a response for each of them and build an array for that
	$surveys_current_answers =[];
	foreach ($surveys_current as $index => $survey) {
		array_push($surveys_current_answers,get_survey_results($survey['id']));
	} //then populate the page with the results (in js)
	$json_surveys_current_answers = json_encode($surveys_current_answers);
	echo "var surveys_current_answers= ". $json_surveys_current_answers.";\n";
		?>
</script>
<!DOCTYPE html>
<html>

<head>

	<meta charset="UTF-8">

	<title>Organization Allignment</title>

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style_demo.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/bootstrap-slider.css">
    <link rel="stylesheet" href="css/popup_box.css">
      
	<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
  	<script src="js/addons/Chart.bundle.js"></script>
  	<script src="js/assets/org_demo_graph.js"></script>
  	<script src="js/assets/org_demo_candidate.js"></script>
  	<script src="js/assets/org_demo_setup.js"></script>
 	<script src="js/addons/bootstrap-slider.js"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/site.webmanifest">
</head>


<body>

	<div class="background">
		

		<div class="bg-div">
		    <button type="button" id="sidebarCollapse" class="btn btn-info" style="margin-top:8px">
               <i class="ion-android-menu"></i>
               <span></span>
           	</button>
		    <img class="logo-img" src="images/platypus_logo.png" width="167" height="55" ALT="align box" ALIGN=CENTER>
		   	<a href="http://theplatypus.io" target="_blank"><img class="" src="images/piechart.svg" width="55" height="55" ALT="align box" align="center" style="float:right"></a>
		</div>

		<div class="wrapper">
		    <!-- Sidebar -->
		    <nav id="sidebar" class="active">
		        <div class="sidebar-header">
		        </div>
		        <ul class="list-unstyled components">
		            <p></p>
		            <li >
		                <a href="org_demo_dashboard.php" >Dashboard</a>
		                <!--<a href="org_demo_culture.php" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Culture</a>-->
		                <ul class="collapse list-unstyled" id="homeSubmenu">
		                </ul>
		            </li>
		            <li >
		                <a href="org_demo_culture.php" >Culture</a>
		                <!--<a href="org_demo_culture.php" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Culture</a>-->
		                <ul class="collapse list-unstyled" id="homeSubmenu">
		                </ul>
		            </li>
		            <li>
		                <a href="org_demo_recruitment.php" >Recruitment</a>
		                <!--<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Recruitment</a>-->
		                <ul class="collapse list-unstyled" id="pageSubmenu">
		                </ul>
		            </li>
		             <li class="active">
		                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">Allignment</a>
		                <ul class="collapse list-unstyled show" id="pageSubmenu">
		                    <li id="li_stats" class="submenu active_tab active">
		                        <a href="#">Stats</a>
		                    </li>
		                    <li id="li_setup" class="submenu">
		                        <a href="#">New Survey</a>
		                    </li>
		                    <li id="li_historic" class="submenu">
		                        <a href="#">Historic</a>
		                    </li>
		                </ul>
		            </li>
		        </ul>
		        <div id="div_container_def" style="margin:10px; font-weight: 500; font-size:large">
		        	Definition:<br>
		        	<div id="div_definition"></div>
		    	</div>
		    </nav>

			<div class="container">
			  	<div class="row">
				    <div class="col column-l"   id='div_column_0'>
				    	
				    </div>
					<div id='div_parking' style="display:none;">
						<div id="div_block_survey_stats" class="block">
							<div id="div_survey_current_list">
							</div>
							
						</div>
						<div id='div_block_survey_setup' class="block">
				    		<div id="div_intro">
				    		</div>
				    		<div id="div_add_new">
								<h2>Add a new survey</h2>
								<div id="button_add_survey" class="btn btn-primary">+</div>
							</div>
							<div id="div_survey_setup_list">
								
							</div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<script src="js/functions/definitions.js"></script>
<script src="js/functions/tools.js"></script>
<script>
	var var_type='alignment';
	var counter_surveys_setup=0;
	var counter_surveys_current=0;
</script>
<script src="js/functions/timer_minute.js"></script>
<script>
screen_stats();
//decompose_results()
populate_existing_surveys_ready();
populate_existing_surveys_current();
/*****************initialization of the top/left menu***************/
$('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
//redundant on the culture one
function submenu_activate(tab_id){
	$('.active_tab').removeClass('active');
	$('.active_tab').removeClass('active_tab');
	$(tab_id).addClass('active_tab');	
	$(tab_id).addClass('active');	
}

$("#li_setup").on("click", setup_setup);
$("#li_stats").on("click", setup_stats);


//default
function setup_stats(){
	screen_stats();
	submenu_activate('#li_stats');
}

function setup_setup(){
	screen_setup();
 	submenu_activate("#li_setup");
}


function screen_stats(){
	$('#div_parking').append($('.block'));
	
	$('#div_column_0').append($('#div_block_survey_stats'));

}

function screen_setup(){
	$('#div_parking').append($('.block'));
$('#div_column_0').append($('#div_block_survey_setup'));
}

//////////results analysis
function populate_existing_surveys_current(){
	for( let index in surveys_current)
	{
		addElement('div_survey_current_list', 'div', 'div_survey_current_'+counter_surveys_current,
            get_survey_result_html(counter_surveys_current,'current'));
		decompose_results();

		$('#span_current_hour_'+counter_surveys_current).html(surveys_current[index].hour);
		$('#span_current_vector_'+counter_surveys_current).html(surveys_current[index].vector);
		$('#span_current_loc_'+counter_surveys_current).html(surveys_current[index].audience_loc);
		$('#span_current_dep_'+counter_surveys_current).html(surveys_current[index].audience_dep);
		$('#span_current_tenure_'+counter_surveys_current).html(surveys_current[index].audience_tenure);
		$('#span_current_gender_'+counter_surveys_current).html(surveys_current[index].audience_gender);
		$('#span_current_date_'+counter_surveys_current).html(surveys_current[index].next_run.slice(0, 10));
		$('#span_current_status_'+counter_surveys_current).html(surveys_current[index].status);

		counter_surveys_current++;
	}
}

function decompose_results(){
	let html='The survey has been sent to '+ surveys_current_answers[counter_surveys_current].audience
	+ ' It has been answered by '+surveys_current_answers[counter_surveys_current].answered+' person<br>';
	$('#div_stats_general_'+counter_surveys_current).html(html);

	html='';
	for(let index in surveys_current_answers[counter_surveys_current].answers)
	{
		html = html + '<br>';
		html = html + ' Vector '+ surveys_current_answers[counter_surveys_current].answers[index].vector;
		html = html + ' Rating '+ surveys_current_answers[counter_surveys_current].answers[index].rating;
		html = html + ' Number of persons '+ surveys_current_answers[counter_surveys_current].answers[index].counts;
	}
	$('#div_stats_specifics_'+counter_surveys_current).html(html);
}

function get_survey_result_html()
{
	html ='<div id="div_survey_info_'+counter_surveys_current+'">'
		+'Status: <span id="span_current_status_'+counter_surveys_current+'"></span><br> '
		+'This survey was sent on the <span id="span_current_date_'+counter_surveys_current+'"></span> '
		+'at <span id="span_current_hour_'+counter_surveys_current+'"></span> o\'clock<br>'
		+'Vector concerned: <span id="span_current_vector_'+counter_surveys_current+'"></span><br>'
		+'Location <span id="span_current_loc_'+counter_surveys_current+'"></span> |'
		+'Department <span id="span_current_dep_'+counter_surveys_current+'"></span> <br>'
		+'Tenure <span id="span_current_tenure_'+counter_surveys_current+'"></span> |'
		+'Gender <span id="span_current_gender_'+counter_surveys_current+'"></span><br>'
		+'<div id="div_stats_general_'+counter_surveys_current+'">'
		+'</div>'
		+'<div id="div_stats_specifics_'+counter_surveys_current+'">'
		+'</div>'
		+'<div id="button_archive_survey_'+counter_surveys_current+'"  data-id="'+counter_surveys_current+'" class="btn btn-primary">Archive this survey</div>';	
		+'</div>'
	
	return html;
}


///////setup survey part
$('#button_add_survey').on('click',add_new_survey_setup);

function populate_existing_surveys_ready()
{
	for(let index in surveys_ready)
	{
		addElement('div_survey_setup_list', 'div', 'div_survey_setup_'+counter_surveys_setup,
            get_survey_setup_html(counter_surveys_setup,'Ready'));
		//TODO replace all the following lines with a function
		populate_select(parent_loc_dep,'select_audience_loc_'+counter_surveys_setup,'All','Location');
		populate_select(removeDuplicatesInChild(parent_loc_dep),'select_audience_dep_'+counter_surveys_setup,'All','Department');  
		populateSelectTenure('select_audience_tenure_'+counter_surveys_setup,max_tenure['max']);
		$('#input_hour_'+counter_surveys_setup).val(surveys_ready[index].hour);
		$('#select_vector_'+counter_surveys_setup).val(surveys_ready[index].vector);
		$('#select_audience_loc_'+counter_surveys_setup).val(surveys_ready[index].audience_loc);
		$('#select_audience_dep_'+counter_surveys_setup).val(surveys_ready[index].audience_dep);
		$('#select_audience_tenure_'+counter_surveys_setup).val(surveys_ready[index].audience_tenure);
		$('#select_audience_gender_'+counter_surveys_setup).val(surveys_ready[index].audience_gender);
		$('#input_date_'+counter_surveys_setup).val(surveys_ready[index].next_run.slice(0, 10));
		counter_surveys_setup++;
	}
}

function add_new_survey_setup(){
	addElement('div_survey_setup_list', 'div', 'div_survey_setup_'+counter_surveys_setup,
            get_survey_setup_html(counter_surveys_setup,'Draft'));     
    populate_select(parent_loc_dep,'select_audience_loc_'+counter_surveys_setup,'All','Location');
    $('#select_audience_loc_'+counter_surveys_setup).val('All');
	populate_select(removeDuplicatesInChild(parent_loc_dep),'select_audience_dep_'+counter_surveys_setup,'All','Department');  
	$('#select_audience_dep_'+counter_surveys_setup).val('All');
	populateSelectTenure('select_audience_tenure_'+counter_surveys_setup,max_tenure['max']);

	$('#button_ready_'+counter_surveys_setup).on('click',set_survey_ready);
    counter_surveys_setup++;
}

function get_survey_setup_html(counter,status){
	let html ='<div id="div_timing_'+counter+'">'
    		+'<p>Status: <span id="span_status_'+counter+'">'+status+'</span><br>'
			+'Send on the <input id="input_date_'+counter+'" type="date" style="float:none;" value="2019-08-27">'
			+'At  <input id="input_hour_'+counter+'" type="number" style="float:none;" value="11" min="0" max="23"> o\'clock.'
			+'</p>'
		+'</div>'
		+'<div id="vector_'+counter+'" style="padding:5; background: white; border-radius: 5px">'
			+'Vector '
			+'<select id="select_vector_'+counter+'" name="select_vector">'
	          +'<option value="communication">COMMUNICATION</option>'
	          +'<option value="teamwork">TEAMWORK</option>'
	          +'<option value="structure">STRUCTURE</option>    '
	          +'<option value="flexibility">FLEXIBILITY</option>'
	          +'<option value="perks">PERKS</option>'
	          +'<option value="diversity">DIVERSITY</option>    '
	          +'<option value="mission">MISSION</option>'
	          +'<option value="career">CAREER</option>'
	          +'<option value="compensation">COMPENSATION</option>    '
	          +'<option value="leadership">LEADERSHIP</option>'
	          +'<option value="development">DEVELOPMENT</option>'
	          +'<option value="social">SOCIAL</option>   '
	        +'</select>'
	        +'Target Audience'
	        +'<select id="select_audience_loc_'+counter+'" name="select_audience_dep">'
	        +'</select>'
	        +'<select id="select_audience_dep_'+counter+'" name="select_audience_loc">'
	        +'</select>'
	        +'<select id="select_audience_tenure_'+counter+'" name="select_audience_tenure">'
	        +'</select>'
	        +'<select id="select_audience_gender_'+counter+'" name="select_audience_gender">'
	          +'<option value="All">Gender</option>'
	          +'<option value="F">Female</option>'
	          +'<option value="M">Male</option>    '
	        +'</select>'
		+'</div>';
		if(status=='Draft')
		{
			html+='<div id="button_ready_'+counter+'" data-id="'+counter+'" class="btn btn-primary">Make this survey ready</div>';
		}
		
		html+='<div id="button_delete_'+counter+'" class="btn btn-primary">Delete this survey</div>';

	return html;
}

function set_survey_ready()
{
	if($("#span_status_"+$(this).data('id')).html()=='Draft')
	{
		$("#span_status_"+$(this).data('id')).html('Ready');
		persist_survey($(this).data('id'));	
		$("#button_ready_"+$(this).data('id')).hide();
	}
	
}

function persist_survey(counter)
{
           $.ajax({
                type: "POST",
                url: "ajax_db_helper.php",
                datatype: "json",
                data: {requestType: ['persist_survey'],
                        arguments: 
                        //will be date, hour, vector, loc, dep, tenure, gender
                        [
                        $("#input_date_"+counter).val(),
                        $("#input_hour_"+counter).val(),
                        $("#select_vector_"+counter).val(),
                        $("#select_audience_loc_"+counter).val(),
                        $("#select_audience_dep_"+counter).val(),
                        $("#select_audience_tenure_"+counter).val(),
                        $("#select_audience_gender_"+counter).val()
                        ]
                            },
                success: function(data) {                    
                   // alert("done"+data);
                }
            });
} 

</script>
     
	


</body>

</html>