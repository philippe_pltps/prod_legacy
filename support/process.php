<?php 

 include 'support_util.php';
 include "../php_util/util.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	echo print_r($_POST);
    if (
    	isset($_FILES['files']) 
    	&& ($_POST['org_name']!='' or $_POST['org_id']!='')
    	
) {

        $errors = [];
        $path = 'uploads/';
		$extensions = ['csv'];
		
        $all_files = count($_FILES['files']['tmp_name']);

        for ($i = 0; $i < $all_files; $i++) {  
			$file_name = $_FILES['files']['name'][$i];
			$file_tmp = $_FILES['files']['tmp_name'][$i];
			$file_type = $_FILES['files']['type'][$i];
			$file_size = $_FILES['files']['size'][$i];
			$file_ext = strtolower(end(explode('.', $_FILES['files']['name'][$i])));

			$file = $path . $file_name;

			if (!in_array($file_ext, $extensions)) {
				$errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
			}

			if ($file_size > 2097152) {
				$errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
			}

			if (empty($errors)) {
				//if you mocve from tmp you persis the file. We do not want to keep everything on the server
				//move_uploaded_file($file_tmp, $file);
			}
		}
	//create org
		if($_POST['org_name']!='')
		{
			$org_id = insert_new_org($_POST['org_name']);
	//add the config
			insert_org_config($org_id);
			echo 'created company with name '.$_POST['org_name'].' and org id '.$org_id.'<br>';		
		} else {
			$org_id = $_POST['org_id'];
			echo 'org id '.$org_id. ' selected';
		}
		$csv = array_map('str_getcsv', file($file_tmp));
		$counter=0;
		$emp_id=0;
		$arr=[];
		for($i =1;$i< count($csv);$i++)
		{
			$combo = $csv[$i][1].'_'.$csv[$i][2];
			if(!in_array($combo,$arr))
			{
				array_push($arr,$combo);
			}
		}
		sort($arr);
		$locations = [];
		foreach($arr as $index => $value)
		{
			$split = explode("_",$value);
			//add locations only once
			if(!in_array($split[0], $locations))
			{
				$loc_id = insert_org_location($org_id,$split[0]);
				array_push($locations,$split[0]);	
			}
			//add all the departments
			insert_org_department($org_id,$split[1],$split[0]);
		}

		$first_emp_id=0;
		for($i =1;$i< count($csv);$i++)
		{
			$csv[$i][0]=strtolower($csv[$i][0]);
			//insert for each guy
			//0=email, 1 = location, 2 = department, 3 = tenure, 4 = gender 5 = title
			$emp_id=insert_employee($org_id, $csv[$i][0], $csv[$i][1], $csv[$i][2],$csv[$i][3], $csv[$i][4]);

			if($i==1)
			{
				$first_emp_id=$emp_id;
			}

			insert_employee_level($emp_id,'XX',$csv[$i][5]);
			$employee_link = insert_org_link($org_id, getRandString(32), '','employee_'.$emp_id);
			if(email_single_employee_template($csv[$i][0], $employee_link))
			{
				$counter++;
			} else {
				echo 'error sending to '.$csv[$i][0];
			}
		}
		//encrypt the employees
		encrypt_employee_info($org_id,$first_emp_id);

		if( $_POST['admin_email']!='')
		{
			$pwd_admin = getRandString(10);

			$pwd_hash = password_hash($pwd_admin, PASSWORD_DEFAULT);

			//activate admin
			$admin_user_id = setup_user_admin($org_id,$_POST['admin_email'], $pwd_hash);
			echo 'nbr of admins: ' .update_employee_info($admin_user_id,$org_id,$_POST['admin_email']).'<br>';

			//if(email_admin($_POST['admin_email'],$pwd_admin))
			//{
			//	echo 'admin created '.$_POST['admin_email'].'<br>';
			//}
			echo '<br>admin created '.$_POST['admin_email'].'<br>'.$pwd_admin.'<br>';
		}

		//return employee link
		echo 'Mail sent to '.$counter.' persons';
//	if ($errors) print_r($errors);
    } else {
    	echo 'something';
    }
}
