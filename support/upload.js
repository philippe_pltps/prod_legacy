const url = 'process.php';
const form = document.querySelector('form');

form.addEventListener('submit', e => {
    e.preventDefault();

    const files = document.querySelector('[type=file]').files;
    const formData = new FormData();

    for (let i = 0; i < files.length; i++) {
        let file = files[i];

        formData.append('files[]', file);
    }
    formData.append('org_name',$('#org_name').val());
    formData.append('admin_email',$('#admin_email').val());
    formData.append('org_id',$('#org_id').val());

    $.ajax({
                type: "POST",
                url: "process.php",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {  
                    //alert(data)             ;     
                    //data = JSON.parse(data);
                    $('#result').html('Your link is '+data);
                }
            });

});