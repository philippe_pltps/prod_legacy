<?php

?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>The Platypus</title>
  <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

  <script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>  
  <script src="../js/addons/Chart.bundle.js"></script> 
</head>


<body>
	<div class="background">
		<h1>Test graph</h1>
      <div id="div_definition"><br>
      </div>

    <div class="chart-container" style="width:100%">
      <canvas id="marksChart"></canvas>
    </div>
    
    <div class="chart-container" style="width:100%">
      <canvas id="marksChartBar"></canvas>
    </div>
   
	</div>

</body>
<script src="../js/functions/definitions.js"></script>
<script src="../js/functions/org_polar.js"></script>
<script src="../js/functions/bar_comparaison.js"></script>
<script src="../js/functions/tools.js"></script>
<script src="../js/assets/org_demo_graph.js"></script>
<script>  
    //add the dataset
var test_data=[{"id":"108","communication":"4.43","teamwork":"2.57","structure":"1.00","flexibility":"3.29","perks":"2.00","diversity":"3.14","mission":"2.25","career":"3.71","compensation":"3.86","leadership":"2.75","development":"2.50","social":"3.00","location":"CPH","department":"Product","gender":"F","tenure":"4.9973"},{"id":"142","communication":"3.50","teamwork":"2.33","structure":"0.50","flexibility":"4.50","perks":"0.00","diversity":"3.50","mission":"2.00","career":"3.67","compensation":"4.00","leadership":"3.00","development":"3.17","social":"3.83","location":"CPH","department":"Product","gender":"M","tenure":"6.5425"}]

    create_chart("marksChart", "numbers_6");

   replace_dataset(0,test_data[0]);     
   add_dataset(0, test_data[1]);

   create_bar_chart("marksChartBar");
   replace_bar_dataset(0,test_data[0],getSortedVectors(test_data[0]));
   add_bar_dataset(0,test_data[1]);

    </script>
</html>