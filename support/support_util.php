<?php
// Include config file
include "../../../initialisation.php";

//to follow up the users
function get_all_last_connection(){
	$mysqli= connection();
	$arr = [];
	$sql ="SELECT AES_DECRYPT(email,?) as email, last_connection FROM pltps_users as pu where 
	orgid !=0 and AES_DECRYPT(email,?) not LIKE ?";
	$stmt = $mysqli->prepare($sql);
	//$param = "%{$_POST['user']}%";
	$param = "%test%";
	$key = constants::MYKEY;
	$stmt->bind_param("sss", $key, $key, $param);
	$stmt->execute();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row;
	}
	return $arr;
	$stmt->close();
}

function get_all_clients(){
	$mysqli= connection();
	$arr = [];
	$sql ="SELECT * FROM pltps_org_info";
	$stmt = $mysqli->prepare($sql);
	$stmt->execute();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row;
	}
	return $arr;
	$stmt->close();
}

function get_all_notcomplete($orig){
	$mysqli= connection();
	$arr = [];
	$sql ="SELECT AES_DECRYPT(email,?) as email, pei.userid FROM pltps_employee_info  as pei
	left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
	where pei.userid !=0 and orgid=? and pcp.status!='complete'"
	 ." and pei.is_active=1 ";
	$stmt = $mysqli->prepare($sql);
	$key = constants::MYKEY;
	$stmt->bind_param("si", $key, $orig);
	$stmt->execute();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row;
	}
	return $arr;
	$stmt->close();
}

function get_all_nonstarted($orig){
	$mysqli= connection();
	$arr = [];
	$sql ="SELECT AES_DECRYPT(email,?) as email FROM pltps_employee_info as pei where 
	userid =0 and orgid=? "
	 ." and pei.is_active=1 ";
	$stmt = $mysqli->prepare($sql);
	$key = constants::MYKEY;
	$stmt->bind_param("si", $key, $orig);
	$stmt->execute();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row;
	}
	return $arr;
	$stmt->close();
}



function get_client_stats($orig){
	$mysqli= connection();
	$arr = [];
  $sql ="SELECT count(pei.id) as notstarted FROM pltps_employee_info as pei left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
  where 
  (pei.userid =0  or pcp.status is null)
  and orgid=?
  and pei.is_active=1 ";
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("i", $orig);
	$stmt->execute();
	$arr = $stmt->get_result()->fetch_assoc();
	$notstarted = $arr["notstarted"];
	$stmt->close();

	$arr = [];
	$sql ="SELECT count(pei.id) as complete FROM pltps_employee_info  as pei
	left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
	where pei.userid !=0 and orgid=? and pcp.status='complete'"
	 ." and pei.is_active=1 ";
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("i", $orig);	
	$stmt->execute();
	$arr = $stmt->get_result()->fetch_assoc();
	$complete = $arr["complete"];
	$stmt->close();

	$arr = [];
	$sql ="SELECT count(pei.id) as notcomplete FROM pltps_employee_info  as pei
	left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
	where pei.userid !=0 and orgid=? and pcp.status!='complete'"
	 ." and pei.is_active=1 ";
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("i", $orig);	
	$stmt->execute();
	$arr = $stmt->get_result()->fetch_assoc();
	$notcomplete = $arr["notcomplete"];
	$stmt->close();

	$arr = [];
	$sql ="SELECT count(id) as total FROM pltps_employee_info  as pei
	where orgid=? "
	 ." and pei.is_active=1 ";
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("i", $orig);	
	$stmt->execute();
	$arr = $stmt->get_result()->fetch_assoc();
	$total = $arr["total"];
	$stmt->close();

	return ["notstarted" => $notstarted,"notcomplete"=> $notcomplete, "complete" => $complete, "total" => $total];
	//return $sql;
}

function insert_new_org($org_name)
{
	$mysqli = connection();     
	$sql =  "INSERT into pltps_org_info (name, creation_ts, is_active) values (?, NOW(), 1)"         ;
	
 if (!($stmt = $mysqli->prepare($sql))) {
      
      return "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }
  if (!$stmt->bind_param("s",$org_name)) {
      return "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  if (!$stmt->execute()) {
      return "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  $stmt->close();
  return $mysqli->insert_id;
  
}

function insert_org_config($org_id)
{
	$mysqli = connection();     
	$sql =  "INSERT into pltps_org_configuration (orgid, anonymous) values (?, 0)"         ;
	
 if (!($stmt = $mysqli->prepare($sql))) {
      
      return "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }
  if (!$stmt->bind_param("i",$org_id)) {
      return "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  if (!$stmt->execute()) {
      return "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  $stmt->close();
  return $mysqli->insert_id;
}


function insert_org_location($orgid,$name)
{  
  $mysqli = connection();  
  $sql = "INSERT INTO pltps_org_locations (orgid,name) VALUES (?,?)";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("is", $orgid, $name);
  $stmt->execute();
  return $mysqli->insert_id;
  $stmt->close();
}

function insert_org_department($orgid,$name,$location)
{  
  $mysqli = connection();  
  $sql = "INSERT INTO pltps_org_departments (orgid,name,locationid) VALUES (?,?,(select id from pltps_org_locations where orgid=? and name = ?))";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("isis", $orgid, $name, $orgid, $location);
  $stmt->execute();
  return $mysqli->insert_id;
  $stmt->close();
}


function insert_org_link($org_id, $code, $default_email,$link_type)
{
	$mysqli = connection();     
	$sql =  "INSERT into pltps_org_links (orgid, code, full_url,default_email,type) values (?,?,?,?,?)"         ;
	$full_url = 'https://theplatypus.io/candidate?page='.$code;
 if (!($stmt = $mysqli->prepare($sql))) {
      
      return "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
  }
  if (!$stmt->bind_param("issss",$org_id,$code,$full_url,$default_email,$link_type)) {
      return "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  if (!$stmt->execute()) {
      return "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
  }
  $stmt->close();
  return $full_url;
}

function insert_employee($org_id, $email, $location, $department,$tenure, $gender)
{
	$sql = "INSERT into pltps_employee_info (orgid, email,location, department, tenure, gender,is_active,creation_ts) 
	values (?, ?,	?,	?,	?,	?,1,NOW())";
	$mysqli = connection();  
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("isssis",$org_id, $email, $location, $department,$tenure, $gender);
	$stmt->execute();
	$stmt->close();
	return $mysqli->insert_id;
}

function insert_employee_level($emp_id, $level, $title)
{
	$sql = "INSERT into pltps_employee_level (employeeid, level,title,is_active) 
	values (?, AES_ENCRYPT(?,?), AES_ENCRYPT(?,?),	1)";
	$mysqli = connection();  
	$stmt = $mysqli->prepare($sql);
	$key = constants::MYKEY;
	$stmt->bind_param("issss",$emp_id, $key, $level, $key, $title);
	$stmt->execute();
	$stmt->close();
	return $mysqli->insert_id;
}

function encrypt_employee_info($org_id,$first_emp_id)
{
	$sql = "UPDATE pltps_employee_info set  
	department = AES_ENCRYPT(department,?),
	location = AES_ENCRYPT(location,?),
	gender = AES_ENCRYPT(gender,?),
	email = AES_ENCRYPT(email,?)
	where orgid=?
	and id >=?";
	$key = constants::MYKEY;
	$mysqli = connection();  
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("ssssii",$key,$key,$key,$key,$org_id,$first_emp_id);
	$stmt->execute();
	$stmt->close();
}

function setup_user_admin($org_id,$admin_email,$pwd_hash)
{
	
	$sql = "INSERT into pltps_users (email, password, orgid, creation_ts) 
	values (AES_ENCRYPT(?,?), ?,?, NOW())";
	$mysqli = connection();  
	$key = constants::MYKEY;
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("sssi",$admin_email,$key,$pwd_hash,$org_id);
	$stmt->execute();
	$stmt->close();

	return $mysqli->insert_id;
	
}

function update_employee_info($userid, $orgid, $email)
{
  $mysqli = connection();               
  $stmt = $mysqli->prepare("UPDATE pltps_employee_info set userid =? where
   orgid= ? and 
   email=AES_ENCRYPT(?,?) and userid=0");
  $key = constants::MYKEY;
  $stmt->bind_param("iiss",$userid, $orgid, $email, $key);
  $stmt->execute();
  return $stmt->affected_rows;
  $stmt->close();
}

function email_single_employee($to, $link)
{
	$subject = 'The platypus';
		 
	// Compose a simple HTML email message
	$message = '<html><body>';
	$message .= '<h1 style="color:#f40;">Hi. Welcome to the Platypus</h1>';
	$message .= '<p style="color:#080;font-size:18px;">PLease follow the link and the instructions to make your print</p>';
	$message .= '<p style="color:#492;font-size:18px;">'.$link.'</p>';
	$message .= '</body></html>';
	 
	// Sending email
	return send_mail($to, $subject, $message);
}
function email_single_employee_template($to, $link)
{
	$subject = 'Your Platypus Print';
		 
$message ='<html lang="en">

<body style="font-family: \'Poppins\', sans-serif;">
  <div style="background: #5acee8; padding:40px">
  </div>
  <div style="margin: 0px 60px;">
    <h2 class="email-header beige" style="margin: 50px 0px 3px 0px;     font-size: 50px !important;
        font-weight: 200 !important; ">LETS CREATE YOUR <br> PLATYPUS PRINT
    </h2>

    <h6 style="font-size: 18px !important;
              line-height: 30px !important;
              font-weight: 400 !important;
              margin-bottom: 20px !important;
              letter-spacing: .2px;
              margin-top: 0;">
      <a href="'.$link.'" style="font-weight: 800;
                color: #13c44f;
                font-size: 25px;
                text-decoration: none;">
        '.$link.'
      </a>
    </h6>

    <h6 style="color: #14bfe4;     font-weight: 700!important;     font-size: 23px !important;
        line-height: 30px !important;
        margin-bottom: 3px !important;
        letter-spacing: .2px;
        margin-top: 0;">Via 3 interactive steps
    </h6>

    <h6 style="font-size: 18px !important;
        line-height: 30px !important;
        font-weight: 200 !important;
        margin-bottom: 8px !important;
        letter-spacing: .2px;
        margin-top: 0;"><span style=" font-weight: 700;">1. Point Allocation <br>
      </span>
      Distributing a
      set
      amount of points between
      predefined values.
    </h6>

    <h6 style="font-size: 18px !important;
        line-height: 30px !important;
        font-weight: 200 !important;
        margin-bottom: 8px !important;
        letter-spacing: .2px;
        margin-top: 0;"><span style=" font-weight: 700;">2. Prioritisation <br>
      </span> Ranking values in
      order of
      importance.
    </h6>

    <h6 style="font-size: 18px !important;
        line-height: 30px !important;
        font-weight: 200 !important;
        margin-bottom: 20px !important;
        letter-spacing: .2px;
        margin-top: 0;"><span style=" font-weight: 700;">3. Reflection <br>
      </span>
      Review your responses
      and make
      final changes.
    </h6>

    <h6 style="font-size: 18px !important;
        line-height: 30px !important;
        font-weight: 200 !important;
        margin-bottom: 20px !important;
        letter-spacing: .2px;
        margin-top: 0;">There are no "right" answers.</h6>

    <h6 style="font-size: 18px !important;
        line-height: 30px !important;
        font-weight: 200 !important;
        margin-bottom: 20px !important;
        letter-spacing: .2px;
        margin-top: 0;">The results will be anonymous, there is no way to identify
      an individual based on their Platypus Print.</h6>

    <h6 style="font-size: 18px !important;
        line-height: 30px !important;
        font-weight: 200 !important;
        margin-bottom: 20px !important;
        letter-spacing: .2px;
        margin-top: 0;">
      <span style=" font-weight: 600;">
        Be authentic. Be honest. </span>
      Really consider what is important to you.
    </h6>
    ​
    <h6 style="color: #14bfe4;     font-weight: 700!important;     font-size: 23px !important;
        line-height: 30px !important;
        margin-bottom: 3px !important;
        letter-spacing: .2px;
        margin-top: 0;"> How will I create an impact? </h6>

    <h6 style="font-size: 18px !important;
        line-height: 30px !important;
        font-weight: 200 !important;
        margin-bottom: 20px !important;
        letter-spacing: .2px;
        margin-top: 0;">Once completed your Platypus Print will be merged along with your colleagues to
      create an Organisational Print. You all become part of the bigger picture.</h6>

    <h6 style="font-size: 18px !important;
        line-height: 30px !important;
        font-weight: 200 !important;
        margin-bottom: 20px !important;
        letter-spacing: .2px;
        margin-top: 0;">
      With this data your values are now represented, the company can see what is important
      not only to the Organisation as a whole but for each location and department. </h6>

  </div>
</body>

</html>';


	 
	// Sending email
	return send_mail($to, $subject, $message);
}


function email_admin($to, $pwd)
{
	$subject = 'The platypus';
		 
	// Compose a simple HTML email message
	$message = '<html><body>';
	$message .= '<h1 style="color:#f40;">note your pwd is'.$pwd.'</h1>';
	$message .= '</body></html>';
	 
	// Sending email
	return send_mail($to, $subject, $message);
}

//used by the cron survey file
function get_today_surveys(){
	$mysqli= connection();
	$arr = [];
	$sql ="SELECT * from pltps_survey_info where DATE(next_run) = DATE(NOW()) and status='ready'";
	$stmt = $mysqli->prepare($sql);
	$stmt->execute();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row;
	}
	return $arr;
	$stmt->close();
}


function get_audience_from_survey($surveyid){
	$mysqli= connection();
	$arr = [];
	$sql ="SELECT vector, audience_dep,audience_loc,audience_tenure, audience_gender, audience_type
	 from pltps_survey_vectors as psv
	 where surveyid=?
	 ";
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("i",$surveyid);
	$stmt->execute();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row;
	}
	return $arr[0];
	$stmt->close();
}
//TODO make it for all audiences
function get_audience_user_id_list($orgid, $vector_audience){
	$counter_filter =0;
	$fullArr=[];
	array_push($fullArr,$orgid);
	$key = constants::MYKEY;
	if($vector_audience['audience_dep']=='All')
	{
		$where_dep = "and 1 ";
	} else {
		$where_dep = "and AES_DECRYPT(department,?) =? ";
		$counter_filter+=2;
		array_push($fullArr,$key,$vector_audience['audience_dep']);
	} 

	if($vector_audience['audience_loc']=='All')
	{
		$where_loc = "and 1 ";
	} else {
		$where_loc = "and AES_DECRYPT(location,?) =? ";
		$counter_filter+=2;
		array_push($fullArr,$key,$vector_audience['audience_loc']);
	}
	if($vector_audience['audience_gender']=='All')
	{
		$where_gender = "and 1 ";
	} else {
		$where_gender = "and AES_DECRYPT(gender,?) =? ";
		$counter_filter+=2;
		array_push($fullArr,$key,$vector_audience['audience_gender']);
	}

	/*if($vector_audience['audience_type']=='All')
	{
		$where_type = "and 1 ";
	} else {
		$where_type = "and AES_DECRYPT(gender,?) =? ";
		$counter_filter+=2;
		array_push($fullArr,$key,$vector_audience['audience_gender']);
	}
	very loooow performance query
SELECT pei.userid FROM `pltps_employee_info` as pei
left join pltps_candidate_profiles as pcp on pei.userid = pcp.userid
where 
(select vector_name from pltps_profiles_step3 as pps3 where pps3.guid =pcp.guid order by value desc limit 1) = 'communication'
and pei.orgid=2

research
SELECT guid, vector_name FROM `pltps_profiles_step3` as pps3
where value in (select value from pltps_profiles_step3 where guid = pps3.guid  order by value desc limit 3)
limit 10


	*/

	$where_tenure = " and  (tenure + FLOOR(datediff(NOW() , creation_ts)/(30.416667)))/12 >= ? ";
	array_push($fullArr,$vector_audience['audience_tenure']);

	$mysqli= connection();
	$arr = [];
	$sql ="SELECT userid
	from pltps_employee_info as pei
	where orgid = ? 
	and is_active=1
	and userid <>0 "
	.$where_dep
	.$where_loc
	.$where_gender
	.$where_tenure
	;

	$stmt = $mysqli->prepare($sql);
	$type = "i".str_repeat('s', $counter_filter).'i'; 
	$args = array_merge(array($type),$fullArr);

	call_user_func_array(array($stmt, 'bind_param'), $args); 

	$stmt->execute();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row;
	}
	return $arr;
	$stmt->close();
}

function get_employee_email_form_id($userid)
{  

  $sql = "SELECT AES_DECRYPT(email,?) as email FROM pltps_employee_info WHERE userid = ?
        LIMIT 1";
  $mysqli = connection();
  $stmt = $mysqli->prepare($sql);
  $key = constants::MYKEY;
  $stmt->bind_param("si", $key, $userid);
  $stmt->execute();
  $result = $stmt->get_result()->fetch_assoc();
      if(!$result) {
        return "";
      } else {
        return $result["email"];
      }
}

function is_employee_top($vector, $top, $userid)
{
	$sql = "SELECT 
vector_name
from pltps_candidate_profiles as pcp 
left join pltps_profiles_step3 as pps3 on pcp.guid = pps3.guid

where userid =?
order by value desc
limit ".$top;
  $mysqli = connection();
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i",  $userid);
  $stmt->execute();
  $result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row['vector_name'];
	}
	return in_array($vector,$arr);
	$stmt->close();
      
}



function insert_survey_link($survey_id,  $user_id,$code)
{
	$mysqli = connection();             
  $sql="INSERT into pltps_survey_links (surveyid,survey_date,userid,code,is_active) 
  	values (?,NOW(),?,?,1)";

    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param("iis",$survey_id,  $user_id,$code);
  $stmt->execute();
  return $stmt->insert_id;
  $stmt->close();
}




//send email for survey to all interested right now only to one
function email_survey_test($to,$code, $full_list)
{
	$subject = 'The platypus';
		 
	// Compose a simple HTML email message
	$message = '<html><body>';
	$message .= '<p>New survey for you at
	<a href="'.constants::DEFAULT_URL.'/survey.php?code='.$code.'">
	'.constants::DEFAULT_URL.'/survey.php?code='.$code.'</a></p>';
	foreach($full_list as $index => $email)
	{
		$message .= '<p>'.$email.'</p>';
	}
	$message .= '</body></html>';
	 
	// Sending email
	return send_mail($to, $subject, $message);
}

//send email for survey to all interested right now only to one
function email_survey_code($to,$code)
{
	$subject = 'The platypus Survey';
		 
	// Compose a simple HTML email message
	$message = '<html><body>';
	$message .= '<p>New survey for you at
	<a href="'.constants::DEFAULT_URL.'/survey.php?code='.$code.'">
	'.constants::DEFAULT_URL.'/survey.php?code='.$code.'</a></p>';
	$message .= '</body></html>';
	 
	// Sending email
	return send_mail($to, $subject, $message);
}

function update_survey_status($survey_id, $status)
{
  $mysqli = connection();      
  $sql = "UPDATE pltps_survey_info set status = ? where   id= ?";         
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("si",$status, $survey_id);
  $stmt->execute();
  return $stmt->affected_rows;
  $stmt->close();
}


//reset user profiles and get user guid
function reset_user_profile($user_id){
	$mysqli = connection();  
/* $sql = "select guid from pltps_candidate_profiles where userid =? order by update_ts desc limit 1";         
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i",$user_id);
  $stmt->execute();
  $result  = $stmt->get_result()->fetch_assoc();
  $guid = $result['guid'];
  $stmt->close();

  $sql = "delete from pltps_profiles_step1 where guid =?";         
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("s",$guid);
  $stmt->execute();
  $stmt->close();

  $sql = "delete from pltps_profiles_step2 where guid =?";         
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("s",$guid);
  $stmt->execute();
  $stmt->close();

  $sql = "delete from pltps_profiles_step3 where guid =?";         
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("s",$guid);
  $stmt->execute();
  $stmt->close();*/

  $sql = "delete from pltps_candidate_profiles where userid =?";         
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("i",$user_id);
  $stmt->execute();
  $stmt->close();

}

function get_current_link($email)
{
	$mysqli = connection();  
	$sql = "SELECT full_url from pltps_org_links 
	where type =
		(Select CONCAT('employee_',id) 
		from pltps_employee_info 
		where AES_DECRYPT(email,?)= ?)";
  $stmt = $mysqli->prepare($sql);
  $key = constants::MYKEY;
  $stmt->bind_param("ss",$key,$email);
  $stmt->execute();
  $result  = $stmt->get_result()->fetch_assoc();
  return $result['full_url'];
  $stmt->close();
}


///////used for the migraiton of the tables from model  vecto1|vector2|vector3 etc... into vector_name|value
function get_all_answers($table_name)
{
	$arr = [];
  $mysqli = connection();      
  $sql = "select * from ".$table_name;         
  $stmt = $mysqli->prepare($sql);
  //$stmt->bind_param("si",$status, $survey_id);
  	$stmt->execute();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row;
	}
	return $arr;
	$stmt->close();
}

function insert_profile_line($table_name,$guid,$vector_name,$value)
{

  $mysqli = connection();      
  $sql = "insert into ".$table_name." (guid,vector_name,value) values (?,?,?)";         
$stmt = $mysqli->prepare($sql);
$stmt->bind_param("ssd",$guid,$vector_name,$value);
$stmt->execute();
  $stmt->close();
}

//only used for test atm
function get_single_profile($step,$guid)
{
	$arr = [];
  $mysqli = connection();      
  $sql = "select vector_name, CAST(value as decimal(10,2)) as value from pltps_profiles_step".$step." where guid = ?";         
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("s", $guid);
  	$stmt->execute();
	$result = $stmt->get_result();
	while($row = $result->fetch_assoc()) 
	{
		$arr[] = $row;
	}

	$single_profile=[];
	foreach($arr as $index => $array_vector)
	{
		$single_profile[$array_vector['vector_name']] = $array_vector['value'];
	}

	return $single_profile;
	$stmt->close();
}


?>