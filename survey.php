<?php

// Require https

if ($_SERVER['HTTPS'] != "on" ) {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}

// Include config file
include "db_helper/db_util.php";


?>

<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script> 
<?php

echo '<script>';
$survey_data = get_survey_code_info($_GET['code']);
 $json_survey_data = json_encode($survey_data); 
        echo "survey_data=". $json_survey_data.";\n"; 
echo '</script>';
 
// Processing form data when form is submitted and that the code is right
if($_SERVER["REQUEST_METHOD"] == "POST" && sizeof($survey_data)>0)
{
    //echo print_r($_POST);

    insert_survey_answers($survey_data['surveyid'],$survey_data['userid'],$_POST);
    deactivate_survey_link($_GET['code']);
} 
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Survey page</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/styles_cand.css">
    <script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>  
          <link rel="stylesheet" href="css/popup_box.css">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/site.webmanifest">
</head>
<body>
       
    <div class="bck">
        <div class="bg-div">
            <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
            <a href="http://theplatypus.io" target="_blank"><img class="logo-img" src="images/piechart.svg" width="60" height="65" ALT="align box" align="center" style="float:right"></a>
            <nav>
            </nav>
        </div>
        <h1>Please help us by completing the following survey</h1>

        <div class="card" style="margin-top:0px;">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                    <div class="banner-content">
                        <div class="signup-form-box">
                            <form action="<?php 
                            echo htmlspecialchars($_SERVER["PHP_SELF"]."?code=".$_GET['code']); 
                            ?>" method="post" 
                            <?php 
                            //do not show if survey not available
                            echo ((sizeof($survey_data)==0 || $_SERVER["REQUEST_METHOD"] == "POST") ? 'style="display:none"' : '');
                            ?>
                            >
                                <div id="div_vector_list">
                                </div>
                               
                                <button type="submit" class="button-2">Submit</button>

                            </form>
                            <div id="div_message"
                            <?php 
                            //if obsolete or just completed show
                            echo ((sizeof($survey_data)==0 || $_SERVER["REQUEST_METHOD"] == "POST")?  '': 'style="display:none"');
                                
                            ?>
                            >
                            <?php
                            //message depends if obsolete or just completed
                            echo (sizeof($survey_data)==0 ? '<h1>Sorry this link is either inexistant or expired</h1>' :
                                '<h1>Thanks for completing this survey</h1>');
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>   
    
</body>
</html>

<script src="js/functions/tools.js"></script>

<script>
populate_vector_list();


function populate_vector_list(){
    //for (let index in survey_data)
    //{
        if(survey_data != null)
        addElement('div_vector_list', 'div', 'div_vector_survey',
            get_vector_survey_html(survey_data.vector));      
    //}
    
                                    
}

function get_vector_survey_html(vector){
html = '<p>'
    +'Are you happy with the way the organisation handles the '+vector+'?'
    +' </p>'
    +'<input type="radio" name="'+vector+'" value="1">Very unhappy<br>'
    +'<input type="radio" name="'+vector+'" value="2">Unhappy<br>'
    +'<input type="radio" name="'+vector+'" value="3">Neutral<br>'
    +'<input type="radio" name="'+vector+'" value="4">Happy<br>'
    +'<input type="radio" name="'+vector+'" value="5">Verry Happy<br>';
    return html

}

</script>
