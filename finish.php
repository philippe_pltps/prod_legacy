<?php

// Require https
if ($_SERVER['HTTPS'] != "on") {
    $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}
// Initialize the session
include "db_helper/session.php"; //Include PHP MySQL sessions
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}
  include 'db_helper/db_util.php';
  
//get latest results from step4
 // if(isset($_SESSION["guid"]) && isset($_POST["select_1"]) && isset($_POST["select_2"]))
 // echo print_r($_SESSION);
   //echo print_r($_POST);
  echo '<script>';
  $results = get_single_profile(3,$_SESSION['guid']);
  $field_array = json_encode($results);
        echo "var field_array = ". $field_array.";\n";
        echo '</script>';


if(starts_with($_SESSION['type'],'candidate') )
{
    if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["form_proof"])) {
        // Validate confirm password
        $password = trim($_POST["password"]);
        if(strlen($password) < 6 && strlen($password)> 0){
            $password_err = "Password must have atleast 6 characters.";
        } elseif(empty(trim($_POST["confirm_password"])) && !empty( $password)){
            $confirm_password_err = "Please confirm password.";     
        } elseif(!empty($password)){
            $confirm_password = trim($_POST["confirm_password"]);
            if(empty($password_err) && ($password != $confirm_password)){
                $confirm_password_err = "Password did not match.";
            }
        }

    // Validate credentials
        if(empty($confirm_password_err) && empty($password_err))
        {
            //setup the password
             if(!empty($password)){
                $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
                update_user_pwd($_SESSION["id"], $param_password);
            }
            //if password is set and not error then just end up the sessions
            session_destroy();
            header("location: logout.php");
            exit;
        }
    }
}
/* elseif($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["form_proof"]) && starts_with($_SESSION['type'],'employee')) {
    echo 'An error occured';
} /*elseif(!starts_with($_SESSION['type'],'employee')){
            $_SESSION = array();
        // Destroy the session.
        session_destroy();
}*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Exit page</title>
    <script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>  
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link rel="stylesheet" href="css/styles_cand.css">
    <script src="js/addons/Chart.bundle.js"></script>
    <script src="js/functions/definitions.js"></script>
    <script src="js/functions/org_polar.js"></script>
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">
    
</head>
<body>
    <div class="bck">
        <div class="bg-div">
            <img class="logo-img" src="images/platypus_logo.png" width="200" height="65" ALT="align box" ALIGN=CENTER>
            <a href="http://theplatypus.io" target="_blank"><img class="logo-img" src="images/piechart.svg" width="60" height="65" ALT="align box" align="center" style="float:right"></a>
            <nav>        
            </nav>
        </div>
        <h1>That's it!✔️</h1>
        <h2>Thank you for taking the time to complete your Platypus Print<br>
        <?php echo (starts_with($_SESSION['type'],'candidate')) 
        ? 'For future access and information/suggestions on Organisations that match your values - please re-confirm your email' 
        : 'Your profile along with your co-workers will help to visualize shared values within the organisation<br>Helping to get a sense of what matters to all of you';?>
        </h2>
        <div class="card-exit" <?php echo (starts_with($_SESSION['type'],'candidate')) ? '' : ' style="display:none; " ';?> >
            <div class="row">
                <div class="banner-content">
                    <div class="signup-form-box">
                        <h3 class="bold">Fill this out if you wish to access your print </h3>
                        <p class="login">
                                You will be able to sign in by using your email and password via theplatypus.io or <br>click “Finish” to access the Start Page
                                <span class="bold"> </span>
                            </p>
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>" placeholder="password">
                                <span class="help-block"><?php echo $password_err; ?></span>
                            </div>
                            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>" placeholder="confirm password">
                                <span class="help-block"><?php echo $confirm_password_err; ?></span>
                            </div>
                            	<input type="text" name="form_proof" value="form_proof" style="display:none">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id='div_definition' style="display:none"></div>
        <div class=right_graph>
            <div id="div_canvas">
                <canvas id="orgChart"></canvas>
            </div>
        </div>
        <h2>

                                <a href="https://theplatypus.io"><button type="submit" class="button-5">FINISH</button></a> 
                            </h2>
        <div class="footer">
            <ul class="progressbar">
                <li class="active">POINT ALLOCATION</li>
                <li class="active">PRIORITISATION</li>
                <li class="active">REFLECTION</li>
                <li class="active">FINISH</li>        
            </ul>
        </div> 
    </div>   
</body>

    <script type="text/javascript">
        create_chart("orgChart", "nonumbers");
        replace_dataset(0,field_array);     
    </script>
</html>